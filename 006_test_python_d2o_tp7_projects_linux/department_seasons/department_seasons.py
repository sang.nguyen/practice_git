"""
Copyright: TP7
"""

from __future__ import print_function
from datetime import datetime, timedelta
import ConfigParser
import season_detection_and_plot as sdap
import pandas
from datetime import date as ddate

from d2o.database import Database

from d2o.utils import logger as log
from d2o.utils.systools import makedirs
from d2o.utils.handlers import ExceptionHandler

from d2o.prep.scaling import Scaler
import d2o.utils.putils as put

from collections import OrderedDict
from sklearn.decomposition import PCA

import pandas as pd
import argparse
import numpy as np

import os,__main__, copy
import binascii
import calendar
import time
import csv
date_format = "%Y-%m-%d"
LOG_VERBOSITY   = "debug"
PRINT_VERBOSITY = "info"

MAV_SIZE = 30
TIMEOUT_SECONDS = 120
COLOR_MAP = None

CUR_USER = os.path.dirname(os.path.realpath(__file__)).split('/home/')[1].split('/')[0]
CONFIG_DIR = '/home/{0}/.chronos/department_seasons/conf/'.format(CUR_USER)
LOGDIR     = '/home/{0}/.chronos/department_seasons/tp7_log/'.format(CUR_USER)
COLORS_CONF_PATH = CONFIG_DIR + 'tp7_colors.conf'


def check_results(db, timestamps):
  """ Simple check to see if results from the latest run were written to db
    :param timestamps: Timestamp (runtime)
  :returns:     None
  """
  if (not timestamps): # Not saving to database
    return
  try:
    if (db.check_timestamps([str(x) for x in timestamps])):
      log.success("All data successfully written to database.")
  except Exception as e:
    log.err(ExceptionHandler(e))

def write_to_database_tp7(db, hid, from_date_str, to_date_str, results, min_length, inner_window, outer_window,t_old):
  """ Writes the results from the season algorithm to db.
  TODO: Simplify

    :param hid: hotel id
    :param from_date_str: from date (string)
    :param to_date_str: to date (string)
    :param results: dictionary returned from the season algorithm of a deparment
    :param :param min_length: Min season length in week
    :param inner_window: Inner window size
    :param outer_window: Outer window size
  :returns:     timestamp, the datetime right now (run id)
  """
  d = datetime.utcnow()
  timestamp = calendar.timegm(d.utctimetuple())
  if timestamp<= t_old:
    time.sleep(1)
    d = datetime.utcnow()
    timestamp = calendar.timegm(d.utctimetuple())

  metadict = {
    'run_id':[timestamp], 'h_id':[hid], 'ma_size':[MAV_SIZE],
    'start_date':[from_date_str], 'end_date':[to_date_str], 'min_season_length':[min_length*7],
    'iwindow_size':[inner_window], 'owindow_size':[outer_window]
    }

  metadf = pd.DataFrame.from_dict(metadict)
  db.write_metadata(metadf)

  print('min_length', min_length)
  print('h_id', hid)
  print('inner_window', inner_window)
  print('owindow_size', outer_window)
  #-----

  curr_year    = ddate.today().year
  start        = datetime(curr_year,1,1)

  seasondict = {
    'run_id':[], 'name':[], 'from_date':[], 'to_date':[],
    'from_day':[], 'to_day':[], 'hex':[], 'is_final':[]
  }


  # Result is a dataframe of one department
  for p in range(0,len(results['from'])):
    seasondict['run_id'].append(timestamp)
    seasondict['name'].append(results['name'][p])
    seasondict['from_date'].append(datetime.strftime(start + timedelta(days=results['from'][p]), '%Y-%m-%d'))
    seasondict['to_date'].append(datetime.strftime(start + timedelta(days=results['to'][p]), '%Y-%m-%d'))
    seasondict['from_day'].append(results['from'][p])
    seasondict['to_day'].append(results['to'][p])
    seasondict['hex'].append(results['color'][p])
    seasondict['is_final'].append(0)


  for p in range(0,len(results['from'])):
    seasondict['run_id'].append(timestamp)
    seasondict['name'].append(results['name'][p])
    seasondict['from_date'].append(datetime.strftime(start + timedelta(days=results['from'][p]), '%Y-%m-%d'))
    seasondict['to_date'].append(datetime.strftime(start + timedelta(days=results['to'][p]), '%Y-%m-%d'))
    seasondict['from_day'].append(results['from'][p])
    seasondict['to_day'].append(results['to'][p])
    seasondict['hex'].append(results['color'][p])
    seasondict['is_final'].append(1)


  seasondf = pd.DataFrame.from_dict(seasondict)
  db.write_seasons(seasondf)

 
  log.info("Writing season definitions to database")
  return timestamp


def date_prev_year(date, years):
  if (years == 0):
    return date

  msday = 1000*60*60*24
  date_wd = pd.Timestamp(date).weekday()

  ly_date = date - pd.Timedelta(years, unit='Y')

  ly_wd = pd.Timestamp(ly_date).weekday()
  ly_date = ly_date - pd.Timedelta( (ly_wd - date_wd) * msday, unit='ms')

  ly_wd = ly_date.weekday()
  if (ly_wd != date_wd):
    raise Exception("Current weekday != last year weekday: %s != %s" % (date_wd, ly_wd))
  return pd.Timestamp(ly_date.date())

def convert_to_rates(data):
  rate_revenue = dict()

  for column in set(['_'.join(x.split('_')[:-1]) for x in data.columns.values]):
    revenue     = data[column + '_RV']
    room_nights = data[column + '_RN'].replace([0.0,np.nan, np.inf, -np.inf],1)
    rate_revenue[column + '_RT'] = revenue/room_nights
  return pd.DataFrame.from_dict(rate_revenue)

def create_median_df(data, from_date=None, to_date=None):
  def __get(df, index, column, default=np.nan):
    try:
      return df.ix[index][column]
    except:
      return np.nan

  def __nanmedian(l):
    ret = np.nanmedian(l)
    return ret if ret != np.nan else 0.0

  if (from_date != None):
    data = data[from_date:]
  if (to_date != None):
    data = data[:to_date]

  years = sorted(list(set(data.index.year)))
  latest_year = years[-1]
  num_days = 366 # If calendar.isleap(latest_year) else 365

  first_date = datetime.strptime('%s-01-01' % latest_year, '%Y-%m-%d')
  d_index = [pd.Timestamp(first_date + timedelta(days=x)) for x in range(0, num_days)]

  years_back = [int(latest_year - ym) for ym in reversed(years)]
  d_dict = {}
  for i,date in enumerate(d_index):
    d_dict[date] = { c:__nanmedian([__get(data, date_prev_year(date, y), c) for y in years_back]) for c in data.columns.values }

  mdf = pd.DataFrame.from_dict(d_dict, orient='index')
  mdf = mdf.fillna(0.0)

  return mdf


def main_id_list(df):
  '''
  Get the list of deparmants' id
  :param df: Hotel data
  :return: List of deparments' id
  '''
  id_list = list()
  df_col_names = list(df.columns.values)[1:]
  special_character = '_'
  for i in range(len(df_col_names)):
    special_character_pos = df_col_names[i].find(special_character, 0)
    new_col_names = df_col_names[i][:special_character_pos]
    id_list.append(new_col_names)
  return id_list

def conditional_sum(df):
  '''
  Fill hotel data with sum columns of departments
  :param df: Hotel data (dataframe)
  :return: Hotel data with sum columns for all departments (dataframe)
  '''
  new_df = df
  main_id = np.unique(main_id_list(new_df))
  id_list = main_id_list(new_df)
  special_character = '_0'
  for i in range(len(main_id)):
      item = main_id[i]
      full_item = item + special_character
      if full_item in list(df.columns.values):
          new_df[item + '_0'] = new_df[item + '_0']
      elif id_list.count(item) > 1:
          new_df[item + '_0'] = new_df.filter(regex = item).sum(axis = 1) - new_df.filter(regex = full_item).sum(axis = 1)
      else:
          new_df[item + '_0'] = new_df.filter(regex = item).sum(axis = 1)
  return new_df


def R_suffix_list(df):
  '''
  Get the suffix_list of data
  :param df: MAV(30) of Revenue/Roomnights of Deparment (ex: '224_0_MAV(30)_RV')
  :return: suffix_list of data (ex:'_RV')
  '''
  R_suffix_list = list()
  df_col_names = list(df.columns.values)[1:]
  R_character = '_R'
  for i in range(len(df_col_names)):
    R_character_pos = df_col_names[i].find(R_character, 0)
    R_suffix = df_col_names[i][R_character_pos:]
    R_suffix_list.append(R_suffix)
  return R_suffix_list


def R_prefix_list(df):
  '''
  Get the prefix_list of data
  :param df: MAV(30) of Revenue/Roomnights of Deparment (ex: '224_0_MAV(30)_RV')
  :return: prefix_list of data (ex:'224_0_MAV(30)')
  '''
  R_prefix_list = list()
  df_col_names = list(df.columns.values)[1:]
  R_character = '_R'
  for i in range(len(df_col_names)):
    R_character_pos = df_col_names[i].find(R_character, 0)
    R_prefix = df_col_names[i][:R_character_pos]
    R_prefix_list.append(R_prefix)
  return R_prefix_list


def change_number(df):
  df_col_names = list(df.columns.values)[1:]
  prefix = R_prefix_list(df)
  suffix = R_suffix_list(df)

  for i in range(len(df_col_names)):
    col_RN = prefix[i] + '_RN'
    col_RV = prefix[i] + '_RV'
    if np.sum(df[col_RN]) == float(len(df[col_RN])):
      df[col_RN] = df[col_RV]
    else:
      df[col_RN] = df[col_RN]
  return df


def hotel_run(dep_list, room_nights, room_revenue, week_number=3, max_period_medium = 1000):

  room_nights = put.impute(room_nights, method='zero')
  room_nights[room_nights < 0.0] = 0.0
  room_nights.index.names = ['date']
  room_nights = conditional_sum(room_nights)
  #room_nights.to_csv("14.csv")

  room_nights = put.add_mav(room_nights, MAV_SIZE)
  room_nights.columns = ['%s_RN' % x for x in room_nights.columns.values]

  room_revenue = put.impute(room_revenue, method='zero')
  room_revenue[room_revenue < 0.0] = 0.0
  room_revenue = conditional_sum(room_revenue)

  room_revenue = put.add_mav(room_revenue, MAV_SIZE)
  room_revenue.columns = ['%s_RV' % x for x in room_revenue.columns.values]

  departments = set([col.split('_')[0] for col in room_nights.columns])
  print(departments)

  hotel_season = {} # Output seasonality and level for hotel

  for Mydep in dep_list:  # Each department get data and plot
    print(Mydep)
    room_nights_colname_new = []
    for Myid in room_nights.columns:
      if (Mydep + "_") in Myid:
        room_nights_colname_new.append(Myid)

    room_nights_new = room_nights[room_nights_colname_new]

    room_revenue_colname_new = []
    for Myid in room_revenue.columns:
      if (Mydep + "_") in Myid:
        room_revenue_colname_new.append(Myid)

    room_revenue_new = room_revenue[room_revenue_colname_new]

    # Have serveral segments, each segment may have room_night or not
    my_flag_plot = 0  # Department have unit data
    for my_rn_col in room_nights_new.columns:
      if '_0_' in my_rn_col and sum(room_nights_new[my_rn_col]) == 0:
        my_flag_plot = 1  # Sum of total is zero, then there is only revenue

      if sum(room_nights_new[my_rn_col]) == 0:
        room_nights_new[my_rn_col] = 1
        # If all values are zero, replacing by 1 pd(col)=1 pd(col).sum=0
    print(my_flag_plot)

    combined = put.combine([room_nights_new, room_revenue_new])

    mav_columns = [x for x in combined.columns.values if 'MAV' in x]
    mav_median_df = create_median_df(combined[mav_columns])

    mav_rate_median_df = convert_to_rates(mav_median_df)

    mav_median_df = put.combine([mav_median_df, mav_rate_median_df])
    mav_median_df = change_number(mav_median_df)
    mav_median_df = mav_median_df.replace([np.inf, -np.inf], np.nan)
    mav_median_df = mav_median_df.fillna(0.0)

    mav_median_df.rename(columns={
      u'%s_0_MAV(%s)_RN' % (Mydep, MAV_SIZE): 'Room Nights',
      u'%s_0_MAV(%s)_RV' % (Mydep, MAV_SIZE): 'Revenue',
      u'%s_0_MAV(%s)_RT' % (Mydep, MAV_SIZE): 'Rate'
    }, inplace=True)

    # ------------------------- DO PCA -------------------------
    pca_data = Scaler().fit(mav_median_df).transform(mav_median_df)
    colnames = pca_data.columns.values
    matrix = pca_data.as_matrix()

    num_cols = matrix.shape[1]
    pca = PCA(copy=True, n_components=num_cols, whiten=True)
    vals = pca.fit_transform(matrix)

    pca_dict = OrderedDict()
    pc1 = vals[:, 0]

    pc2 = vals[:, 1]
    diff = pc1 - pc2
    pca_dict['PC1'] = pc1
    pca_dict['PC2'] = pc2
    pca_dict['diff'] = diff
    pca_df = pd.DataFrame.from_dict(pca_dict)
    pca_df.index = pca_data.index
    # ------------------------ END -------------------------


    # ------------------ COMBINE PCA/TOTAL_DF ------------------
    df = put.combine([pca_df, mav_median_df])
    unscaled = copy.deepcopy(df)

    df = Scaler(method='min_max').fit(df).transform(df)
    # ------------------------ END -------------------------


    # ------------------ REINDEX------------------
    df.index = range(0, len(df))
    # ------------------ END ------------------


    # ------------------------- RUN -------------------------

    ycol = 'PC1'
    color_cols = ['Room Nights', None, 'Rate']

    # ------------------------ END -------------------------


    # --------------------- SEASON DETECT -----------------------

    min_days_medium = week_number * 3

    if my_flag_plot == 1:  # Plot revenue and PC1
      pc1_nor = (pc1 - np.min(pc1)) / (np.max(pc1) - np.min(pc1))
      rev = mav_median_df["Revenue"]
      rev_nor = rev
      if sum(rev) != 0:
        rev_nor = (rev - np.min(rev)) / (np.max(rev) - np.min(rev))

      df = pandas.DataFrame({"PC1": pc1_nor, "Revenue": rev_nor, "Date": mav_median_df.index.values})

      # Plot df_plot here
      level = np.linspace(0.02, 0.98, num=25)

      medium_interval = sdap.season_detection(x=df['PC1'], level=level, max_period=max_period_medium, min_days=min_days_medium)[0]
      medium_level = sdap.setlevel_rev(rev=df['Revenue'], t=medium_interval, level=level)

      aa = pandas.DataFrame(medium_interval, columns=['from'])
      aa['to'] = np.concatenate((medium_interval[1:]-1, [0]))
      aa['to'].iloc[-2] = 365
      new_periods = aa[:-1]
      new_periods['season'] = map(int, (medium_level+0.02)/0.04)

      my_cmap = pandas.DataFrame()
      my_cmap['season'] = np.arange(1, 26)
      my_cmap['color'] = ['#97040c', '#9d4b00', '#d4b400', '#0515a9', '#026f02',
                          '#a0300e', '#d85013', '#ffdd1c', '#454ea0', '#20a308',
                          '#d8017a', '#fc6621', '#f1ef00', '#3d8ddc', '#3cc923',
                          '#fc4eb0', '#fb9262', '#fffe69', '#3fcdfd', '#88d63c',
                          '#ff96d1', '#ffc0a4', '#fffeab', '#9de6fe', '#98edb5']
      my_cmap['name'] = ['dip_1', 'dip_2', 'dip_3', 'dip_4', 'dip_5',
                      'low_1', 'low_2', 'low_3', 'low_4', 'low_5',
                      'medium_1', 'medium_2', 'medium_3', 'medium_4', 'medium_5',
                      'high_1', 'high_2', 'high_3', 'high_4', 'high_5',
                      'peak_1', 'peak_2', 'peak_3', 'peak_4', 'peak_5']
      new_periods = pd.merge(new_periods, my_cmap, on='season', how='left')
      #new_periods.to_csv(Mydep + ".csv", index=False, header=True)

      # Output hotel_season_detection
      dep_season = new_periods.to_dict('list')
      hotel_season[Mydep] = dep_season
      
      #print("Process", Mydep, "%s seconds ---" % (time.time()-start_time))
      
    else:
      level = [0.1, 0.3, 0.5, 0.7, 0.9]

      pc1_nor = (pc1 - np.min(pc1)) / (np.max(pc1) - np.min(pc1))
      rev = mav_median_df["Room Nights"]
      rn_nor = (rev - np.min(rev)) / (np.max(rev) - np.min(rev))
      rev = mav_median_df["Rate"]
      rate_nor = (rev - np.min(rev)) / (np.max(rev) - np.min(rev))

      df = pandas.DataFrame(
        {"PC1": pc1_nor, "Room Nights": rn_nor, "Rate": rate_nor, "Date": mav_median_df.index.values})

      # Add more plot with RN & Rate
      medium_interval = sdap.season_detection(x=df['PC1'], level=level, max_period=max_period_medium, min_days=min_days_medium)[0]

      medium_level_rn = sdap.setlevel_rev(rev=df['Room Nights'], t=medium_interval, level=level)
      medium_level_rate = sdap.setlevel_rev(rev=df['Rate'], t=medium_interval, level=level)

      aa = pandas.DataFrame(medium_interval, columns=['from'])
      aa['to'] = np.concatenate((medium_interval[1:] - 1, [0]))
      aa['to'].iloc[-2] = 365
      new_periods = aa[:-1]
      new_periods['season'] = map(int, (medium_level_rate + 0.02) / 0.04)

      my_cmap = pandas.DataFrame()
      my_cmap['season'] = np.arange(1, 26)
      my_cmap['color'] = ['#97040c', '#9d4b00', '#d4b400', '#0515a9', '#026f02',
                          '#a0300e', '#d85013', '#ffdd1c', '#454ea0', '#20a308',
                          '#d8017a', '#fc6621', '#f1ef00', '#3d8ddc', '#3cc923',
                          '#fc4eb0', '#fb9262', '#fffe69', '#3fcdfd', '#88d63c',
                          '#ff96d1', '#ffc0a4', '#fffeab', '#9de6fe', '#98edb5']
      my_cmap['name'] = ['dip_1', 'dip_2', 'dip_3', 'dip_4', 'dip_5',
                         'low_1', 'low_2', 'low_3', 'low_4', 'low_5',
                         'medium_1', 'medium_2', 'medium_3', 'medium_4', 'medium_5',
                         'high_1', 'high_2', 'high_3', 'high_4', 'high_5',
                         'peak_1', 'peak_2', 'peak_3', 'peak_4', 'peak_5']
      new_periods = pd.merge(new_periods, my_cmap, on='season', how='left')
      #new_periods.to_csv(Mydep + ".csv", index=False, header=True)

      # Output hotel_season_detection
      dep_season = new_periods.to_dict('list')
      hotel_season[Mydep] = dep_season
      #print("Process", Mydep, "%s seconds ---" % (time.time()-start_time))
      
  return hotel_season

def run(username, password, host, database, schema, hotel_ids, years_back=3, write=True, plot=False, min_length=3, inner_window=14, outer_window=60):
  """ Runs the season algorithm with the specified arguments.
  Writes the results to db after completion.

    :param username: database username
    :param password: database password
    :param host: database host/ip
    :param schema: database schema (deprecated)
    :param hotel_ids: list of hotel ids to run on
    :param years_back: number of years of data to include
    :param write: wether or not data should be written to the database after completion, default=True
    :param plot: create local plots, default=False
    :param min_length: minimum season length in week, default=3
    :param inner_window: not use
    :param outer_window: not use
    :returns:     None
  """  
  timestamps = []
  db = Database(uid=username, pwd=password, host=host, dbname=database, schema=schema)
  hotel_ids = 'all' if hotel_ids == 'all' else [int(x) for x in hotel_ids.split(',')]

  try:
    db.is_connected()
  except:
    pass

  if (__name__ != "__main__"):
    log.set_print_verbosity("silent")

  curdir = os.path.dirname(os.path.realpath(__main__.__file__))
  if not (curdir[-1] == "/"):
    curdir += "/"
  plotsdir = curdir + "plots/%s/iw%s_ow%s/" % (min_length,inner_window,outer_window)

  if (not db.db.is_connected()):
    raise Exception("Could not connect to database")

  hotel_names = db.hotel_names()

  if (hotel_ids == 'all'):
    log.warn('Running on all hotels in database')
    hotel_ids = hotel_names['hotel_id'] #'.index.values.tolist()

  for i,hid in enumerate(hotel_ids):
    log.dbg("Current index: %s, remaining %s" % (i+1, len(hotel_ids)))

    try:
      hotel_name = db.hotel_name(hid)
      #print("Read from data base: ", "--- %s seconds ---" % (time.time() - start_time))
      
    except Exception as e:
      log.err("%s\n\nCould not fetch data for hotel id %s" % (ExceptionHandler(e), hid))
      continue
    if (not db.has_data(hid)):
      log.err("Insufficient data for hotel %s (%s), skipping..." % (hid, hotel_name))
      continue

    from_date = db.earliest_date(hid)
    to_date   = db.latest_date(hid)

    max_days = years_back * 365
    if ((to_date - from_date).days > max_days):
      from_date = to_date - timedelta(days=max_days)
    elif ((to_date - from_date).days < 365):
      log.err("Insufficient data for hotel %s (%s), skipping..." % (hid, hotel_name))
      continue

    from_date_str = from_date.strftime(date_format)
    to_date_str = to_date.strftime(date_format)

    try:

      room_nights  = db.room_nights(hid, from_date, to_date)
      revcols = room_nights.columns

      room_revenue = db.revenue(hid, from_date, to_date, columns=revcols)
      dep_list = set([col.split('_')[0] for col in room_nights.columns]) - {str(hid)}
      results = hotel_run(dep_list=dep_list, room_nights=room_nights, room_revenue=room_revenue, week_number=min_length, max_period_medium=1000)

      if (write):
        timestamps = []
        t_old=0
        for dep_id in dep_list:
          t = write_to_database_tp7(db, dep_id, from_date_str, to_date_str, results[dep_id], min_length, inner_window, outer_window,t_old)
          timestamps.append(t)
          t_old = t
               
        check_results(db, timestamps)
      else:
        log.warn("Running with -N, results not saved to db")

      log.success("Finished running on hotel %s (%s)" % (hid, hotel_name))
      #print("Write to database: ", "%s seconds ---" % (time.time()-start_time))
      
    except Exception as e:
      log.err("Could not run on hotel %s (%s): %s\nSkipping..." % (hid, hotel_name, ExceptionHandler(e)))
      continue

import time

if __name__ == "__main__":
  try:
    start_time = time.time()
    
    parser = argparse.ArgumentParser("Generates seasons and writes result to database", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-H", "--host", help="Database host", required=True)
    parser.add_argument("-U", "--user", help="Database user name", required=True)
    parser.add_argument("-P", "--password", help="Database password", required=True)
    parser.add_argument("-d", "--database", help="Database", required=True)
    parser.add_argument("-s", "--schema", help="Database schema", default="dbo")
    parser.add_argument("-i", "--hotel-ids", help="Comma separated list of hotel id's or 'all'", default="all")
    parser.add_argument("-l", "--length", help="Minimum length for each season", default=3)
    parser.add_argument("-Y", "--years", help="Number of years of data to include", default=3)
    parser.add_argument("-N", "--no-write", help="Run, but do not write results to database", action="store_true", default=False)
    parser.add_argument("-A", "--anims", help="Create animations and plots", action="store_true", default=False)
    parser.add_argument("-I", "--inner-window", help="Inner window size", default=14)
    parser.add_argument("-O", "--outer-window", help="Outer window size", default=60)

    args = parser.parse_args()
    host = args.host
    user = args.user
    password = args.password
    dbname = args.database
    schema = args.schema
    hotel_ids = args.hotel_ids
    years_back = int(args.years)
    min_length = int(args.length)
    no_write = args.no_write
    make_plots = args.anims

    inner_window = int(args.inner_window)
    outer_window = int(args.outer_window)

    log.set_print_verbosity(PRINT_VERBOSITY)
    log.set_log_verbosity(LOG_VERBOSITY)
    log.enable_colors()

    log.enable_logging()
    makedirs(LOGDIR)
    log.set_logdir(LOGDIR)

    write = not no_write

    log.info("Starting service!")

    run(user, password, host, dbname, schema, hotel_ids, years_back, write=write, plot=make_plots, min_length=min_length, inner_window=inner_window, outer_window=outer_window)
    log.info("Finished!")
    #print("--- %s seconds ---" % (time.time() - start_time))
  except Exception as e:
    log.err(ExceptionHandler(e))
    log.info("Terminated!")
