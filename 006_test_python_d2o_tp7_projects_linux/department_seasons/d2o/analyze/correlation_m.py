# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import pandas as pd
import numpy as np #Used for unittest
from os import path #Used for unittest

from statsmodels.tsa.stattools import acf, pacf
import d2o.utils.logger as log
from d2o.utils.handlers import ExceptionHandler

# TODO: Remove dependency on seaborn, replace with scipy.
# http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
import seaborn as sns

def __clamp(val, _min, _max):
  return max(min(_max, val), _min)

def correlation_matrix(df, method='pearson', cluster=True, abs_values=False, **kwargs):
  # TODO: Remove dependency on seaborn clustermap. Use scipy functions instead.
  """
  Creates and returnes the correlation matrix of the dataframe.

  Parameters
  ----------
  df : pd.DataFrame

  method : str, default 'pearson'
    vallid arguments: 'pearson', 'kendall', 'spearman'.

  cluster : bool, default True
    If True, the matrix will be clustered.

  plot : bool, default False
    Not implemented, function will not plot regardless of this argument.

  abs_values : bool, default False
    If True, returns matrix with absolute values.

  **kwargs
  --------
  sort_method : str, default "average".
    What method to use in sorting/clustering algorithm.

  Returns
  -------
  corrmat : pd.DataFrame
  """
  valid_methods = ['pearson', 'kendall', 'spearman']
  if (method not in valid_methods):
    raise Exception("Invalid method '%s', valid methods are %s" % (method, valid_methods))
  corrmat = df.corr(method=method)
  if abs_values:
    corrmat = corrmat.abs()
  corrmat = corrmat.sort_index(axis=1, ascending=False)
  corrmat = corrmat.sort_index(axis=0, ascending=False)

  if cluster:
    sort_method = kwargs.get("sort_method", "average")
    cg = sns.clustermap(corrmat, method=sort_method, linewidths=.5)
    corrmat = corrmat[cg.dendrogram_col.reordered_ind]
    corrmat = corrmat.reindex(corrmat.columns.values)

  return corrmat

def autocorrelation(df, confidence=95., nlags=None, keys="ALL", detrend=False, plot=False,):
  """
  Does the autocorrelation on each column in the df.

  Parameters
  ----------
  df : pd.DataFrame

  confidence : float
    Default 95.

  nlags :
    Default None

  keys : string or list
    Default "ALL"

  detrend : bool
    Default False

  plot : bool
    Default False

  Returns
  -------
  aco : pd.DataFrame
    Columns contain the aoutocorrelation of the columns in df and the
    confidence interval.
  """
  nlags = int(len(df)/2) if (nlags == None) else nlags
  nlags = __clamp(nlags,0,len(df))-1
  alpha = (100.0 - confidence)/100.0

  try:
    if keys == "ALL":
      keys = df.columns
    elif type(keys) == str:
      keys = [keys]
  except Exception as e:
    log.err(ExceptionHandler(e))

  resdict = {}
  for i,column in enumerate(keys):
    avals, aconfint = acf(df[column].values, nlags=nlags, alpha=alpha)
    aconfint = aconfint - aconfint.mean(1)[:,None]

    resdict[column + '_ACF'] = avals
    resdict[column + '_ACF_CONFINT_LOW'] = aconfint.T[0]
    resdict[column + '_ACF_CONFINT_HIGH'] = aconfint.T[1]

  aco = pd.DataFrame.from_dict(resdict)
  return aco

def partial_autocorr(df, detrend=False, confidence=95., nlags=None, plot=False, keys="ALL"):
  """
  Does the autocorrelation on each column in the df.

  Parameters
  ----------
  df : pd.DataFrame

  detrend : bool
    Default False

  confidence : float
    Default 95.

  nlags :
    Default None

  plot : bool
    Default False

  keys : string or list
    keys to itterate over.

  Returns
  -------
  paco : pd.DataFrame
    Columns contain the partial aoutocorrelation of the columns in df and the
    confidence interval.
  """
  nlags = int(len(df)/2) if (nlags == None) else nlags
  nlags = __clamp(nlags,0,len(df))-1
  alpha = (100.0 - confidence)/100.0

  try:
    if keys == "ALL":
      keys = df.columns
    elif type(keys) == str:
      keys = [keys]
  except Exception as e:
    log.err(ExceptionHandler(e))

  resdict = {}
  for i,column in enumerate(keys):
      pavals, paconfint = pacf(df[column].values, nlags=nlags, alpha=alpha)
      paconfint = paconfint - paconfint.mean(1)[:,None]

      resdict[column + '_PACF'] = pavals
      resdict[column + '_PACF_CONFINT_LOW'] = paconfint.T[0]
      resdict[column + '_PACF_CONFINT_HIGH'] = paconfint.T[1]

  paco = pd.DataFrame.from_dict(resdict)
  return paco

def cross_corr_df(df, keys="ALL", mode="full", n_last_elm=365):
  # TODO: Link to the database table design when completet.
  """
  Cross-correlation of the columns in a data frame.

  Parameters
  ----------
  df : pd.DataFrame
    Data frame with the series to cross-correlate as columns.

  keys : str or list, default "ALL"
    List of strings with the columns to cross-correlate.

  mode : str, default "full"
    This argument is passed to np.correlate. Don't change unless you know what you're doing.

  n_last_elm : int, default 365
    The number of elements to use when doing cross-correlation.
    The default value works for timeseries with sampling frequency (Fs) of one day, or any other timeseries
    with a periodicity of 365*Fs

  Returns
  -------
  ccf_df : pd.DataFrame
    Data frame containing all the cross correlations of the columns.

  See Also
  --------
  numpy.correlate : http://docs.scipy.org/doc/numpy-1.10.0/reference/generated/numpy.correlate.html
  wiki : https://en.wikipedia.org/wiki/Cross-correlation

  Notes
  -----
  A note on complexity: Due to the options available in numpy.correlate this function now does n**2 - n cross-correlations.
  This is to maintain symetry in the data. The following last paragraph
  is true for a optimized algorithm, but would require another library or a rewrite of the cross-correlation algorithm.

  The number of cross-correlations done in this function is (1/2)/(n-1)n , where n is the number of columns.
  If your dataframe has a lot of columns, the returned dataframe will be very large. E.g. 20 columns will produce
  a cross_corr_table containing 190 columns. In general, the returned data frame will be (1/2)(n-1) times larger
  than the input frame.
  """
  cross_corr_dict = {}

  if keys == "ALL":
    columns = df.columns.values
  else:
    columns = keys

  for i in xrange(0, len(columns)):
#    for j in xrange(i+1, len(columns)):
#      temp = np.correlate(df[columns[i]], df[columns[j]], mode=mode)
#      cross_corr_dict[columns[i]+"_cross_"+columns[j]] = temp
#######################################################################
#    for j in xrange(0, len(columns)):
#      if i == j:
#        pass
#      temp = np.correlate(df[columns[i]], df[columns[j]].values[-n_last_elm:-1], mode=mode)
#      cross_corr_dict[columns[i]+"_cross_"+columns[j]] = temp
########################################################################
    for j in xrange(0, len(columns)):
      if i == j:
        pass
      temp = np.correlate(df[columns[i]], df[columns[j]], mode=mode)
      cross_corr_dict[columns[i]+"_cross_"+columns[j]] = temp

  try:
    #Adjusting the index range to show lags in index.
    length_ccf = len(temp)
    lags = range(-length_ccf/2 + 1, length_ccf/2 + 1)
    cross_corr_dict["lag"] = lags
  except Exception as e:
    log.err(ExceptionHandler(e))

  ccf_df = pd.DataFrame.from_dict(cross_corr_dict).set_index("lag")
  return ccf_df

def trim_cross_df(df, index_range):
  # TODO: Implement list functionality for inex_range
  """
  Trims the data frame and returns the elements in index_range.

  Parameters
  ----------
  df : pd.DataFrame
    Dataframe created using cross_corr_df()

  index_range : int or list. List not implemented.
    If int, the function returns the index_range centered elements in df. If list, on the form [A, B], it will
    return the indexes from A to B, including B.

  Returns
  -------
  trimmed_df : pd.DataFrame
    Trimmed data frame.
  """
  length = len(df.index)
  start = length/2 - index_range/2
  stop = start + index_range
  indexes = range(start, stop)

  return df.take(indexes, convert=False)

def transform_cross_df(df):
  """
  Transforms a cross_corr_df to conform to a four column data frame.

  Parameters
  ----------
  df : pd.DataFrame
    Data frame created in cross_corr_df() (remember to normalize input).

  Returns
  -------
  t_df : pd.DataFrame
    Dataframe with columns, [h_id_source, segment_id_source, type_source, h_id_crrelated, segment_id_correlated,
    type_correlated, lag, correlation]
  """
  length_name = 1
  for column in df:
    try:
      a, b = column.split("_cross_")
    except Exception as e:
      log.err(ExceptionHandler(e))
    len_a = len(a); len_b = len(b)
    if len_a > len_b:
      len_str = len_a
    else:
      len_str = len_b
    if len_str > length_name:
      length_name = len_str
  length = len(df.index)

  type_source_name, type_correlated_name = df.columns[0].split("_cross_")

  type_source = np.empty(length, dtype="a"+str(length_name))
  type_correlated = np.empty(length, dtype="a"+str(length_name))
  correlation = df[df.columns[0]].values.copy()
  lags = df.index.values.copy()

  type_source[:] = type_source_name
  type_correlated[:] = type_correlated_name

  for i in xrange(1, len(df.columns)):
#    print df.columns[i]
    type_source_name, type_correlated_name = df.columns[i].split("_cross_")

    temp_type_source = np.empty(length, dtype="a"+str(length_name))
    temp_type_correlated = np.empty(length, dtype="a"+str(length_name))
    temp_lags = df.index.values.copy()
    temp_correlation = df[df.columns[i]].values.copy()

    temp_type_source[:] = type_source_name
    temp_type_correlated[:] = type_correlated_name

    type_source = np.concatenate((type_source, temp_type_source))
    type_correlated = np.concatenate((type_correlated, temp_type_correlated))
    lags = np.concatenate((lags, temp_lags))
    correlation = np.concatenate((correlation, temp_correlation))

  temp_dict = {"type_source":type_source,
               "type_correlated":type_correlated,
               "lags":lags,
               "correaltion":correlation}
  return pd.DataFrame.from_dict(temp_dict)

def transform_cross_df_d2o(df, n_values="ALL"):
  # TODO: Exeption handeling when column names don't conform to the standard h_id + segment_id + type
  """
  Transforms a cross_corr_df to conform to the d2o database table structure for
  correlations.

  Parameters
  ----------
  df : pd.DataFrame
    Data frame created in cross_corr_df() (remember to normalize input).

  n_values : int or str, default "ALL"
    Number of values to keep. If int, the table returned will only contain the n_values highest correlations
    pr. cross_correlation.

  Returns
  -------
  ret : pd.DataFrame
    Dataframe with columns, [h_id_source, segment_id_source, type_source, h_id_crrelated, segment_id_correlated,
    type_correlated, lag, correlation]
  """
  length_name = 1
  for column in df:
    try:
      a, b = column.split("_cross_")
    except Exception as e:
      log.err(ExceptionHandler(e))
    len_a = len(a); len_b = len(b)
    if len_a > len_b:
      len_str = len_a
    else:
      len_str = len_b
    if len_str > length_name:
      length_name = len_str
  length = len(df.index)

  if n_values == "ALL":
    n_values = length

  source_name, correlated_name = df.columns[0].split("_cross_")
  h_id_source_name, segment_id_source_name, type_source_name = source_name.split("_")
  h_id_corr_name, segment_id_corr_name, type_corr_name = correlated_name.split("_")

  h_id_source = np.empty(n_values, dtype="U"+str(length_name))
  segment_id_source = np.empty(n_values, dtype="U"+str(length_name))
  type_source = np.empty(n_values, dtype="U"+str(length_name))

  h_id_correlated = np.empty(n_values, dtype="U"+str(length_name))
  segment_id_correlated = np.empty(n_values, dtype="U"+str(length_name))
  type_correlated = np.empty(n_values, dtype="U"+str(length_name))

#  correlation = df[df.columns[0]].values.copy()
#  lags = df.index.values.copy()

  indexes = _get_n_highest(df[df.columns[0]].values, n_values)
  correlation = df[df.columns[0]].values[indexes]
  lags = df.index.values[indexes]

  h_id_source[:] = h_id_source_name
  segment_id_source[:] = segment_id_source_name
  type_source[:] = type_source_name

  h_id_correlated[:] = h_id_corr_name
  segment_id_correlated[:] = segment_id_corr_name
  type_correlated[:] = type_corr_name

  for i in xrange(1, len(df.columns)):
#    print df.columns[i]
    source_name, correlated_name = df.columns[i].split("_cross_")
    try:
      h_id_source_name, segment_id_source_name, type_source_name = source_name.split("_")
      h_id_corr_name, segment_id_corr_name, type_corr_name = correlated_name.split("_")
    except Exception as e:
      log.err('%s\n%s' % (ExceptionHandler(e), source_name.split('_')))
      pass

    temp_h_id_source = np.empty(n_values, dtype="U"+str(length_name))
    temp_segment_id_source = np.empty(n_values, dtype="U"+str(length_name))
    temp_type_source = np.empty(n_values, dtype="U"+str(length_name))

    temp_h_id_correlated = np.empty(n_values, dtype="U"+str(length_name))
    temp_segment_id_correlated = np.empty(n_values, dtype="U"+str(length_name))
    temp_type_correlated = np.empty(n_values, dtype="U"+str(length_name))

# TODO: Retrive only n_values
#    temp_lags = df.index.values.copy()
#    temp_correlation = df[df.columns[i]].values.copy()

    indexes = _get_n_highest(df[df.columns[i]].values, n_values)
    temp_correlation = df[df.columns[i]].values[indexes]
    temp_lags = df.index.values[indexes]

    temp_h_id_source[:] = h_id_source_name
    temp_segment_id_source[:] = segment_id_source_name
    temp_type_source[:] = type_source_name

    temp_h_id_correlated[:] = h_id_corr_name
    temp_segment_id_correlated[:] = segment_id_corr_name
    temp_type_correlated[:] = type_corr_name

    h_id_source = np.concatenate((h_id_source, temp_h_id_source))
    segment_id_source = np.concatenate((segment_id_source, temp_segment_id_source))
    type_source = np.concatenate((type_source, temp_type_source))

    h_id_correlated = np.concatenate((h_id_correlated, temp_h_id_correlated))
    segment_id_correlated = np.concatenate((segment_id_correlated, temp_segment_id_correlated))
    type_correlated = np.concatenate((type_correlated, temp_type_correlated))

    lags = np.concatenate((lags, temp_lags))
    correlation = np.concatenate((correlation, temp_correlation))

  temp_dict = {"h_id_source":h_id_source,
               "segment_id_source":segment_id_source,
               "type_source":type_source,
               "h_id_correlated":h_id_correlated,
               "segment_id_correlated":segment_id_correlated,
               "type_correlated":type_correlated,
               "lag":lags,
               "correlation":correlation}

  # TODO: Remove duplicates / mirrored correlations

  ret = pd.DataFrame.from_dict(temp_dict)
  # if (n_values != "ALL"):
  #   ret = ret.groupby(['h_id_source', 'segment_id_source']).head(n_values)
  return ret

def _get_n_highest(narray, n):
  """
  Find the n highest values and return the indexes.

  Parameters
  ----------
  narray : np.array
    Array to search for highest values.

  n : int
    Number of indexes to return.

  Returns:
  --------
  indexes : list
    Indexes containing the n highest values.

  Notes
  -----
  narray[indexes] will return the n highest values in narray.
  """
  temp = (-narray).argsort()
  indexes = temp[:n]
  return indexes


#TESTING
import unittest
# TODO: Implement tests for autocorrelation and artial autocorr.
class CorrealtionTests(unittest.TestCase):
  def setUp(self):
    print "setUP: correlation.py, need robust solution to file path"
    x = np.linspace(0, 2*np.pi, 50)
    keys = ["x", "np.sin(x)", "np.cos(x)", "np.tan(x)", "np.abs(x)"]
    temp_dict = {}
    for key in keys:
      temp_dict[key] = eval(key)
    self.df = pd.DataFrame.from_dict(temp_dict).set_index("x")

    file_path = get_path_to_datasett("correlation_matrix_test_data.csv")
    self.true_corr_mat = pd.read_csv(file_path, index_col=0)

  def test_correlation_matrix(self):
    print "test_correlation_matrix: correlation.py"
    corr_mat = correlation_matrix(self.df)
    test_result = True # If changed to False, this test fails.
    epsilon = 1e-12 # Error tolerance.
    for key in corr_mat:
      for i in xrange(len(corr_mat[key].values)):
        temp_var = abs(corr_mat[key][i]-self.true_corr_mat[key][i])
        if temp_var > epsilon:
          test_result = False
          break
      if test_result == False:
        break

    self.assertTrue(test_result, msg="No definitive error msg created")

  def test_autocorrelation(self):
    print "test_autocorrealtion: correaltion.py"
    self.assertTrue(False, msg="Test not implemented")

  def test_partial_autocorr(self):
    print "test_partial_autocorr: correlation.py"
    self.assertTrue(False, msg="Test not implemented")


if __name__ == "__main__":
  unittest.main()

  # TODO: Replace by unittest module. (partialy complete)
  """
  print "Executing test block..."
  import numpy as np
  import matplotlib.pyplot as plt
  import time

  x = np.linspace(0, 10*np.pi, 1000)

  y1 = np.sin(x)
  y2 = x**2

  testFrame = pd.DataFrame.from_dict({"var_1" : y1, "var_2": y2})
  # TODO: Test of autocorrelation()
  # TODO: Test of correlation_matrix()

  fig1 = plt.figure()
  ax = fig1.add_subplot(3, 1, 1)
  line = ax.plot(x, y1, label="sin(x)")
  ax.legend()

  #Test of partialAutoCorr()
  print "Testing partialAutoCorr()"
  PACStart = time.clock()
  PACFrame = partial_autocorr(testFrame)
  PACFinished = time.clock()
  PACTime = PACFinished - PACStart
  print "Time used in partial_autocorr():", PACTime

  ax1 = fig1.add_subplot(3, 1, 2)
  line1 = ax1.plot(PACFrame["var_1_PACF"], '*',
                   PACFrame["var_1_PACF_CONFINT_LOW"], 'r-',
                   PACFrame["var_1_PACF_CONFINT_HIGH"], 'r-')
  #ax1.legend()

  #Test of autocorrelation
  print "Testing autocorrelation()"
  ACStart = time.clock()
  ACFrame = autocorrelation(testFrame)
  ACFinished = time.clock()
  ACTime = ACFinished - ACStart
  print "Time used in autocorrelation():", ACTime

  ax2 = fig1.add_subplot(3, 1, 3)
  line2 = ax2.plot(ACFrame["var_1_ACF"], '*',
                   ACFrame["var_1_ACF_CONFINT_LOW"], 'r-',
                   ACFrame["var_1_ACF_CONFINT_HIGH"], 'r-')
  #ax2.legend()

  #Drawing the plots.
  print "Drawing plot."
  fig1.canvas.draw()
  plt.show()

  #Output:
  """
  """
  $ python correlation.py
  Executing test block...
  Testing partialAutoCorr()
  Time used in partialAutoCorr(): 24.03003
  Testing autocorrelation()
  Time used in autocorrelation(): 0.007206
  Drawing plot.
  """


