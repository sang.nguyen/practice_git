# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import pandas as pd
import numpy as np #Used for unittest
from os import path #Used for unittest

from statsmodels.tsa.stattools import acf, pacf
import d2o.utils.logger as log
from d2o.utils.putils import get_path_to_dataset

# TODO: Remove dependency on seaborn, replace with scipy.
# http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
import seaborn as sns

def __clamp(val, _min, _max):
  return max(min(_max, val), _min)

def correlation_matrix(df, method='pearson', sort=True, abs_values=False, **kwargs):
  # TODO: Remove dependency on seaborn clustermap. Use scipy functions instead.
  """
  Creates and returns the correlation matrix of the dataframe.

  Parameters
  ----------
  df : pd.DataFrame

  method : str, default 'pearson'
    vallid arguments: 'pearson', 'kendall', 'spearman'.

  sort : bool, default True
    not implemented, does nothing. Indexes will be sorted regardless of this argument.

  plot : bool, default False
    Not implemented, function will not plot regardless of this argument.

  abs_values : bool, default False
    If True, returns matrix with absolute values.

  **kwargs
  --------
  sort_method : str, default "average".
    Which method to use in sorting/clustering algorithm.

  Returns
  -------
  corrmat : pd.DataFrame
  """
  valid_methods = ['pearson', 'kendall', 'spearman']
  if (method not in valid_methods):
    raise Exception("Invalid method '%s', valid methods are %s" % (method, valid_methods))
  corrmat = df.corr(method=method)
  if abs_values:
    corrmat = corrmat.abs()
  corrmat = corrmat.sort_index(axis=1, ascending=False)
  corrmat = corrmat.sort_index(axis=0, ascending=False)

  if sort:
    sort_method = kwargs.get("sort_method", "average")
    cg = sns.clustermap(corrmat, method=sort_method, linewidths=.5)
    corrmat = corrmat[cg.dendrogram_col.reordered_ind]
    corrmat = corrmat.reindex(corrmat.columns.values)

  return corrmat

def autocorrelation(df, confidence=95., nlags=None, keys="ALL", detrend=False, plot=False,):
  """
  Calculates autocorrelation for each column in df.

  Parameters
  ----------
  df : pd.DataFrame

  confidence : float
    Default 95.

  nlags :
    Default None

  keys : string or list
    Default "ALL"

  detrend : bool
    Default False

  plot : bool
    Default False

  Returns
  -------
  aco : pd.DataFrame
    Columns contains aoutocorrelation of the columns in df with the
    confidence interval represented as two lines.
  """
  nlags = int(len(df)/2) if (nlags == None) else nlags
  nlags = __clamp(nlags,0,len(df))-1
  alpha = (100.0 - confidence)/100.0

  try:
    if keys == "ALL":
      keys = df.columns
    elif type(keys) == str:
      keys = [keys]
  except Exception as e:
    log.err(ExceptionHandler(e))

  resdict = {}
  for i,column in enumerate(keys):
    avals, aconfint = acf(df[column].values, nlags=nlags, alpha=alpha)
    aconfint = aconfint - aconfint.mean(1)[:,None]

    resdict[column + '_ACF'] = avals
    resdict[column + '_ACF_CONFINT_LOW'] = aconfint.T[0]
    resdict[column + '_ACF_CONFINT_HIGH'] = aconfint.T[1]

  aco = pd.DataFrame.from_dict(resdict)
  return aco

def partial_autocorr(df, detrend=False, confidence=95., nlags=None, plot=False, keys="ALL"):
  """
  Calculates partial autocorrelation for each column.

  Parameters
  ----------
  df : pd.DataFrame

  detrend : bool
    Default False

  confidence : float
    Default 95.

  nlags :
    Default None

  plot : bool
    Default False

  keys : string or list
    keys to iterate over.

  Returns
  -------
  paco : pd.DataFrame
    Dataframe with the partial autocorrelation per column, and the upper and
    lower bounds of the confidence interval as two lines.
  """
  nlags = int(len(df)/2) if (nlags == None) else nlags
  nlags = __clamp(nlags,0,len(df))-1
  alpha = (100.0 - confidence)/100.0

  try:
    if keys == "ALL":
      keys = df.columns
    elif type(keys) == str:
      keys = [keys]
  except Exception as e:
    log.err(ExceptionHandler(e))

  resdict = {}
  for i,column in enumerate(keys):
      pavals, paconfint = pacf(df[column].values, nlags=nlags, alpha=alpha)
      paconfint = paconfint - paconfint.mean(1)[:,None]

      resdict[column + '_PACF'] = pavals
      resdict[column + '_PACF_CONFINT_LOW'] = paconfint.T[0]
      resdict[column + '_PACF_CONFINT_HIGH'] = paconfint.T[1]

  paco = pd.DataFrame.from_dict(resdict)
  return paco


#TESTING
import unittest
# TODO: Implement tests.
class CorrealtionTests(unittest.TestCase):
  def setUp(self):
    print "setUP: correlation.py, need robust solution to file path"
    x = np.linspace(0, 2*np.pi, 50)
    keys = ["x", "np.sin(x)", "np.cos(x)", "np.tan(x)", "np.abs(x)"]
    temp_dict = {}
    for key in keys:
      temp_dict[key] = eval(key)
    self.df = pd.DataFrame.from_dict(temp_dict).set_index("x")

    file_path = get_path_to_datasett("correlation_matrix_test_data.csv")
    self.true_corr_mat = pd.read_csv(file_path, index_col=0)

  def test_correlation_matrix(self):
    print "test_correlation_matrix: correlation.py"
    corr_mat = correlation_matrix(self.df)
    test_result = True # If changed to False, this test fails.
    epsilon = 1e-12 # Error tolerance.
    for key in corr_mat:
      for i in xrange(len(corr_mat[key].values)):
        temp_var = abs(corr_mat[key][i]-self.true_corr_mat[key][i])
        if temp_var > epsilon:
          test_result = False
          break
      if test_result == False:
        break

    self.assertTrue(test_result, msg="No definitive error msg created")

  def test_autocorrelation(self):
    print "test_autocorrealtion: correaltion.py"
    self.assertTrue(False, msg="Test not implemented")

  def test_partial_autocorr(self):
    print "test_partial_autocorr: correlation.py"
    self.assertTrue(False, msg="Test not implemented")


if __name__ == "__main__":
  unittest.main()

  # TODO: Replace by unittest module.
  """
  print "Executing test block..."
  import numpy as np
  import matplotlib.pyplot as plt
  import time

  x = np.linspace(0, 10*np.pi, 1000)

  y1 = np.sin(x)
  y2 = x**2

  testFrame = pd.DataFrame.from_dict({"var_1" : y1, "var_2": y2})
  # TODO: Test of autocorrelation()
  # TODO: Test of correlation_matrix()

  fig1 = plt.figure()
  ax = fig1.add_subplot(3, 1, 1)
  line = ax.plot(x, y1, label="sin(x)")
  ax.legend()

  #Test of partialAutoCorr()
  print "Testing partialAutoCorr()"
  PACStart = time.clock()
  PACFrame = partial_autocorr(testFrame)
  PACFinished = time.clock()
  PACTime = PACFinished - PACStart
  print "Time used in partial_autocorr():", PACTime

  ax1 = fig1.add_subplot(3, 1, 2)
  line1 = ax1.plot(PACFrame["var_1_PACF"], '*',
                   PACFrame["var_1_PACF_CONFINT_LOW"], 'r-',
                   PACFrame["var_1_PACF_CONFINT_HIGH"], 'r-')
  #ax1.legend()

  #Test of autocorrelation
  print "Testing autocorrelation()"
  ACStart = time.clock()
  ACFrame = autocorrelation(testFrame)
  ACFinished = time.clock()
  ACTime = ACFinished - ACStart
  print "Time used in autocorrelation():", ACTime

  ax2 = fig1.add_subplot(3, 1, 3)
  line2 = ax2.plot(ACFrame["var_1_ACF"], '*',
                   ACFrame["var_1_ACF_CONFINT_LOW"], 'r-',
                   ACFrame["var_1_ACF_CONFINT_HIGH"], 'r-')
  #ax2.legend()

  #Drawing the plots.
  print "Drawing plot."
  fig1.canvas.draw()
  plt.show()

  #Output:
  """
  """
  $ python correlation.py
  Executing test block...
  Testing partialAutoCorr()
  Time used in partialAutoCorr(): 24.03003
  Testing autocorrelation()
  Time used in autocorrelation(): 0.007206
  Drawing plot.
  """


