# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import numpy as np
import pandas as pd

def fftdf(dataFrame, keys="ALL"):
  """
  Does a fast fourier transform on the columns in a dataframe. All columns
  need to contain floats except index column.

  Parameters
  ----------
  dataFrame : pd.DataFrame

  keys : string or list, default "ALL".
    What keys to itterate over in DataFrame.

  Returns
  -------
  fftDF : pd.DataFrame
    Data Frame containing the absolute value of the fourier transformation of
    the columns in dataFrame.
  """
  fftDF = {}
  length = dataFrame.shape[0]
  if keys == "ALL":
    keys = dataFrame.columns.values.tolist()
  elif type(keys) == str:
    keys = [keys]
  for key in keys:
    transformed = np.fft.fft(dataFrame[key].values)[0:length/2]
    fftDF[key] = np.abs(transformed)

  return pd.DataFrame.from_dict(fftDF)


#import of putils need to be worked out diffrently in the test block. does not work now.
if __name__ == "__main__":
  import matplotlib.pyplot as plt

  #import sys
  #sys.path.append("~/Documents/chronos/clib/utils")
  #import putils
  #import imp
  #imp.load_source("putils", "~/Documents/chronos/clib/utils/putils")
  #import d2o.utils.putils as putils

  hotel = "HotelBristol"
  path = "~/Documents/chronos/d2o-datasett/" + hotel + ".csv"

  #testFrame = pd.read_csv("~/Documents/chronos/d2o-datasett/BritanniaHotel.csv", sep=";")
  testFrame = pd.read_csv(path, sep=";", index_col="Date")

#  print testFrame.columns.values.tolist()

#  testFrameMav = putils.add_mav(testFrame, 10)
  testFrameMav = testFrame

#  figure = plt.figure()
#  ax = figure.add_subplot(1, 1 ,1)
##  ax.plot(testFrameMav["Total_RV_MAV(10)"][365:400].values)
#  ax.plot(testFrameMav["Total_RV"][365:400].values)
#  figure.show()
#
#  raw_input("enter: ")
#

  length = testFrameMav.shape[0]
  index = length - 365*2
#  print length, index
  testFrameMav = testFrameMav.ix[index:]
#  print testFrameMav
  otherFrame = fftdf(testFrameMav, keys=["Total_RV", "Total_RN"])
  print otherFrame

  fig = plt.figure()
  fig.suptitle(hotel, fontsize=20)
  ax = fig.add_subplot(1, 1, 1)
  ax.plot(otherFrame["Total_RV"][:-otherFrame.shape[0]/2], label="Total_RV")
#  ax.plot(otherFrame["Total_RV_MAV(10)"][:-otherFrame.shape[0]/2], label="MAV")
  ax.grid(b=True)
  ax.legend()

  fig.show()

  #testFrame.plot()

  raw_input("something")