"""
Copyright: TP7
"""

from __future__ import print_function
from d2o.utils import systools
import errno
import os

def home_dirs():
  hdirs = [d for d in os.listdir('/home') if os.path.isdir(os.path.join('/home', d))]
  return hdirs

CUR_USER = os.path.dirname(os.path.realpath(__file__)).split('/home/')[1].split('/')[0]

user = raw_input("Install for user (default: %s): " % CUR_USER)
user = CUR_USER if (user in ['y','Y', ""]) else user

if (user not in home_dirs()):
  raise Exception("User /home/%s does not exist" % user)

COLORS_CONF_CONTENTS = """##
# tp7_colors.conf
#
# Color map for season coloring as hex values.
# NOTE:
#   Do not prefix the hex values with '#',
#   as it will be recognized as a comment.
##

[peak]
expensive = fffde2
above = fffbc9
normal = f3d7b5
below = db947f
cheap = a3376f

[high]
expensive = dae3fe
above = b5c1f1
normal = 7591e4
below = 5757a9
cheap = 382958

[medium]
expensive = e3fec8
above = c0e79c
normal = 9ede60
below = 80a55f
cheap = 355135

[low]
expensive = fee2b9
above = febc70
normal = fe8f32
below = d85013
cheap = a0300e

[dip]
expensive = fecbd5
above = fd8ea0
normal = fc2f4d
below = c92037
cheap = 5e1e2e
"""

CONFIG_DIR = '/home/{0}/.chronos/department_seasons/conf/'.format(user)
COLORS_CONF_PATH = CONFIG_DIR + 'tp7_colors.conf'

for path in [CONFIG_DIR]:
  if not os.path.exists(path):
    os.makedirs(path)

# Creating default configuration files in path
def create_default(path, contents):
  try:
    if (systools.file_exists(path)):
      print("Configuration file <%s> already exists" % path)
      yn = raw_input("Overwrite? [y/n]:").lower()
      if (yn not in ['yes','y','ye']):
        return
      else:
        fb = open(path, 'r')
        bcontents = fb.read()
        print(bcontents, file=open(path + '.bak', 'w'))

    f = open(path, 'w')
    print(contents, file=f)
    f.close()
    print("Created default configuration file: %s" % path)
  except Exception as e:
    print(e)

try:
  create_default(COLORS_CONF_PATH, COLORS_CONF_CONTENTS)
  print("Installed to /home/%s" % user)
  print("IMPORTANT: You need to manually configure the files in %s" % CONFIG_DIR)
except Exception as e:
  if ('permission denied' in str(e).lower()):
    print("You need to run setup.py as root")
    exit()
