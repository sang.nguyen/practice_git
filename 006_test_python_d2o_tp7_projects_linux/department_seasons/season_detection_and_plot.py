"""
Copyright: TP7
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
import datetime

# Duplicate level to 366 values
def seasonal_level(period, level):    
    frequence = np.subtract(period[1:], period[:-1])
    frequence[0] = frequence[0] + 1
    
    frequence = np.array(frequence, int)
    level_list = np.repeat(level, frequence)
    return level_list

def mystd(x):
    n = len(x)
    kq = np.std(x) * np.sqrt(n/float(n-1))
    return kq

# Find turning point without checking length
def separate(x, min_days):
    n = len(x)
    kq = 0
    gain = 0
    for i in range(min_days, (n - (min_days-1))):
        p = (i+1)/float(n)
        temp = mystd(x) - (p*mystd(x[0:i+1]) + (1-p)*mystd(x[i+1:]))
        if gain < temp:
            gain = temp
            kq = i
    return kq # No turning point return 0 by default

# Set level for data
def setlevel(x,t,level):
    '''
    t is ok without connect the first interval to the end, not the case below.
    The case below will be proccessed directly in the next function
    t= [0   4  25  46  67  88 109 129 150 171 192 213 234 255 276 297 318 339 365]
    NOTICE: NOT process t=[0 0 25 .... 365]
    '''
    l = np.zeros(len(t)-1)
    t = t[1:]
    m = len(t)
    y = x[0:t[0]]
    for j in range(0, m):
        gain_min = 100000
        for i in range(0, len(level)):
            temp = np.median(np.absolute(np.subtract(y, level[i])))
            if temp < gain_min:
                gain_min = temp
                l[j] = level[i]
        if j < m-1:
            y = x[t[j]+1:t[j+1]+1]
    return l

# Set level for revenue data
def setlevel_rev(rev,t,level):
    if t[1]==0:
        t=np.unique(t)
        l= setlevel(rev,t,level)
        return l
    else:
        x=rev
        xlast = np.append(x[t[-2]+1 : ], x[: t[-2]+1])
        t_new=np.add(t,t[-1]-t[-2])
        t_new[0]=0
        t_new=t_new[:-1]
        l_temp= setlevel(xlast,t_new,level)
        
        # Cut the firt to end, bring back 26
        l= np.append(l_temp,l_temp[0])
        return l


def next_interval(x, t, min_days): 
    # x = PC1, t= list of turning point 
    y = []
    position = []
    m = len(t)
    stdmax = -10**9
    position = 0
    for i in range(m-1):
        if i == 0:
            temp = x[t[i] : t[i+1]+1]
        else:
            temp = x[t[i]+1 : t[i+1]+1]
            
        if mystd(temp)>stdmax and len(temp)>= min_days*2:
            stdmax = mystd(temp)
            y = temp
            position = i
    return (y, position)
 
# Detect seasonal 
def season_detection(x, level, max_period, min_days):
    max_period=max_period+2
    x = np.true_divide(np.subtract(x, np.min(x)), (np.max(x) - np.min(x)))
    stop = 0

    t = list()
    t.extend([0, separate(x, min_days), len(x)-1])
    
    if t[1]==0 or t[1] == (len(x) - min_days):
        stop = 1
        t=np.unique(t)
    xlast = np.append(x[t[-2]+1 : ], x[: t[-2]+1])

    t2 = list()
    t2.extend([0, separate(xlast, min_days), len(xlast)-1])

    if t2[1]==0 or t2[1] == (len(xlast) - min_days):
        stop = 1
        
    while stop == 0 and len(t2) <= max_period:
        (y2,position2) = next_interval(xlast, t2, min_days)
        temp2 = separate(y2, min_days)
        if temp2 == 0 or temp2 == t2[position2]+1:
            break
        if t2[position2] == 0:
            temp2 = temp2 + t2[position2]
        else:
            temp2 = temp2 + t2[position2] + 1
        t2.append(temp2)
        t2 = sorted(t2)

    t_new=np.subtract(t2, (t[-1]-t[-2]))
    t_new=t_new[1:]
    while t_new[0]<0: # Bring to the end
        t_new=np.append(t_new,365+t_new[0])
        t_new=t_new[1:]
    t_new[0]=0
    t_new=np.append(t_new,365)

    l2 = setlevel_rev(x, t_new, level)
    return t_new, l2

# Plot 2 subplots in the firgue
def comp_2_plots(d2o_interval, tp7_interval, d2o_level, tp7_level,
                 min_days_above, min_days_below, max_period_below,
                 df, df_val1, df_val2, hotel_ids, main):
    vmin = 0.02
    vmax = 0.98
    d2o_season_level = seasonal_level(d2o_interval, d2o_level)
    tp7_season_level = seasonal_level(tp7_interval, tp7_level)
    df['d2o_season_level'] = d2o_season_level
    df['tp7_season_level'] = tp7_season_level
    df['Date'] = pd.to_datetime(df['Date'])

    plt.subplots_adjust(wspace=11, hspace=1, right = 0.75, top=0.95, bottom=0.2)
    fig = plt.figure(figsize=(25,20),facecolor='w')
    my_cmap = [
        '#97040c', '#9d4b00', '#d4b400', '#0515a9', '#026f02',
        '#a0300e', '#d85013', '#ffdd1c', '#454ea0', '#20a308',
        '#d8017a', '#fc6621', '#f1ef00', '#3d8ddc', '#3cc923',
        '#fc4eb0', '#fb9262', '#fffe69', '#3fcdfd', '#88d63c',
        '#ff96d1', '#ffc0a4', '#fffeab', '#9de6fe', '#98edb5'
    ]
    my_cmap_name = ['dip_1', 'dip_2', 'dip_3', 'dip_4', 'dip_5',
                    'low_1', 'low_2', 'low_3', 'low_4', 'low_5',
                    'medium_1', 'medium_2', 'medium_3', 'medium_4', 'medium_5',
                    'high_1', 'high_2', 'high_3', 'high_4', 'high_5',
                    'peak_1', 'peak_2', 'peak_3', 'peak_4', 'peak_5']
    import matplotlib
    cmap1 = matplotlib.colors.ListedColormap(my_cmap)

    ax1 = fig.add_subplot(211)    
    # Count number of seasons
    num_periods = list(set(df['d2o_season_level'].values[np.newaxis][0]))  # Drop duplicates

    Total_Revenue, = plt.plot(df['Date'], df[df_val2], color = 'y', label = 'Total_Revenue')
    PC1, = plt.plot(df['Date'], df[df_val1], color = 'r', label = df_val1)
    Revenue_Level, = plt.plot(df['Date'], df['d2o_season_level'], color = 'b', label = 'Revenue_Level')

    plt.title(main, fontsize=20)
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days='+str(min_days_above)+'__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)

    if np.abs(sum(df[df_val1] - df[df_val2])) < 1e-9:
        legend_name = mpatches.Patch(label='PC1 collapses to Total Revenue', alpha=0)
        plt.legend(handles=[legend_name, PC1, Revenue_Level],
                               bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)
    else:
        plt.legend(handles=[PC1, Total_Revenue, Revenue_Level],
                   bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)

    plt.setp(ax1.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax1.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax1.pcolorfast(ax1.get_xlim(), ax1.get_ylim(), df['d2o_season_level'].values[np.newaxis], 
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)
    
    ax2 = fig.add_subplot(212)
    # Count number of seasons
    num_periods = list(set(df['tp7_season_level'].values[np.newaxis][0]))  # Drop duplicates

    plt.plot(df['Date'], df[df_val2], color = 'y', label = 'Total_Revenue')
    plt.plot(df['Date'], df[df_val1], color = 'r', label = df_val1)
    plt.plot(df['Date'], df['tp7_season_level'], color = 'b', label = 'Revenue_Level')
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days='+str(min_days_below)+'__Max_period='+str(max_period_below)+'  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    plt.setp(ax2.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax2.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax2.pcolorfast(ax2.get_xlim(), ax2.get_ylim(), df['tp7_season_level'].values[np.newaxis], 
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)
                   
    # Display legends
    myTotalList = list(df['d2o_season_level'].values[np.newaxis][0]) \
                  + list(df['tp7_season_level'].values[np.newaxis][0])
    legend_value = sorted(list(set(myTotalList)))
    legend_value = [round(elem, 2) for elem in legend_value]
    value_legend = 0.02
    legend_j = 0
    legend_handles = []
    for i in range(0, len(legend_value)):
        while abs(legend_value[i] - value_legend) > 1e-9:
            value_legend = value_legend + 0.04
            legend_j = legend_j + 1
        legend_handles.append(mpatches.Patch(color=cmap1.colors[legend_j], label=my_cmap_name[legend_j], alpha=0.6))
        value_legend = 0.02
        legend_j = 0
    plt.legend(handles=legend_handles, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)

    plt.savefig(hotel_ids + '_' + main + '_21day_NoMax_vs_8_.png', bbox_inches='tight',dpi=100)

# Plot 3 subplots in the firgue
def comp_3_plots(d2o_interval, tp7_interval, d2o_level, tp7_level, medium_interval, medium_level,
                 min_days_above, min_days_medium, min_days_below, df, df_val1, df_val2, hotel_ids, main):
    vmin = 0.02
    vmax = 0.98
    d2o_season_level = seasonal_level(d2o_interval, d2o_level)
    tp7_season_level = seasonal_level(tp7_interval, tp7_level)
    medium_season_level = seasonal_level(medium_interval, medium_level)
    df['d2o_season_level'] = d2o_season_level
    df['tp7_season_level'] = tp7_season_level
    df['medium_season_level'] = medium_season_level
    df['Date'] = pd.to_datetime(df['Date'])

    plt.subplots_adjust(wspace=15, hspace=0.8, right = 0.75, top=0.95, bottom=0.2)
    fig = plt.figure(figsize=(30,25),facecolor='w')
    my_cmap = [
    '#97040c', '#9d4b00', '#d4b400', '#0515a9', '#026f02',
    '#a0300e', '#d85013', '#ffdd1c', '#454ea0', '#20a308',
    '#d8017a', '#fc6621', '#f1ef00', '#3d8ddc', '#3cc923',
    '#fc4eb0', '#fb9262', '#fffe69', '#3fcdfd', '#88d63c',
    '#ff96d1', '#ffc0a4', '#fffeab', '#9de6fe', '#98edb5'
    ]
    my_cmap_name = ['dip_1', 'dip_2', 'dip_3', 'dip_4', 'dip_5',
                    'low_1', 'low_2', 'low_3', 'low_4', 'low_5',
                    'medium_1', 'medium_2', 'medium_3', 'medium_4', 'medium_5',
                    'high_1', 'high_2', 'high_3', 'high_4', 'high_5',
                    'peak_1', 'peak_2', 'peak_3', 'peak_4', 'peak_5']
    import matplotlib
    cmap1 = matplotlib.colors.ListedColormap(my_cmap)

    ax1 = fig.add_subplot(311)
    # Count number of seasons
    num_periods = list(set(df['d2o_season_level'].values[np.newaxis][0]))  # Drop duplicates

    Total_Revenue, = plt.plot(df['Date'], df[df_val2], color = 'y', label = 'Total_Revenue')
    PC1, = plt.plot(df['Date'], df[df_val1], color = 'r', label = df_val1)
    Revenue_Level, = plt.plot(df['Date'], df['d2o_season_level'], color = 'b', label = 'Revenue_Level')

    plt.title(main, fontsize=20)
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days='+str(min_days_above)+'__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    if np.abs(sum(df[df_val1]-df[df_val2]))<1e-9:
        legend_name = mpatches.Patch(label='PC1 collapses to Total Revenue', alpha=0)
        plt.legend(handles=[legend_name, PC1, Revenue_Level],
                               bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)
    else:
        plt.legend(handles=[PC1, Total_Revenue, Revenue_Level],
                   bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)

    plt.setp(ax1.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax1.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax1.pcolorfast(ax1.get_xlim(), ax1.get_ylim(), df['d2o_season_level'].values[np.newaxis],
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)
    
    ax2 = fig.add_subplot(312)
    # Count number of seasons
    num_periods = list(set(df['medium_season_level'].values[np.newaxis][0]))  # Drop duplicates

    plt.plot(df['Date'], df[df_val2], color = 'y', label = 'Total_Revenue')
    plt.plot(df['Date'], df[df_val1], color='r', label=df_val1)
    plt.plot(df['Date'], df['medium_season_level'], color = 'b', label = 'Revenue_Level')
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days='+str(min_days_medium)+'__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    plt.setp(ax2.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax2.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax2.pcolorfast(ax2.get_xlim(), ax2.get_ylim(), df['medium_season_level'].values[np.newaxis], 
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)
                   
    # Display legends
    myTotalList = list(df['d2o_season_level'].values[np.newaxis][0])\
                  + list(df['medium_season_level'].values[np.newaxis][0])\
                  + list(df['tp7_season_level'].values[np.newaxis][0])
    legend_value = sorted(list(set(myTotalList)))
    legend_value = [round(elem, 2) for elem in legend_value]
    value_legend = 0.02
    legend_j = 0
    legend_handles = []
    for i in range(0, len(legend_value)):
        while abs(legend_value[i] - value_legend) > 1e-9:
            value_legend = value_legend + 0.04
            legend_j = legend_j + 1
        legend_handles.append(mpatches.Patch(color=cmap1.colors[legend_j], label=my_cmap_name[legend_j], alpha=0.6))
        value_legend = 0.02
        legend_j = 0
    plt.legend(handles=legend_handles, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)

    ax3 = fig.add_subplot(313)
    # Count number of seasons
    num_periods = list(set(df['tp7_season_level'].values[np.newaxis][0]))  # Drop duplicates

    plt.plot(df['Date'], df[df_val2], color = 'y', label = 'Total_Revenue')
    plt.plot(df['Date'], df[df_val1], color='r', label=df_val1)
    plt.plot(df['Date'], df['tp7_season_level'], color = 'b', label = 'Revenue_Level')
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days='+str(min_days_below)+'__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    plt.setp(ax3.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax3.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax3.pcolorfast(ax3.get_xlim(), ax3.get_ylim(), df['tp7_season_level'].values[np.newaxis], 
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)

    plt.savefig(hotel_ids + '_' + main + '_14_21_28days_NoMaxPeriod.png', bbox_inches='tight', dpi=100)

# Plot 3 subplots (include lines) in the firgue
def comp_3_plots_3lines(level, above_interval, below_interval, medium_interval,
                        above_level_rn, medium_level_rn, below_level_rn,
                        above_level_rate, medium_level_rate, below_level_rate,
                        min_days_above, min_days_medium, min_days_below,
                        df, df_val1, df_val2, df_val3, hotel_ids, main):
    vmin = 0.02
    vmax = 0.98
    above_season_level_rn = seasonal_level(above_interval, above_level_rn)
    medium_season_level_rn = seasonal_level(medium_interval, medium_level_rn)
    below_season_level_rn = seasonal_level(below_interval, below_level_rn)

    above_season_level_rate = seasonal_level(above_interval, above_level_rate)
    medium_season_level_rate = seasonal_level(medium_interval, medium_level_rate)
    below_season_level_rate = seasonal_level(below_interval, below_level_rate)

    # Create representing level for roomnight and rate
    mydict = {
        (0.1, 0.1): 0.02, (0.1, 0.3): 0.06, (0.1, 0.5): 0.10, (0.1, 0.7): 0.14, (0.1, 0.9): 0.18,
        (0.3, 0.1): 0.22, (0.3, 0.3): 0.26, (0.3, 0.5): 0.30, (0.3, 0.7): 0.34, (0.3, 0.9): 0.38,
        (0.5, 0.1): 0.42, (0.5, 0.3): 0.46, (0.5, 0.5): 0.50, (0.5, 0.7): 0.54, (0.5, 0.9): 0.58,
        (0.7, 0.1): 0.62, (0.7, 0.3): 0.66, (0.7, 0.5): 0.70, (0.7, 0.7): 0.74, (0.7, 0.9): 0.78,
        (0.9, 0.1): 0.82, (0.9, 0.3): 0.86, (0.9, 0.5): 0.90, (0.9, 0.7): 0.94, (0.9, 0.9): 0.98,
    }
    above_season_level = []
    medium_season_level = []
    below_season_level = []
    for ii in range(0, len(above_season_level_rn)):
        above_season_level.append(mydict[(above_season_level_rn[ii], above_season_level_rate[ii])])
        medium_season_level.append(mydict[(medium_season_level_rn[ii], medium_season_level_rate[ii])])
        below_season_level.append(mydict[(below_season_level_rn[ii], below_season_level_rate[ii])])

    df['above_season_level_rn'] = above_season_level_rn
    df['below_season_level_rn'] = below_season_level_rn
    df['medium_season_level_rn'] = medium_season_level_rn

    df['above_season_level_rate'] = above_season_level_rate
    df['below_season_level_rate'] = below_season_level_rate
    df['medium_season_level_rate'] = medium_season_level_rate

    df['above_season_level'] = np.asarray(above_season_level)
    df['below_season_level'] = np.asarray(below_season_level)
    df['medium_season_level'] = np.asarray(medium_season_level)

    df['Date'] = pd.to_datetime(df['Date'])

    plt.subplots_adjust(wspace=15, hspace=0.8, right=0.75, top=0.95, bottom=0.2)
    fig = plt.figure(figsize=(30, 25), facecolor='w')
    my_cmap = [
        '#97040c', '#9d4b00', '#d4b400', '#0515a9', '#026f02',
        '#a0300e', '#d85013', '#ffdd1c', '#454ea0', '#20a308',
        '#d8017a', '#fc6621', '#f1ef00', '#3d8ddc', '#3cc923',
        '#fc4eb0', '#fb9262', '#fffe69', '#3fcdfd', '#88d63c',
        '#ff96d1', '#ffc0a4', '#fffeab', '#9de6fe', '#98edb5'
    ]
    my_cmap_name = ['dip_cheap', 'dip_below', 'dip_normal', 'dip_above', 'dip_expensive',
                    'low_cheap', 'low_below', 'low_normal', 'low_above', 'low_expensive',
                    'medium_cheap', 'medium_below', 'medium_normal', 'medium_above', 'medium_expensive',
                    'high_cheap', 'high_below', 'high_normal', 'high_above', 'high_expensive',
                    'peak_cheap', 'peak_below', 'peak_normal', 'peak_above', 'peak_expensive']
    import matplotlib
    cmap1 = matplotlib.colors.ListedColormap(my_cmap)

    ax1 = fig.add_subplot(311)
    # Count number of seasons
    num_periods = list(set(df['above_season_level'].values[np.newaxis][0]))  # Drop duplicates

    plt.plot(df['Date'], df[df_val1], color='k', linewidth=2, label=df_val1)
    plt.plot(df['Date'], df[df_val2], color='r', linewidth=2, label='Cover')
    plt.plot(df['Date'], df[df_val3], color='m', linewidth=2, label='Rate')
    plt.plot(df['Date'], df['above_season_level_rn'], color='b', linewidth=2, label='Cover_Level')
    plt.plot(df['Date'], df['above_season_level_rate'], color='g', linewidth=2, label='Rate_Level')
    plt.title(main, fontsize=20)
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days=' + str(min_days_above) + '__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)
    plt.setp(ax1.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax1.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax1.pcolorfast(ax1.get_xlim(), ax1.get_ylim(), df['above_season_level'].values[np.newaxis],
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)

    ax2 = fig.add_subplot(312)
    # Count number of seasons
    num_periods = list(set(df['medium_season_level'].values[np.newaxis][0]))  # Drop duplicates

    plt.plot(df['Date'], df[df_val1], color='k', linewidth=2, label=df_val1)
    plt.plot(df['Date'], df[df_val2], color='r', linewidth=2, label='Cover')
    plt.plot(df['Date'], df[df_val3], color='m', linewidth=2, label='Rate')
    plt.plot(df['Date'], df['medium_season_level_rn'], color='b', linewidth=2, label='Cover_Level')
    plt.plot(df['Date'], df['medium_season_level_rate'], color='g', linewidth=2, label='Rate_Level')

    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days=' + str(min_days_medium) + '__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    plt.setp(ax2.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax2.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax2.pcolorfast(ax1.get_xlim(), ax1.get_ylim(), df['medium_season_level'].values[np.newaxis],
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)
    # Display legends
    myTotalList = list(df['above_season_level'].values[np.newaxis][0]) \
                  + list(df['medium_season_level'].values[np.newaxis][0]) \
                  + list(df['below_season_level'].values[np.newaxis][0])
    legend_value = sorted(list(set(myTotalList)))
    legend_value = [round(elem, 2) for elem in legend_value]
    value_legend = 0.02
    legend_j = 0
    legend_handles = []
    for i in range(0, len(legend_value)):
        while abs(legend_value[i] - value_legend) > 1e-9:
            value_legend = value_legend + 0.04
            legend_j = legend_j + 1
        legend_handles.append(mpatches.Patch(color=cmap1.colors[legend_j], label=my_cmap_name[legend_j], alpha=0.6))
        value_legend = 0.02
        legend_j = 0
    plt.legend(handles=legend_handles, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=20)

    ax3 = fig.add_subplot(313)
    # Count number of seasons
    num_periods = list(set(df['below_season_level'].values[np.newaxis][0]))  # Drop duplicates

    plt.plot(df['Date'], df[df_val1], color='k', linewidth=2, label=df_val1)
    plt.plot(df['Date'], df[df_val2], color='r', linewidth=2, label='Cover')
    plt.plot(df['Date'], df[df_val3], color='m', linewidth=2, label='Rate')
    plt.plot(df['Date'], df['below_season_level_rn'], color='b', linewidth=2, label='Cover_Level')
    plt.plot(df['Date'], df['below_season_level_rate'], color='g', linewidth=2, label='Rate_Level')
    plt.ylabel('PC/Revenue', fontsize=20)
    plt.xlabel('Min_days=' + str(min_days_below) + '__No_max_number_period  (Number of seasons: '+str(len(num_periods))+')', fontsize=20)
    plt.setp(ax3.get_xticklabels(), fontsize=12)
    plt.xticks(rotation=45)
    ax3.set_xlim([np.min(df['Date']), np.max(df['Date'])])
    ax3.pcolorfast(ax1.get_xlim(), ax1.get_ylim(), df['below_season_level'].values[np.newaxis],
                   cmap=cmap1, alpha=0.6, vmin=vmin, vmax=vmax)

    plt.savefig(hotel_ids + '_' + main + '_14_21_28days_NoMaxPeriod.png', bbox_inches='tight', dpi=100)
