"""
Copyright: TP7
"""

from __future__ import print_function
from d2o.utils import systools
import errno
import os

def home_dirs():
  hdirs = [d for d in os.listdir('/home') if os.path.isdir(os.path.join('/home', d))]
  return hdirs

CUR_USER = os.path.dirname(os.path.realpath(__file__)).split('/home/')[1].split('/')[0]

user = raw_input("Install for user (default: %s): " % CUR_USER)
user = CUR_USER if (user in ['y','Y', ""]) else user

if (user not in home_dirs()):
  raise Exception("User /home/%s does not exist" % user)

# Contents of webservice config file
UPSTART_CONF_CONTENTS = """
start on runlevel [2345]
stop on runlevel [!2345]

respawn
exec python {0}

post-start script
  echo "Webservice started"
end script
""".format(os.path.abspath('start.py'))

# Contents of server config file
path_list = (os.path.abspath('department_seasons.py').replace('webservice', 'department_seasons'),
             os.path.abspath('revenue_driver_detection.py').replace('webservice', 'revenue_driver_detection'))

SERVER_CONF_CONTENTS = """
###########################
# tp7_service.conf
#
# Web service configuration
###########################

[server]
# default port = 4000
port = 4000

# enable/disable debugging output
debug = false

[modules]
department_seasons = {0}
revenue_driver_detection = {1}
""".format(*path_list)


CONFIG_DIR = '/home/{0}/.chronos/webservice/conf/'.format(user)
LOGDIR     = '/home/{0}/.chronos/webservice/tp7_log/'.format(user)

SERVER_CONF_PATH = CONFIG_DIR + 'tp7_service.conf'
UPSTART_CONF_PATH = '/etc/init/tp7_webservice.conf'

for path in [CONFIG_DIR, LOGDIR]:
  if not os.path.exists(path):
    os.makedirs(path)

# Creating default configuration files in path
def create_default(path, contents):
  if (systools.file_exists(path)):
    print("Configuration file <%s> already exists" % path)
    yn = raw_input("Overwrite? [y/n]:").lower()
    if (yn not in ['yes','y','ye']):
      return
    else:
      fb = open(path, 'r')
      bcontents = fb.read()
      print(bcontents, file=open(path + '.bak', 'w'))

  f = open(path, 'w')
  print(contents, file=f)
  print("Created default configuration file: %s" % path)

try:
  create_default(UPSTART_CONF_PATH, UPSTART_CONF_CONTENTS)
  create_default(SERVER_CONF_PATH, SERVER_CONF_CONTENTS)
  print("Installed to /home/%s" % user)
  #print("IMPORTANT: You need to manually configure the files in %s" % CONFIG_DIR)
except Exception as e:
  if ('permission denied' in str(e).lower()):
    print("You need to run setup.py as root")
    exit()
