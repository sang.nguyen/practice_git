Requirements:
-------------------------------
flask

Installation and configuration
-------------------------------
1. Install flask
  1.1 run: pip install flask
  
2. Install the webservice and create default configuration files
  2.1 run: sudo python setup.py
  
3. Navigate to the webservice directory and start the service
  3.1 run: python start.py
  
4.  Test service
  4.1 Functional test: python department_seasons.py -H 172.16.0.50 -d pmi_tp7 -U tp7 -P reHj456 -i 223
  4.2 Service test: curl -H "Content-Type: application/json" -X POST –d '{"host":"172.16.0.50", "username":"tp7","password":"reHj456"}' http://localhost:4000/department_seasons/run/pmi_tp7/14


NOTE:
- Logs will be written to ~/.chronos/webservice/tp7_log/flask.log by default. This file contents all errors like missing packages and modules and not enough data for calculate seasons.
  - Configuration file will be written to ~/.chronos/webservice/conf/tp7_service.conf by default. For example, this file includes content:
###########################
# tp7_service.conf
#
# Web service configuration
###########################
[server]
# default port = 4000
port = 4000
# enable/disable debugging output
debug = false

[modules]
department_seasons = /path/to/department_seasons.py

  - Webservice config file will be written to /etc/init/tp7_webservice.conf  by default. For example, this file includes content:
Automatic startup and respawn on exit
-------------------------------
1. Create the file /etc/init/tp7_webservice.conf
2. Add the following to the file:
description "Flask webservice"

start on runlevel [2354]
stop on runlevel [!2345]

respawn
exec /path/to/webservice/start.py
