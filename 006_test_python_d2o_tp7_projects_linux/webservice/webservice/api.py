"""
Copyright: TP7
"""

from . import app, db_conf, modules

from . import department_seasons_api as department_seasons
from . import doc
from . import revenue_driver_detection_api as revenue_driver_detection

from d2o.utils import logger as log
from d2o.utils.handlers import ExceptionHandler
import os

OK = 200
CREATED = 201
ACCEPTED = 202
BAD_REQUEST = 400
UNAUTHORIZED = 401
FORBIDDEN = 403
NOT_FOUND = 404
INTERNAL_ERROR = 500
NOT_IMPLEMENTED = 501
REALPATH = os.path.dirname(os.path.realpath(__file__))

@app.route('/')
def status():
  return "Running", 200

# Documentation
@app.route('/doc/department_seasons')
def doc_seasons():
  doc.department_seasons()

# Seasons
@app.route('/department_seasons/run/<database>', methods=['POST'])
@app.route('/department_seasons/runall/<database>', methods=['POST'])
def department_seasons_runall(database):
  return department_seasons.run(database, 'all')

@app.route('/department_seasons/run/<database>/<hid>', methods=['POST'])
def department_seasons_run_one(database, hid):
  return department_seasons.run(database, hid)

@app.route('/department_seasons/terminate', methods=['GET', 'POST'])
def department_seasons_terminate():
  return department_seasons.terminate()


# Revenue
@app.route('/revenue_driver_detection/run/<database>', methods=['POST'])
@app.route('/revenue_driver_detection/runall/<database>', methods=['POST'])
def revenue_driver_detection_runall(database):
  return revenue_driver_detection.run(database, 'all')

@app.route('/revenue_driver_detection/run/<database>/<hid>', methods=['POST'])
def revenue_driver_detection_run_one(database, hid):
  return revenue_driver_detection.run(database, hid)

@app.route('/revenue_driver_detection/terminate', methods=['GET', 'POST'])
def revenue_driver_detection_terminate():
  return revenue_driver_detection.terminate()