"""
Copyright: TP7
"""

from webservice import app, db_conf, modules
import api
from flask import Flask, jsonify, abort, request, url_for, make_response, g, redirect, render_template
import json
import subprocess
from d2o.utils import logger as log
from d2o.utils.handlers import ExceptionHandler
import os

def check_seasons():
  if ('department_seasons' not in modules or not os.path.exists(modules['department_seasons'])):
    raise Exception("Missing season module")

def terminate():
  log.dbg('/department_seasons/terminate - REQUEST')
  try:
    call_list = ['pgrep', '-f', 'department_seasons.py']
    ret = subprocess.check_output(call_list)
    kill_call_list = ['pkill', '-9', '-f', 'department_seasons.py']
    subprocess.Popen(kill_call_list)
  except Exception as e:
    log.warn(ExceptionHandler(e))
    return "No running process", api.NOT_FOUND

  return "Process terminated", api.OK

def run(database, hid='all'):
  log.dbg('/department_seasons/run/%s/%s - REQUEST' % (database, hid))

  if (hid != 'all'):
    try:
      hid = int(hid)
    except:
      retstr = "Invalid hotel id %s, %s" % (hid, ExceptionHandler(e))
      log.err(retstr)
      return retstr, api.BAD_REQUEST

  try:
    check_seasons()
  except Exception as e:
    log.err("MISSING MODULE: %s" % ExceptionHandler(e))
    return "Season module is missing", api.NOT_FOUND
  if (not request.json):
    return "JSON request expected", api.BAD_REQUEST
  else:
    rj = request.json

  data_copy = json.loads(request.data)
  data_copy['password'] = '********'
  log.dbg(json.dumps(data_copy))

  host = rj.get('host', None)
  username = rj.get('username', None)
  password = rj.get('password', None)

  if ( (host == None) or (username == None) or (password == None) ):
    log.err("BAD REQUEST: Missing host, username or password")
    return "", api.BAD_REQUEST

  years_back = rj.get('years', 3)
  min_length = rj.get('length', 3)
  inner = rj.get('inner', 14)
  outer = rj.get('outer', 60)
  write = rj.get('write', True)
  plot = rj.get('plot', False)

  hotel_ids = "%s" % hid

  call_list = [
    'python', modules['department_seasons'],
    '-H', '%s' % host,
    '-U', '%s' % username,
    '-P', '%s' % password,
    '-d', '%s' % database,
    '-l', '%s' % min_length,
    '-Y', '%s' % years_back,
    '-I', '%s' % inner,
    '-O', '%s' % outer,
    '-i', '%s' % hotel_ids]

  if (write == False):
    call_list.append('-N')
  if (plot == True):
    call_list.append('-A')

  dbg_list = call_list[:]
  dbg_list[7] = '********'
  log.dbg("CALL LIST: '%s'" % ' '.join(dbg_list))
  try:
    subprocess.Popen(call_list)
  except Exception as e:
    log.err("INTERNAL ERROR: %s" % ExceptionHandler(e))
    return e, api.INTERNAL_ERROR
  return "Process started", api.OK
