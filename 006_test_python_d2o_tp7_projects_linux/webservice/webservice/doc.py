"""
Copyright: TP7
"""

from webservice import app, db_conf, modules
from flask import Flask, jsonify, abort, request, url_for, make_response, g, redirect, render_template
import json
import subprocess
from d2o.utils import logger as log
from d2o.utils.handlers import ExceptionHandler

def department_seasons():
  try:
    return render_template('department_seasons.html')
  except Exception as e:
    log.err(ExceptionHandler(e))


def revenue_driver_detection():
  try:
    return render_template('revenue_driver_detection.html')
  except Exception as e:
    log.err(ExceptionHandler(e))
