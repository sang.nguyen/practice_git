"""
Copyright: TP7
"""

from flask import Flask
from d2o.utils.handlers import ExceptionHandler
from d2o.utils import systools
import d2o.utils.logger as log
import ConfigParser
import os

CUR_USER = os.path.dirname(os.path.realpath(__file__)).split('/home/')[1].split('/')[0]

SERVER_CONF_PATH = '/home/{0}/.chronos/webservice/conf/tp7_service.conf'.format(CUR_USER)
LOGDIR     = '/home/{0}/.chronos/webservice/tp7_log/'.format(CUR_USER)

log.enable_colors()
log.set_logdir(LOGDIR)

modules = {}
db_conf = None
server_conf = None

if (not systools.file_exists(SERVER_CONF_PATH)):
  print("Could not start server, missing service config.")
  exit()

try:
  server_conf = ConfigParser.ConfigParser()
  server_conf.read(SERVER_CONF_PATH)

  sections = server_conf.sections()
  if ('server' not in sections):
    raise Exception("Missing section [server] from config file %s" % SERVER_CONF_PATH)
  if ('modules' not in sections):
    raise Exception("Missing section [modules] from config file %s" % SERVER_CONF_PATH)

  mlist = server_conf.options('modules')
  for module in mlist:
    module_path = os.path.expanduser(server_conf.get('modules', module))
    if not os.path.exists(module_path):
      raise Exception("Path %s invalid for module %s" % (module_path, module))
    else:
      modules[module] = module_path

except Exception as e:
  raise Exception("Invalid configuration file: %s, %s" % (SERVER_CONF_PATH, ExceptionHandler(e)))

app = Flask(__name__, static_folder='../static', template_folder='../templates')
if not app.debug:
  import logging
  logging.basicConfig(filename = LOGDIR + 'flask.log', level=logging.DEBUG)

import webservice.api
