"""
Copyright: TP7
"""

from webservice import app
from d2o.utils import logger as log
from d2o.utils.systools import makedirs
import argparse
import os

LOG_VERBOSITY   = "debug"
PRINT_VERBOSITY = "info"

def create_app():
  app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
  return app

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Start the webservice", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-p", "--port", help="Port", default=4000)

  LOG_VERBOSITY   = "debug"
  PRINT_VERBOSITY = "info"

  log.set_log_verbosity(LOG_VERBOSITY)
  log.enable_colors()
  log.enable_logging()
  logdir = os.path.expanduser('~/.chronos/webservice/tp7_log/')
  makedirs(logdir)
  log.set_logdir(logdir)

  args = parser.parse_args()
  port = args.port

  app = create_app()
  app.run('0.0.0.0', debug=False, port=port)
  log.dbg("Webservice started on port %s" % port)
