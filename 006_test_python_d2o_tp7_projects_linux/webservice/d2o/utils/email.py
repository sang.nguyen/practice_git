# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import smtplib

class Email:
  def __init__(self, address, port, username, password):
    self.address = address
    self.port = port
    self.username = username
    self.password = password

    self.server = smtplib.SMTP(address, port)

  def send(self, sender, recipient, subject, message):
    message = "Subject: %s\n\n%s" % (subject, message)

    self.server.starttls()
    self.server.login(self.username, self.password)
    self.server.sendmail(sender, recipient, message)
    self.server.quit()