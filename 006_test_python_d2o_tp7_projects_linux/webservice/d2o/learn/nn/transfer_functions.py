# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import numpy as np

def sigmoid(x):
  return 1.0/(1.0 + np.exp(-x))

def sigmoid_derivative(x):
  s = sigmoid(x)
  return s * (1 - s)

def gaussian(x):
  return np.exp(-np.pow(x,2))

def gaussian_derivative(x):
  return -2.0 * x * gaussian(x)

def linear(x):
  return x

def linear_derivative(x):
  return 1.0

def tanh(x):
  return np.tanh(x)
  # if (x == 0):
  #   return 0
  # else:
  #   return (1-np.exp(-2*x))/(1+np.exp(-2*x))

def tanh_derivative(x):
  return 1.0 - np.tanh(x)**2