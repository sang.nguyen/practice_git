# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import numpy as np
from d2o.utils import logger as log

from sklearn.preprocessing import MinMaxScaler, StandardScaler

class Dataset:
  """ Dataset designed for use in Neural Networks.
  Splits into training and target sets and contains methods for
  scaling input to transfer function ranges."""
  def __init__(self, df, input_mask, target_mask, training_percentage=0.8, name="Unnamed"):
    self.df = df

    self.input_mask = input_mask
    self.target_mask = target_mask
    self.name = name

    earliest_index = abs(min([x[1] for x in self.input_mask.layout()]))

    inputs = [] # (input, target)
    targets = []
    for ix in range(earliest_index, len(self.df)):
      try:
        input_vector = self.input_mask.apply_serialize(self.df, ix)[1]
        target_vector = self.target_mask.apply_serialize(self.df, ix)[1]

        inputs.append(input_vector)
        targets.append(target_vector)
      except IndexError:
        continue
      except:
        raise

    self.input_scaler = MinMaxScaler([-1, 1])
    self.output_scaler = MinMaxScaler([-1, 1])

    # self.input_scaler = StandardScaler()
    # self.output_scaler = StandardScaler()

    inputs = self.input_scaler.fit_transform(np.array(inputs))
    targets = self.output_scaler.fit_transform(np.array(targets))

    data_set = np.array([list(x) for x in zip(inputs.tolist(), targets.tolist())])
    training_size = int(len(data_set)*training_percentage)

    self.training_set = data_set[:training_size]
    self.test_set = data_set[training_size:]

  def training_inputs(self, limit=None):
    ret = self.training_set[:,0][:limit]
    return np.array(ret.tolist())

  def training_targets(self, limit=None):
    ret = self.training_set[:,1][:limit]
    return np.array(ret.tolist())

  def test_inputs(self, limit=None):
    ret = self.test_set[:,0][:limit]
    return np.array(ret.tolist())

  def test_targets(self, limit=None):
    ret = self.test_set[:,1][:limit]
    return np.array(ret.tolist())

  def input_size(self):
    return len(self.input_mask)

  def output_size(self):
    return len(self.target_mask)

  def validate(self):
    num_equal_inputs = 0
    __test_inputs = self.test_inputs()
    __test_targets = self.test_targets()
    __training_inputs = self.training_inputs()
    __training_targets = self.training_targets()

    for itest,test_input in enumerate(__test_inputs):
      for itrain,training_input in enumerate(__training_inputs):
        if (np.array_equal(test_input, training_input)):
          num_equal_inputs += 1

    num_equal_targets = 0
    for itest,test_target in enumerate(__test_targets):
      for itrain,training_target in enumerate(__training_targets):
        if (np.array_equal(test_input, training_input)):
          num_equal_targets += 1

    if (num_equal_inputs == num_equal_targets == 0):
      return True
    else:
      log.warn("Number of equal samples (%s/%s)" % (num_equal_inputs, num_equal_targets))
      return False

