# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import datetime
from sys import platform as pl
from datetime import datetime, timedelta
from dateutil import parser
from collections import OrderedDict
from pymongo.errors import DuplicateKeyError, InvalidDocument
from bson.objectid import ObjectId
import sys, os
import numpy as np
import pandas as pd
import pyodbc

import d2o.utils.putils as put
from d2o.data.db.mssql import MSSQL
from d2o.utils import logger as log
from d2o.utils.handlers import ExceptionHandler

N_OTB_MCOLS = 2
N_FOTB_MCOLS = 3
CACHE_START_DATE = '2000-01-01'
MAX_ROWS = 500

date_format = "%Y-%m-%d"

class MissingNetworkException(Exception):
  pass

def add_query(e, query):
  e.args = tuple([e.args[0] + '\n\nQUERY:\n%s\n\n' % query.strip()] + list(e.args[1:]))
  return e

def cast(val):
  if isinstance(val, basestring):
    return "'%s'" % val.replace("'","\\'")
  if isinstance(val, pd.tslib.Timestamp):
    return "'%s'" % val
  if (isinstance(val, float)):
    return '{:f}'.format(val)

  return ('%s' % val).encode('utf-8')

def create_insert_strings(dataframe,cs='`',ce='`'):
  columns = dataframe.columns
  escaped_columns = ['%s%s%s' % (cs,x,ce) for x in dataframe.columns]
  rowstrings = []
  for i,row in dataframe.iterrows():
    row_str     = '('
    row_strings = [cast(row[c]) for c in columns]
    row_str    += ','.join(row_strings)
    row_str    += ')'
    rowstrings.append(row_str)

  colstr = '(' + ','.join(escaped_columns) + ')'
  valstr = ',\n'.join(rowstrings)
  dupstr = ','.join(['{0} = VALUES({0})'.format(x) for x in escaped_columns])
  return (colstr, valstr, dupstr)

class Database:
  def __init__(self, uid, pwd, host, dbname, schema):
    self.db = MSSQL(user=uid, password=pwd, host=host, database=dbname)

    self.dbname = dbname
    self.schema = schema

  def is_connected(self):
    return self.db.is_connected()

  def header(self, variables=[]):
    header = "USE %s;\n" % self.dbname
    for t in variables:
      header += 'DECLARE @%s %s;\n' % (t[0],t[1])
      header += 'SET @%s = %s;\n' % (t[0],t[2])
    return header

  # def __upsert(self, dataframe, table):
  #   statements = []
  #   for ix,row in dataframe.iterrows():
  #     query = \
  #     """
  #     IF NOT EXISTS(SELECT * FROM {0}.{1}.Forecasting_Auto_dates
  #       WHERE h_id = {2} AND segment_id = {3} AND date = '{4}')
  #     INSERT INTO
  #       {0}.{1}.Forecasting_Auto_Dates(h_id, segment_id, date, revenue)
  #       VALUES ({2},{3},'{4}',{5})
  #   ELSE
  #     UPDATE {0}.{1}.Forecasting_Auto_Dates SET
  #       h_id = {2}, segment_id = {3}, date = '{4}', revenue = {5}
  #     WHERE
  #       h_id = {2} AND segment_id = {3} AND date = '{4}';
  #     """.format(self.dbname, self.schema, row['h_id'], row['segment_id'], row['date'], row['revenue'])
  #     statements.append(query)

  #   for statement in statements:
  #     self.db.execute_raw(statement)

  def __upsert(self, dataframe, table, primary_keys):
    if (any([pk not in dataframe.columns.values for pk in primary_keys])):
      raise Exception("One or more primary keys not found in dataframe columns")

    if (len(dataframe) > MAX_ROWS):
      log.debug("Dataframe too large (%s rows), splitting" % len(dataframe))
      delim = int(np.rint(len(dataframe)/MAX_ROWS)+1)
      for __slice in np.array_split(dataframe, delim):
        for ix,row in __slice.iterrows():
          cols = row.keys().values
          col_str = ','.join(["[%s]" % x for x in cols])
          where_str = ' AND '.join(['%s = %s' % (pk, cast(row[pk])) for pk in primary_keys])
          set_str = ','.join(['%s = %s' % (c, cast(row[c])) for c in cols])
          val_str = ','.join([cast(row[c]) for c in cols])
          query = \
          """
          USE {0}

          IF NOT EXISTS
            (SELECT * FROM {1} WHERE {2}) INSERT INTO {1}({3}) VALUES({4})
          ELSE
            UPDATE {1} SET {5} WHERE {2};
          """.format(self.dbname, table, where_str, col_str, val_str, set_str)

          self.db.execute_raw(query)
    else:
      for ix,row in dataframe.iterrows():
        cols = row.keys().values
        col_str = ','.join(["[%s]" % x for x in cols])
        where_str = ' AND '.join(['%s = %s' % (pk, cast(row[pk])) for pk in primary_keys])
        set_str = ','.join(['%s = %s' % (c, cast(row[c])) for c in cols])
        val_str = ','.join([cast(row[c]) for c in cols])
        query = \
        """
        USE {0}

        IF NOT EXISTS
          (SELECT * FROM {1} WHERE {2}) INSERT INTO {1}({3}) VALUES({4})
        ELSE
          UPDATE {1} SET {5} WHERE {2};
        """.format(self.dbname, table, where_str, col_str, val_str, set_str)
        self.db.execute_raw(query)

  def __insert(self, dataframe, table):
    if (len(dataframe) > MAX_ROWS):
      log.debug("Dataframe too large (%s rows), splitting" % len(dataframe))
      delim = int(np.rint(len(dataframe)/MAX_ROWS)+1)
      for __slice in np.array_split(dataframe, delim):
        colstr, valstr, _ = create_insert_strings(__slice,cs='[',ce=']')
        query = \
          """
          INSERT INTO {0}.{1}.{2}
            {3}
          VALUES
            {4}
          """.format(self.dbname, self.schema, table, colstr, valstr).replace('nan','NULL')

        self.db.execute_raw(query)
    else:
      colstr, valstr, _ = create_insert_strings(dataframe,cs='[',ce=']')
      query = \
        """
        INSERT INTO {0}.{1}.{2}
          {3}
        VALUES
          {4}
        """.format(self.dbname, self.schema, table, colstr, valstr).replace('nan','NULL')

      self.db.execute_raw(query)

  def percent_data(self, hid, segment_id, threshold=80):
    from_date = self.earliest_date_for_segment(hid, segment_id)
    to_date = self.latest_date_for_segment(hid, segment_id)

    header_vars = [
      ('h_id','int',hid),
      ('from','datetime', "'%s'" % from_date),
      ('to','datetime', "'%s'" % to_date),
      ('segment_id','int',segment_id)
    ]
    header_string = self.header(header_vars)

    query = \
    """
    {0}

    WITH d(date) AS (
      SELECT CAST(@from as datetime)
      UNION ALL
      SELECT date+1
      FROM d
      WHERE date < @to
      )

    SELECT (100.0*COUNT(r.revenue))/COUNT(*) AS percent_data
    FROM d
    LEFT JOIN Revenue r on r.date = d.date
    AND r.h_id = @h_id AND r.segment_id = @segment_id
    OPTION (MAXRECURSION 0)
    """.format(header_string)

    try:
      val = self.db.select_df(query)['percent_data'].values[0]
    except Exception as e:
      e = add_query(e, query)
      raise
    return val

  def has_segments(self, hotel_id):
    header_string = self.header([('h_id','int',hotel_id)])

    query = \
    """
    {0}

    SELECT CAST(CASE WHEN (use_segments = 1 AND use_total_values = 0) THEN 1 ELSE 0 END as bit)
    AS
    use_segments from Forecasting_Settings
    WHERE
    h_id = @h_id
    """.format(header_string)

    #log.dbg("has_segments QUERY: %s" % query)
    return (self.db.select_df(query)['use_segments'].values[0])

  def has_data(self, hotel_id):
    REQ_LEN = 365
    REQ_PERCENT_DATA = 80

    header_string = self.header([('h_id','int',hotel_id)])
    query = \
    """
    {0}

    SELECT TOP {1} *
    FROM
      Revenue
    WHERE
      h_id = @h_id;
    """.format(header_string, REQ_LEN)

    #log.dbg("has_data QUERY ONE: %s" % query)
    has_year = len(self.db.select_df(query)) >= REQ_LEN
    if (not has_year):
      return False

    query = \
    """
    {0}

    SELECT TOP 1000 *
    FROM
      Revenue
    WHERE
      h_id = @h_id;
    """.format(header_string)
    stats = put.statistics(self.db.select_df(query))
    #log.dbg("STATISTICS: %s" % stats)
    if (stats['revenue']['percent_nan'] > REQ_PERCENT_DATA or stats['revenue']['percent_zero'] > REQ_PERCENT_DATA):
      return False
    return True

  def latest_date_for_segment(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])

    query = \
    """
    {0}

    SELECT * FROM
    (SELECT date,  DATEDIFF(day, LAG(date,1) OVER (ORDER BY date DESC), date) as diff
     FROM Revenue where h_id = @h_id AND segment_id = @segment_id AND revenue IS NOT NULL
    )p
    WHERE p.diff = -1;
    """.format(header_string)

    df = self.db.select_df(query)
    if (len(df) <= 0):
      raise Exception("No data for hotel %s" % hotel_id)

    latest_date = pd.Timestamp(df['date'].values[0]).to_datetime() + timedelta(days=1)
    yesterday = datetime.now() - timedelta(days=1)
    if (latest_date > yesterday):
      return yesterday
    else:
      return latest_date

  def earliest_forecasting_date(self, hotel_id):
    query = \
    """
    {0}

    SELECT TOP 1 date from Forecasting_Values v
    WHERE v.h_id = @h_id
    ORDER by date ASC
    """.format(self.header([('h_id','int',hotel_id)]))

    df = self.db.select_df(query)
    return pd.Timestamp(df['date'].values[0]).to_datetime()

  def latest_forecasting_date(self, hotel_id):
    query = \
    """
    {0}

    SELECT TOP 1 date from Forecasting_Values v
    WHERE v.h_id = @h_id
    ORDER by date DESC
    """.format(self.header([('h_id','int',hotel_id)]))

    df = self.db.select_df(query)
    return pd.Timestamp(df['date'].values[0]).to_datetime()

  def earliest_date_for_segment(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])

    query = \
    """
    {0}

    SELECT * FROM
    (SELECT date,  DATEDIFF(day, LAG(date,1) OVER (ORDER BY date ASC), date) as diff
     FROM Revenue where h_id = @h_id AND segment_id = @segment_id AND revenue IS NOT NULL
    )p
    WHERE p.diff = 1;
    """.format(header_string)

    df = self.db.select_df(query)
    if (len(df) <= 0):
      raise Exception("No data for h_id %s" % hid)

    return pd.Timestamp(df['date'].values[0]).to_datetime() - timedelta(days=1)


  def latest_date(self, hotel_id):
    header_string = self.header([('h_id','int',hotel_id)])
    query = \
    """
    {0}

    SELECT * FROM
    (SELECT date,  DATEDIFF(day, LAG(date,1) OVER (ORDER BY date DESC), date) as diff
     FROM Revenue where h_id = @h_id
    )p
    WHERE p.diff = -1;
    """.format(header_string)

    df = self.db.select_df(query)
    if (len(df) <= 0):
      raise Exception("No data for hotel %s" % hotel_id)

    latest_date = pd.Timestamp(df['date'].values[0]).to_datetime() + timedelta(days=1)
    yesterday = datetime.now() - timedelta(days=1)
    if (latest_date > yesterday):
      return yesterday
    else:
      return latest_date

  def earliest_date(self, hotel_id):
    header_string = self.header([('h_id','int',hotel_id)])
    query = \
    """
    {0}

    SELECT * FROM
    (SELECT date,  DATEDIFF(day, LAG(date,1) OVER (ORDER BY date ASC), date) as diff
     FROM Revenue where h_id = @h_id
    )p
    WHERE p.diff = 1;
    """.format(header_string)

    df = self.db.select_df(query)
    if (len(df) <= 0):
      raise Exception("No data for hotel %s" % hotel_id)

    return pd.Timestamp(df['date'].values[0]).to_datetime() - timedelta(days=1)

  def hotel_ids(self):
    query = \
      """
      USE {0}

      SELECT
        hierarchy_id AS 'hotel_id'
      FROM
        tbl_global_hierarchy
      WHERE
        hierarchy_income_level=1
      """.format(self.dbname)

    #log.dbg("hotel_ids QUERY: %s" % query)
    return self.db.select_df(query)

  def hotel_name(self, hotel_id):
    header_string = self.header([('h_id','int',hotel_id)])
    query = \
      """
      {0}

      SELECT
        hierarchy_name as 'hotel_name'
      FROM
        tbl_global_hierarchy
      WHERE
        hierarchy_id = @h_id
      """.format(header_string)

    #log.dbg("hotel_name QUERY: %s" % query)
    return self.db.select_df(query)['hotel_name'][0].encode('utf-8')

  def hotel_id(self, hotel_name):
    header_string = self.header([('hotel_name','nvarchar(255)', hotel_name)])
    query = \
      """
      {0}

      SELECT
        hierarchy_id as 'hotel_id'
      FROM
        tbl_global_hierarchy
      WHERE
        hierarchy_name = @hotel_name'
      """.format(header_string)

    #log.dbg("hotel_id QUERY: %s" % query)
    return self.db.select_df(query)

  def hotel_names(self):
    query = \
      """
      USE {0}

      SELECT
        hierarchy_id AS 'hotel_id',
        hierarchy_name AS 'hotel_name'
      FROM
        tbl_global_hierarchy
      WHERE
        hierarchy_income_level=1
      """.format(self.dbname)

    #log.dbg("hotel_names QUERY: %s" % query)
    return self.db.select_df(query)

  def revenue(self, hotel_id, from_date, to_date, columns='all'):
    if (isinstance(columns,basestring) and columns == 'all'):
      columns = self.segments(hotel_id)['id'].values

    if (isinstance(from_date, basestring)):
      from_date = parser.parse(from_date)

    if (isinstance(to_date, basestring)):
      to_date = parser.parse(to_date)

    from_date = datetime.strftime(from_date, date_format)
    to_date = datetime.strftime(to_date, date_format)

    header_vars = [
    ('h_id','int',hotel_id),
    ('from','datetime', "'%s'" % from_date),
    ('to','datetime', "'%s'" % to_date)
    ]

    header_string = self.header(header_vars)

    query = \
    """
    {0}

    SELECT *
    FROM(
      SELECT DISTINCT date, r.revenue,
      CAST(hi2.hierarchy_id AS varchar(10)) + '_'  + CAST(segment_id AS varchar(10)) AS 'id'
      --CAST(hi2.hierarchy_name AS varchar(255))  + '_' + (CASE WHEN r.segment_id = 0 THEN 'Total' ELSE h.hierarchy_segment_name END) AS 'name'
      FROM
        tbl_global_hierarchy hi
        INNER JOIN tbl_global_hierarchy hi2 on hi.hierarchy_id = hi2.hierarchy_parent_id OR hi.hierarchy_id = hi2.hierarchy_id
        INNER JOIN Forecasting_Settings s on s.h_id = hi2.hierarchy_id
        INNER JOIN Revenue r on r.h_id = hi2.hierarchy_id
        LEFT OUTER JOIN tbl_hierarchy_segments h ON h.hierarchy_segment_id = r.segment_id
      WHERE
        hi.hierarchy_id = @h_id AND ((r.segment_id <> 0 AND s.use_segments = 1 AND s.use_total_values = 0)
        OR (r.segment_id = 0 AND (s.use_segments = 0 OR s.use_total_values = 1)) OR (hi.hierarchy_id = hi2.hierarchy_id))
        AND date >= @from AND date < @to
      ) AS x
    PIVOT(
    SUM(revenue) FOR id IN ({1})
    ) AS p
    ORDER BY date
    """.format(header_string, ','.join(['[%s]' % x.encode('utf-8') for x in columns]))

    df = self.db.select_df(query, parse_dates='date', index_col='date')
    df.columns = [x.encode('utf-8') for x in df.columns]
    return df

  def room_nights(self, hotel_id, from_date, to_date, columns='all'):
    if (isinstance(columns,basestring) and columns == 'all'):
      columns = [x['id'] for ix,x in self.segments(hotel_id).iterrows() if (int(x['id'].split('_')[0]) == int(hotel_id))]

    if (isinstance(from_date, basestring)):
      from_date = parser.parse(from_date)

    if (isinstance(to_date, basestring)):
      to_date = parser.parse(to_date)

    from_date = datetime.strftime(from_date, date_format)
    to_date = datetime.strftime(to_date, date_format)

    header_vars = [
    ('h_id','int',hotel_id),
    ('from','datetime', "'%s'" % from_date),
    ('to','datetime', "'%s'" % to_date)
    ]

    header_string = self.header(header_vars)

    query = \
    """
    {0}

    SELECT *
    FROM(
      SELECT DISTINCT date, r.units,
      CAST(hi2.hierarchy_id AS varchar(10)) + '_'  + CAST(segment_id AS varchar(10)) AS 'id'
      --CAST(hi2.hierarchy_name AS varchar(255))  + '_' + (CASE WHEN r.segment_id = 0 THEN 'Total' ELSE h.hierarchy_segment_name END) AS 'name'
      FROM
        tbl_global_hierarchy hi
        INNER JOIN tbl_global_hierarchy hi2 on hi.hierarchy_id = hi2.hierarchy_parent_id OR hi.hierarchy_id = hi2.hierarchy_id
        INNER JOIN Revenue r on r.h_id = hi2.hierarchy_id
        LEFT OUTER JOIN tbl_hierarchy_segments h ON h.hierarchy_segment_id = r.segment_id
      WHERE
        hi.hierarchy_id = @h_id AND (hi.hierarchy_id = hi2.hierarchy_id)
        AND date >= @from AND date < @to
      ) AS x
    PIVOT(
    SUM(units) FOR id IN ({1})
    ) AS p
    ORDER BY date
    """.format(header_string, ','.join(['[%s]' % x.encode('utf-8') for x in columns]))

    df = self.db.select_df(query, parse_dates='date', index_col='date')
    df.columns = [x.encode('utf-8') for x in df.columns]
    return df

  def remove_networks(self, hotel_id):
    hids = list(set([x.split('_')[0] for x in self.segments(hotel_id)['id']] + ['%s' % hotel_id]))

    query = \
    """
    DELETE FROM {0}.{1}.Forecasting_Auto WHERE h_id IN ({2});
    """.format(self.dbname, self.schema, ','.join(hids))

    self.db.execute_raw(query)

  def get_parent(self, hid):
    header_string = self.header([('h_id','int',hid)])
    query = \
    """
    {0}
    SELECT DISTINCT h.hierarchy_income_level as 'income_level' FROM
    tbl_global_hierarchy h
    WHERE h.hierarchy_id = @h_id
    """.format(header_string)

    ic = int(self.db.select_df(query)['income_level'][0])
    if (ic == 1):
      return hid

    query = \
    """
    {0}

    SELECT DISTINCT h.hierarchy_parent_id as 'parent' FROM
    tbl_global_hierarchy h
    WHERE h.hierarchy_id = @h_id;
    """.format(header_string)

    return self.db.select_df(query)['parent'][0]

  def predictable_segments(self, hotel_id, names=False):
    sub_df = self.segments(hotel_id)
    sub_df = sub_df[sub_df['predict'] == 1]

    if (len(sub_df) > 0):
      if (names == False):
        return sub_df['id']
      else:
        return sub_df['name']
    return pd.Series([])

  def id_to_name(self, hid, segment_id):
    hotel_id = hid if (segment_id == 0) else self.get_parent(hid)
    seg_df = self.segments(hotel_id)

    name = seg_df[(seg_df.id == '%s_%s' % (hid, segment_id))]['name'].values[0].split('_')
    return (name[0], name[1])

  def segments(self, hotel_id):
    header_vars = [('h_id','int',hotel_id)]
    query = \
    """
    {0}

    SELECT DISTINCT hi.hierarchy_id AS hotel_id,
    CAST(hi2.hierarchy_id AS varchar(10)) + '_'  + CAST(segment_id AS varchar(10)) AS 'id',
    CAST(hi2.hierarchy_name AS varchar(255))  + '_' + (CASE WHEN r.segment_id = 0 THEN 'Total' ELSE h.hierarchy_segment_name END) AS 'name',
    CASE WHEN hi2.hierarchy_id = hi.hierarchy_id THEN 0 ELSE 1 END AS 'predict'
    FROM
      tbl_global_hierarchy hi
      INNER JOIN tbl_global_hierarchy hi2 on hi.hierarchy_id = hi2.hierarchy_parent_id OR hi.hierarchy_id = hi2.hierarchy_id
      INNER JOIN Forecasting_Settings s on s.h_id = hi2.hierarchy_id
      INNER JOIN Revenue r on r.h_id = hi2.hierarchy_id
      LEFT OUTER JOIN tbl_hierarchy_segments h ON h.hierarchy_segment_id = r.segment_id
    WHERE
      hi.hierarchy_id = @h_id AND ((r.segment_id <> 0 AND s.use_segments = 1 AND s.use_total_values = 0)
      OR (r.segment_id = 0 AND (s.use_segments = 0 OR s.use_total_values = 1)) OR (hi.hierarchy_id = hi2.hierarchy_id))
    """.format(self.header(header_vars), hotel_id)
    log.dbg("segment_names QUERY: %s" % query)
    df = self.db.select_df(query)
    return df

  def forecasting_values(self, hotel_id, from_date, to_date, total=True):
    if (isinstance(from_date, basestring)):
      from_date = parser.parse(from_date)

    if (isinstance(to_date, basestring)):
      to_date = parser.parse(to_date)

    from_date = datetime.strftime(from_date, date_format)
    to_date = datetime.strftime(to_date, date_format)

    header_vars = [
    ('h_id','int',hotel_id),
    ('from','datetime', "'%s'" % from_date),
    ('to','datetime', "'%s'" % to_date)
    ]

    header_string = self.header(header_vars)

    if (total):
      valstr = \
      """
      SELECT d.h_id, d.date, SUM(ISNULL(v.units_rolling,0)) AS room_nights,
      SUM(ISNULL(s1.units_rolling,0)) AS 't-7',
      SUM(ISNULL(s2.units_rolling,0)) AS 't-14',
      SUM(ISNULL(s3.units_rolling,0)) AS 't-21',
      SUM(ISNULL(s4.units_rolling,0)) AS 't-28',
      SUM(ISNULL(s5.units_rolling,0)) AS 't-35',
      SUM(ISNULL(s6.units_rolling,0)) AS 't-42'
      """
      tailstr = \
      """
      GROUP BY d.h_id, d.date
      ORDER BY d.date
      """
    else:
      valstr = \
      """
      SELECT d.h_id, d.name, d.date, d.segment, d.segment_id, ISNULL(v.units_rolling,0) AS room_nights,
      ISNULL(s1.units_rolling,0) AS 't-7',
      ISNULL(s2.units_rolling,0) AS 't-14',
      ISNULL(s3.units_rolling,0) AS 't-21',
      ISNULL(s4.units_rolling,0) AS 't-28',
      ISNULL(s5.units_rolling,0) AS 't-35',
      ISNULL(s6.units_rolling,0) AS 't-42'
      """
      tailstr = \
      """
      ORDER BY d.date, d.segment_id
      """

    query = \
    """
    {0}

    {1}
    FROM
    (
    SELECT DISTINCT hi.hierarchy_id AS h_id, hi.hierarchy_name AS name, d.date,
      CASE WHEN ISNULL(h.hierarchy_segment_h_id,0) = 0 THEN 'Total' ELSE h.hierarchy_segment_name END AS segment,
      ISNULL(h.hierarchy_segment_id,0) AS segment_id,
      dateadd(day,-7,d.date) AS d1,
      dateadd(day,-14,d.date) AS d2,
      dateadd(day,-21,d.date) AS d3,
      dateadd(day,-28,d.date) AS d4,
      dateadd(day,-35,d.date) AS d5,
      dateadd(day,-42,d.date) AS d6
      FROM tbl_global_hierarchy hi
      INNER JOIN Forecasting_Settings s ON s.h_id = hi.hierarchy_id
      INNER JOIN DateInformation d ON d.date BETWEEN @from AND @to
      LEFT OUTER JOIN tbl_hierarchy_segments h ON hi.hierarchy_id = h.hierarchy_segment_h_id AND (s.use_segments = 1 AND s.use_total_values = 0)
      WHERE hi.hierarchy_id = @h_id AND hierarchy_income_level = 1
    )d
    LEFT JOIN Forecasting_Values v ON v.h_id = d.h_id AND v.segment_id = d.segment_id AND v.date = d.date
    LEFT JOIN Forecasting_Values_SnapShots s1 ON s1.h_id = d.h_id AND s1.segment_id = d.segment_id AND s1.date = d.date AND d1 between s1.snapshot_date AND s1.snapshot_to
    LEFT JOIN Forecasting_Values_SnapShots s2 ON s2.h_id = d.h_id AND s2.segment_id = d.segment_id AND s2.date = d.date AND d2 between s2.snapshot_date AND s2.snapshot_to
    LEFT JOIN Forecasting_Values_SnapShots s3 ON s3.h_id = d.h_id AND s3.segment_id = d.segment_id AND s3.date = d.date AND d3 between s3.snapshot_date AND s3.snapshot_to
    LEFT JOIN Forecasting_Values_SnapShots s4 ON s4.h_id = d.h_id AND s4.segment_id = d.segment_id AND s4.date = d.date AND d4 between s4.snapshot_date AND s4.snapshot_to
    LEFT JOIN Forecasting_Values_SnapShots s5 ON s5.h_id = d.h_id AND s5.segment_id = d.segment_id AND s5.date = d.date AND d5 between s5.snapshot_date AND s5.snapshot_to
    LEFT JOIN Forecasting_Values_SnapShots s6 ON s6.h_id = d.h_id AND s6.segment_id = d.segment_id AND s6.date = d.date AND d6 between s6.snapshot_date AND s6.snapshot_to
    {2}
    """.format(header_string, valstr, tailstr)

    df = self.db.select_df(query, parse_dates='date', index_col='date')
    return df

  def expired_networks(self, days=180):
    query = \
    """
    USE {0}
    SELECT h_id, segment_id FROM Forecasting_Auto WHERE generated < DATEADD(day, -{1}, GETDATE());
    """.format(self.dbname, days)

    return self.db.select_df(query)

  def network_entry_exists(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])
    query = \
    """
    {0}

    SELECT 1 FROM Forecasting_Auto
    WHERE h_id = @h_id AND segment_id = @segment_id;
    """.format(header_string)

    return (len(self.db.select_df(query.decode('utf-8'))) > 0)

  def network_exists(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])
    query = \
    """
    {0}

    SELECT 1 FROM Forecasting_Auto
    WHERE h_id = @h_id AND segment_id = @segment_id AND network IS NOT NULL;
    """.format(header_string)

    return (len(self.db.select_df(query.decode('utf-8'))) > 0)

  def validate_network(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])
    query = \
    """
    {0}

    UPDATE Forecasting_Auto SET valid = 1
    WHERE h_id = @h_id AND segment_id = @segment_id;
    """.format(header_string)

    self.db.execute_raw(query)

  def invalidate_network(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])
    query = \
    """
    {0}

    UPDATE Forecasting_Auto SET valid = 0
    WHERE h_id = @h_id AND segment_id = @segment_id;
    """.format(header_string)

    self.db.execute_raw(query)

  def networks(self, hotel_id=None):
    header_string = self.header([('h_id','int',hotel_id)])
    if (hotel_id == None or hotel_id == 'all'):
      query = \
      """
      {0}

      SELECT h_id, segment_id FROM Forecasting_Auto;
      """.format(header_string)
    else:
      query = \
      """
      {0}

      SELECT DISTINCT f.h_id, f.segment_id FROM
        Forecasting_Auto f
      INNER JOIN
      tbl_global_hierarchy h ON f.h_id = h.hierarchy_id
      WHERE h.hierarchy_parent_id = @h_id;
      """.format(header_string)

    return self.db.select_df(query)

  def invalid_networks(self, hotel_id=None):
    header_string = self.header([('h_id','int',hotel_id)])
    if (hotel_id == None or hotel_id == 'all'):
      query = \
      """
      {0}

      SELECT h_id, segment_id FROM Forecasting_Auto WHERE valid = 0;
      """.format(header_string)
    else:
      query = \
      """
      {0}

      SELECT DISTINCT f.h_id, f.segment_id FROM
        Forecasting_Auto f
      INNER JOIN
        tbl_global_hierarchy h ON f.h_id = h.hierarchy_id
      WHERE f.valid = 0 AND h.hierarchy_parent_id = @h_id;
      """.format(header_string)

    return self.db.select_df(query)

  def valid_networks(self, hotel_id=None):
    header_string = self.header([('h_id','int', hotel_id)])
    if (hotel_id == None or hotel_id == 'all'):
      query = \
      """
      {0}

      SELECT h_id, segment_id FROM Forecasting_Auto WHERE valid = 1;
      """.format(header_string)
    else:
      query = \
      """
      {0}

      SELECT DISTINCT f.h_id, f.segment_id FROM
        Forecasting_Auto f
      INNER JOIN
        tbl_global_hierarchy h ON f.h_id = h.hierarchy_id
      WHERE f.valid = 1 AND h.hierarchy_parent_id = @h_id;
      """.format(header_string)

    return self.db.select_df(query)

  def insert_empty_network(self, hid, segment_id):
    header_string = self.header([('h_id','int',hid),('segment_id','int',segment_id)])
    query = \
    """
    {0}

    INSERT INTO Forecasting_Auto (h_id, segment_id, valid) VALUES (@h_id,@segment_id,0)
    """.format(header_string)
    self.db.execute_raw(query)

  def write_network(self, hid, segment_id, network_bio):
    conn = self.db.pyodbc_connection()
    date = datetime.now().isoformat()
    valid = True

    network_binary = pyodbc.Binary(network_bio.read())
    network_bio.close()

    if (self.network_entry_exists(hid, segment_id)):
      query = 'UPDATE {0}.{1}.Forecasting_Auto SET network = ?, valid = ?, generated = ? WHERE h_id = {2} AND segment_id = {3};'.format(self.dbname, self.schema, hid, segment_id)
      vals = (network_binary, 1, datetime.now())
    else:
      query = 'INSERT INTO {0}.{1}.Forecasting_Auto (h_id, segment_id, network, valid, generated) VALUES (?, ?, ?, ?, ?);'.format(self.dbname, self.schema)
      vals = (hid, segment_id, network_binary, 1, datetime.now())

    conn.cursor().execute(query, vals)
    conn.commit()
    conn.close()

  def load_network(self, hid, segment_id):
    if (not self.network_exists(hid, segment_id)):
      raise MissingNetworkException("No network for (h_id/segment_id) = (%s/%s)" % (hid, segment_id))

    conn = self.db.pyodbc_connection()

    query = 'SELECT * FROM {0}.{1}.Forecasting_Auto WHERE h_id = {2} and segment_id = {3}'.format(self.dbname, self.schema, hid, segment_id)
    cursor = conn.cursor().execute(query)

    cols = [column[0] for column in cursor.description]
    rows = []

    for row in cursor.fetchall():
      rows.append(dict(zip(cols,row)))

    conn.close()

    if (len(rows) > 1):
      raise Exception("Multiple networks for (h_id/segment_id) = (%s/%s)" % (hid, segment_id))

    return rows[0]

  def write_network_results(self, hid, segment_id, df):
    # Add h_id and segment_id columns
    df['h_id'] = [int(hid)]*len(df)
    df['segment_id'] = [int(segment_id)]*len(df)

    self.__upsert(df, 'Forecasting_Auto_Dates', primary_keys=['h_id', 'segment_id', 'date'])

  def get_network_results(self, hid, segment_id, from_date, to_date):
    if (isinstance(from_date, basestring)):
      from_date = parser.parse(from_date)

    if (isinstance(to_date, basestring)):
      to_date = parser.parse(to_date)

    from_date = datetime.strftime(from_date, date_format)
    to_date = datetime.strftime(to_date, date_format)

    header_vars = [
      ('h_id','int', hid),
      ('segment_id', 'int', segment_id),
      ('from','datetime', "'%s'" % from_date),
      ('to','datetime', "'%s'" % to_date)
    ]

    header_string = self.header(header_vars)

    query = \
    """
    {0}

    SELECT * FROM Forecasting_Auto_Dates
    WHERE h_id = @h_id AND segment_id = @segment_id AND date >= @from AND date < @to
    """.format(header_string)

    return self.db.select_df(query, parse_dates='date', index_col='date')

  def write_metadata(self, meta_df):
    self.__insert(meta_df, 'Season_AutoRun')#, primary_keys=['run_id','h_id'])

  def write_seasons(self, season_df):
    self.__insert(season_df, 'Season_AutoRun_Data')#, primary_keys=season_df.columns.values)

  def write_plotdata(self, plot_df):
    self.__insert(plot_df, 'Season_AutoRun_PlotData')#, primary_keys=season_df.columns.values)

  def write_correlations(self, cdf):
    pks = [
    'h_id_source',
    'segment_id_source',
    'type_source',
    'h_id_correlated',
    'segment_id_correlated',
    'type_correlated'
    ]
    self.__upsert(cdf, 'Forecasting_Auto_Correlations', primary_keys=pks)

  # Deprecated (used by seasons.py:115)
  def segment_names(self, hotel_id):
    query = \
      """
      SELECT DISTINCT
        CASE WHEN r.segment_id = 0 THEN 'Total' ELSE h.hierarchy_segment_name END AS 'SegmentName',
        r.segment_id AS 'SegmentId'
      FROM
        {0}.{1}.revenue r
      LEFT OUTER JOIN
        {0}.{1}.tbl_hierarchy_segments h
      ON
        h.hierarchy_segment_id = r.segment_id
      WHERE
        r.h_id = {2};
      """.format(self.dbname, self.schema, hotel_id)

    log.dbg("segment_names QUERY: %s" % query)
    return self.db.select_df(query)

  # Deprecated (used by seasons.py:51)
  def check_timestamps(self, timestamps):
    count = len(timestamps)

    query1 = \
    """
    SELECT COUNT(DISTINCT run_id) AS 'Count'
    FROM {0}.{1}.Season_AutoRun
    WHERE run_id IN ({2})
    """.format(self.dbname, self.schema, ','.join(timestamps))

    num_autorun = int(self.db.select_df(query1)['Count'].values[0])
    if (num_autorun != count):
      raise Exception("Expected data missing from Season_AutoRun")

    query2 = \
    """
    SELECT COUNT(DISTINCT run_id) AS 'Count'
    FROM {0}.{1}.Season_AutoRun_Data
    WHERE run_id IN ({2})
    """.format(self.dbname, self.schema, ','.join(timestamps))

    num_autorun_data = int(self.db.select_df(query2)['Count'].values[0])
    if (num_autorun_data != count):
      raise Exception("Expected data missing from Season_AutoRun_Data")

    query3 = \
    """
    SELECT COUNT(DISTINCT run_id) AS 'Count'
    FROM {0}.{1}.Season_AutoRun_PlotData
    WHERE run_id IN ({2})
    """.format(self.dbname, self.schema, ','.join(timestamps))

    num_autorun_plotdata = int(self.db.select_df(query3)['Count'].values[0])
    if (num_autorun_plotdata != count):
      raise Exception("Expected data missing from Season_AutoRun_PlotData")

    return True