# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import numpy as np
import pandas as pd

class IndexMask:
  ''' Applied when creating a sliding window dataset'''
  def __init__(self, masks, name=None):
    #TODO: Document the 'name' variable.
    #TODO: Examples is a mock up, replace with real ipython output.
    """
    Parameters
    ----------
    masks : dict
        Dictionary containing np.arrays of np.arrays with relative indexes.

    name : string
        Name of the mask.

    Returns
    -------
    None

    Examples
    -------
    >>> maskDict = {"var_1": [[-1, -2, -3]], "var_2": [[-1, -2, -3], [-14, -15]]}
    >>> mask = indexMask(maskDict)
    >>> mask.masks
    {"var_1": [[-1, -2, -3]], "var_2": [[-1, -2, -3], [-14, -15]]}
    """
    self.masks = masks
    self.name = name
    self.cols = sorted(self.masks.keys())

    if (self.name == None):
      self.name = ','.join(self.cols)

  def layout(self):
    sig = []
    for col in self.cols:
#      print "Inside layout, col = ", col
      masks = self.masks[col]
#      print "Inside layout, masks = ", masks
      sig += [(col,min(mask),max(mask), len(mask)) for mask in masks]
#    print "Inside layout, sig = ", sig
    return sig

  def apply(self, df, index):
    """ Returns a subset (pandas series.Series) per variable created by
    applying the mask on the original dataframe for the given index
    """

    for col in self.cols:
      if (col not in df.columns.values):
        s = "Invalid dataframe, column '%s' not found in %s" % (col, df.columns.values)
        print(s)
        raise Exception(s)

    sub_dfs = {}
    for col,masks in self.masks.iteritems():
      indexed_mask = [l for sublist in [index + mask for mask in masks] for l in sublist]
      if (any(x >= len(df) for x in indexed_mask) or any(x < 0 for x in indexed_mask)):
        raise IndexError("Mask indices %s out of bounds for dataframe with range [0,%s]" % (indexed_mask, len(df)))
      sub_dfs[col] = pd.concat([ df.iloc[(index + mask)][col] for mask in masks])

    return sub_dfs

  def apply_serialize(self, df, index):
    """ Returns a serialized ([index], [values]) representation of the
    masked dataframe """

    sub_dfs = self.apply(df, index)
    keys = sorted(sub_dfs.keys())

    serialized_index = [sub_dfs[key].index.values for key in keys]
    serialized_index = [l for sublist in serialized_index for l in sublist] # Flatten

    serialized_values = [sub_dfs[key].values.tolist() for key in keys]
    serialized_values = [l for sublist in serialized_values for l in sublist] # Flatten

    return (serialized_index, serialized_values)

  def __len__(self):
    return sum([x[3] for x in self.layout()])

  def __repr__(self):
    return '%s' % self.layout()
    s = u'Name: %s' % self.name
    for i,el in enumerate(self.layout()):
      s += u"""
      |Pos :{0:>5}
      |Column :{1:>5}
      |T_Min  :{2:>5}
      |T_Max  :{3:>5}
      |Length :{4:>5}
      """.format(i,el[0],el[1],el[2],el[3])
    return s

import unittest
class IndexMaskTest(unittest.TestCase):
  import pandas as pd
  from datetime import datetime, timedelta

  df = pd.DataFrame.from_dict({'Total_RV':range(0,1000), 'Total_RN':range(0,1000), 'Restaurant':range(0,1000)})

  start_date = datetime.strptime('2000-01-01', '%Y-%m-%d')
  df.index = [start_date + timedelta(days=x) for x in range(0,1000)]

  targets = [np.array(l) for l in [[6,13,20,27,34,41]]]
  inputs  = [np.array(l) for l in [[0,-6,-13,-20,-27,-34,-41],[-365]]]

  target_mask = IndexMask({'Restaurant':targets})
  input_mask = IndexMask({'Restaurant':inputs, 'Total_RN':inputs})

  def test_apply(self):
    applied_targets = self.target_mask.apply(self.df, 0)
    applied_inputs = self.input_mask.apply(self.df, 365)

    expected_targets = self.df.iloc[self.targets[0]]
    expected_inputs = self.df.iloc[(self.inputs[0] + 365).tolist() + [0]]

    first = applied_targets['Restaurant'].equals(expected_targets['Restaurant'])
    second = applied_inputs['Restaurant'].equals(expected_inputs['Restaurant'])

    self.assertTrue(first and second)

  def test_serialized(self):
    targets = self.target_mask.apply_serialize(self.df, 0)
    inputs = self.input_mask.apply_serialize(self.df, 365)

    first = targets[1] == [6, 13, 20, 27, 34, 41]
    second = inputs[1] == [365, 359, 352, 345, 338, 331, 324, 0, 365, 359, 352, 345, 338, 331, 324, 0]

    self.assertTrue(first and second)

if __name__ == "__main__":
  unittest.main()