# -*- encoding: utf-8 -*-
#
# Copyright (c) 2016 Chronos AS
#
# Authors: Fredrik Stormo, Stefan Remman
# Contact: kjetil.karlsen@chronosit.no

import matplotlib

import warnings
warnings.filterwarnings("ignore", module="matplotlib") # silence

import matplotlib.pyplot as plt
import d2o.utils.errors as err
import d2o.utils.logger as log
import numpy as np

class Trainer:
  """ Trains the neural network on the supplied dataset """
  def __init__(self, network, dataset):
    self.network = network
    self.dataset = dataset

    network.output_scaler = dataset.output_scaler
    network.input_scaler = dataset.input_scaler

    self.targets = dataset.output_scaler.inverse_transform(dataset.test_targets()).T
    self.targets_unscaled = dataset.test_targets().T

    target_mask = dataset.target_mask
    self.target_column = target_mask.masks.keys()[0] # TODO: Remove hardcoded fetching of first column (d2o specific)
    self.target_indices = target_mask.masks[self.target_column][0] # TODO: Remove hardcoded fetching of index 0
    self.errors = []

  def train_epochs_test(self, epochs=5000, learning_rate=0.009, momentum=0.5, update_rate=10, min_err=1e-3, plot=True):
    """ Used for testing. Contains plotting functionality """
    if (plot):
      fig, axes = plt.subplots(3)
      error_line, = axes[0].plot([], linewidth=2)
      target_line, = axes[1].plot(self.targets[0])
      output_line, = axes[1].plot([], linewidth=2)

    target_line_unscaled, = axes[2].plot(self.targets_unscaled[0], linewidth=2)
    output_line_unscaled, = axes[2].plot([], linewidth=2)

    for i in range(epochs):
      try:
        error = self.network.train_epoch(dataset=self.dataset, learning_rate=learning_rate, momentum=0.5)
        self.errors.append(error)

        if (i % update_rate == 0):
            # Bold driver
          if (len(self.errors) > 1):
            if ((error - self.errors[-2]) < 0):
              learning_rate = learning_rate + learning_rate*0.025
              learning_rate = learning_rate if (learning_rate <= 0.99) else 0.001
            elif ((error - self.errors[-2]) > 0.0000000001):
              print("Error increased, decreasing learning rate")
              learning_rate = learning_rate - learning_rate*0.50

          log.info('error: %s, learning_rate: %s' % (error, learning_rate))

          diffs = abs(np.diff(self.errors[-update_rate:]))
          roc = float(np.sum(diffs))

          all_outputs = self.network.run(self.dataset.test_inputs())
          all_outputs_unscaled = self.network.run(self.dataset.test_inputs(), inverse_transform=False)

          dims = {'t+%s' % self.target_indices[i]:all_outputs[:,i] for i in range(0, len(self.target_indices))}
          dims_unscaled = {'t+%s' % self.target_indices[i]:all_outputs_unscaled[:,i] for i in range(0, len(self.target_indices))}

          if (plot):
            axes[0].set_ylim([min(self.errors),max(self.errors)])
            axes[0].set_xlim([0,len(self.errors)])

            output_line.set_data(range(len(dims['t+7'])), dims['t+7'])
            output_line_unscaled.set_data(range(len(dims_unscaled['t+7'])), dims_unscaled['t+7'])

            print(dims_unscaled['t+7'][0])
            error_line.set_data(range(len(self.errors)), self.errors)

            plt.draw()
            plt.pause(0.0000001)

        if (error <= min_err):
          log.info("Reached minimum after {0} epochs".format(i))
          break

      except KeyboardInterrupt:
        print("Terminating")
        break

    return self.network

  def train_epochs(self, epochs=5000, learning_rate=0.009, momentum=0.5, update_rate=10, min_err=1e-3, plot=False):
    """ Train network for the chosen number of epochs.
    :param epochs: Number of epochs to train for
    :param learning_rate: Network learning rate
    :param momentum: Network learning momentum
    :param update_rate: Update plots and logging every update_rate tick
    :param min_err: Termination error
    :param plot: Create and update plots during training, default=False
    :returns: Trained network
    """
    if (plot):
      fig,axes = plt.subplots(4)
      error_line, = axes[0].plot([], linewidth=2)

      target_7_line, = axes[1].plot(self.targets[0], linewidth=2)
      output_7_line, = axes[1].plot([], linewidth=2)

      target_21_line, = axes[2].plot(self.targets[2], linewidth=2)
      output_21_line, = axes[2].plot([], linewidth=2)

      target_35_line, = axes[3].plot(self.targets[4], linewidth=2)
      output_35_line, = axes[3].plot([], linewidth=2)

      titlesize = 30
      subtitlesize = 20
      plt.suptitle('%s - %s' % (self.dataset.name, self.target_column), fontsize=titlesize)
      axes[0].set_title('Training error', fontsize=subtitlesize)
      axes[1].set_title('%s (blue), output (green)' % self.target_column + " t+7", fontsize=subtitlesize)
      axes[2].set_title('%s (blue), output (green)' % self.target_column + " t+21", fontsize=subtitlesize)
      axes[3].set_title('%s (blue), output (green)' % self.target_column + " t+35", fontsize=subtitlesize)

      mae_text = axes[0].text(0.95, 0.95, 'MAE : {0:.1f}'.format(np.inf),
        verticalalignment='bottom', horizontalalignment='right',
        transform=axes[0].transAxes, fontsize=subtitlesize)
      mape_text = axes[0].text(0.95, 0.90,'MAPE: {0:.1f}%'.format(np.inf),
        verticalalignment='bottom', horizontalalignment='right',
        transform=axes[0].transAxes, fontsize=subtitlesize)

    for i in range(epochs):
      try:
        error = self.network.train_epoch(dataset=self.dataset, learning_rate=learning_rate, momentum=0.5)

        if (i % update_rate == 0):
          # Bold driver
          if (len(self.errors) > 0):
            if ((error - self.errors[-1]) <= 0):
              learning_rate = learning_rate + learning_rate*0.025
              learning_rate = learning_rate if (learning_rate <= 0.99) else 0.001
            elif ((error - self.errors[-1]) > 0.0000000001):
              log.info("Error increased, decreasing learning rate")
              learning_rate = learning_rate - learning_rate*0.50

          self.errors.append(error)
          diffs = abs(np.diff(self.errors[-update_rate:]))
          roc = float(np.sum(diffs))

          all_outputs = self.network.run(self.dataset.test_inputs())
          dims = {'t+%s' % self.target_indices[i]:all_outputs[:,i] for i in range(0, len(self.target_indices))}

          abs_err = err.absolute_error(dims['t+7'], self.targets[0])
          rel_err = err.relative_error(dims['t+7'], self.targets[0])
          per_err = err.percent_error(dims['t+7'], self.targets[0])

          top_80 = int(len(abs_err)*.80)
          error_metrics = {'mae':np.median(sorted(abs_err)[:top_80]), 'mape':np.median(sorted(per_err)[:top_80])}
          log.info("{0}: {1:.6f} roc: {2:.4f}, lr: {3:.4f}".format(i,error, roc, learning_rate))

          if (plot):
            axes[0].set_ylim([min(self.errors),max(self.errors)])
            axes[0].set_xlim([0,len(self.errors)])

            error_line.set_data(range(0,len(self.errors)),self.errors)

            output_7_line.set_data(range(0,len(dims['t+7'])), dims['t+7'])
            output_21_line.set_data(range(0,len(dims['t+21'])), dims['t+21'])
            output_35_line.set_data(range(0,len(dims['t+35'])), dims['t+35'])

            mae_text.set_text('MAE : {0:.1f}'.format(error_metrics['mae']))
            mape_text.set_text('MAPE: {0:.1f}%'.format(error_metrics['mape']))

            plt.draw()
            plt.pause(0.0000001)

        if (error <= min_err):
          log.info("Reached minimum after {0} epochs".format(i))
          break

      except KeyboardInterrupt:
        print("Terminating")
        break

    return self.network
