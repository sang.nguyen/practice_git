# -*- encoding: utf-8 -*-
import pandas as pd
import numpy as np
from datetime import timedelta, datetime
import time
import os
import pickle
import ConfigParser
import argparse

import plot_driver_df_ver_1
import create_driver_df
import fill_capture_arr
import capture_arr
import project_lib as lib
import department_correlation


import d2o.utils.putils as put
from d2o.database import Database



def create_filled_list(df, hotel_id):
    '''
    create a filled list for a hotel
    :param df: a dataframe that has rn, gn, rv data
    :param hotel_id: hotel_id in string type
    :return: a list of filled data column names
    '''
    filled_list = []
    for i in df.columns:
        if hotel_id in i:
            filled_list.append(i)
    return filled_list


def add_total_to_df(df):
    '''
    add total columns for dataframe
    :param df: the dataframe need to add total columns
    :return: a data frame that have total columns for all departments
    '''
    new_df = df[df.columns]
    dep_list = set(i.split("_")[0] for i in new_df.columns) - {"date"}
    for dep in dep_list:
        other_seg = []
        if (dep + "_0") not in new_df.columns:
            other_seg = [i for i in new_df.columns if dep in i]
            new_df[dep + "_0"] = new_df[other_seg].sum(axis = 1)
    return new_df


def add_total_segname(segment_name):
    '''
    add a row for total segment of all departments for a dataframe
    :param segment_name: a dataframe that has the segment names data
    :return: a data frame that have total segment rows of all departments for a dataframe
    '''
    new_df = segment_name[segment_name.columns]
    dep_list = set(i.split("_")[0] for i in new_df["id"])
    for dep in dep_list:
        if (dep + "_0") not in list(new_df["id"]):
            print(dep)
            print((dep + "_0") in list(new_df["id"]))
            count = len(new_df)
            other_seg = [i for i in new_df["id"] if dep in i]
            copy_id =  new_df[new_df.id == other_seg[0]].index[0]
            new_df.loc[count] = new_df.loc[copy_id]
            new_df.loc[count, ["id", "name"]] = [dep + "_0", new_df.loc[copy_id, "name"].split("_")[0] + "_Total"]
    new_df = new_df.reset_index()
    return new_df


def shift_data(df, segment_name, list_shifted_names):
    '''
    we shift some columns of a dataframe to 1 day later
    :param df: a pandas dataframe
    :param segment_name: segment_name of a hotel
    :param list_names: a list of segment names needs to shift
    :return: dataframe that has shifted columns where needed
    '''
    new_df = df[df.columns]
    for name in list_shifted_names:
        list_col_names = [i for i in segment_name["name"] if name in i]        
        list_id = []
        for seg_name in list(segment_name["name"]):
            if name in seg_name:
                id_ = segment_name.ix[segment_name.name == seg_name]["id"].iloc[0]
                new_df[id_] = new_df[id_].shift(1)

    return new_df

def dominant_and_fill(file_df_driver, hotel_id, df_driver, df_insample):
    # # # WARNING: 
    # # # this code is no longer used, we use fill_capture_arr.fill_capture_arr instead
    df_driver['index_order'] = range(len(df_driver))

    df_driver['ARR'] = -1
    df_driver['ARR_unit'] = -1
    df_driver['capture_unit'] = -1
    df_driver['capture'] = -1

    idx = pd.IndexSlice
    #Find dominant driver
    cols = df_driver['col'].unique()
    dates_of_df = df_driver['date']
    df_driver = df_driver.set_index(['col', 'date']).sort_index()
    for col in cols:
        dep = col.split('_')[0]
        print col
        l_season_tf = lib.read_dep_season_timeframe(dep, hotel_id)
        for i, season_tf in enumerate(l_season_tf):
            dates_in_season = lib.get_season_dates_from_tf(season_tf, dates_of_df)
            for day_of_week in range(7):
                weekdays_in_season = lib.get_dates_by_dayWeek(dates_in_season, day_of_week)
                dominant_driver = df_driver.loc[idx[col, weekdays_in_season],'col_d'].value_counts().index[0]
                df_driver.loc[idx[col, weekdays_in_season],'col_d'] = dominant_driver
                lag = int(dominant_driver.split("_")[3])
                col_d = "_".join(dominant_driver.split("_")[:3])
                result = capture_arr.compute_capture_arr(col, col_d, lag, season_tf, num_day, day_of_week, df_insample)
                df_driver.loc[idx[col, weekdays_in_season], 'ARR'] = result['ARR']
                df_driver.loc[idx[col, weekdays_in_season], 'ARR_unit'] = result['ARR_unit']
                df_driver.loc[idx[col, weekdays_in_season], 'capture_unit'] = result['capture_unit']
                df_driver.loc[idx[col, weekdays_in_season], 'capture'] = result['capture']

    df_driver = df_driver.reset_index()
    df_driver = df_driver.set_index('index_order').sort_index()
    df_driver.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, file_df_driver)), index=False)


def create_dominant_df(driver_df, period_dict):
    new_df = driver_df[driver_df.columns]
    new_df["date"] = pd.to_datetime(new_df["date"])

    new_df['index_order'] = range(len(new_df))

    idx = pd.IndexSlice
    #Find dominant driver
    cols = new_df['col'].unique()
    dates_of_df = new_df['date']
    new_df = new_df.set_index(['col', 'date']).sort_index()
    for col in cols:
        dep = col.split('_')[0]
        l_season_tf = period_dict[dep]
        for i, season_tf in enumerate(l_season_tf):
            dates_in_season = lib.get_season_dates_from_tf(season_tf, dates_of_df)
            for day_of_week in range(7):
                try:
                    weekdays_in_season = lib.get_dates_by_dayWeek(dates_in_season, day_of_week)
                    dominant_driver = new_df.loc[idx[col, weekdays_in_season],'col_d'].value_counts().index[0]
                    new_df.loc[idx[col, weekdays_in_season],'col_d'] = dominant_driver
                    lag = int(dominant_driver.split("_")[3])
                    col_d = "_".join(dominant_driver.split("_")[:3])
                except:
                    pass

    new_df = new_df.reset_index()
    new_df = new_df.set_index('index_order').sort_index()
    return new_df

def convert_date_df(file_df_driver, period_dict, temp_year):
    # # # WARNING: 
    # # # this code is no longer used, because we do not need to 
    # # # change date of driver df anymore
    df_driver = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_df_driver), index_col=False)
    df_driver['date'] = pd.to_datetime(df_driver['date'])
    ### add more
    df_driver["season_tf"] = 1
    df_driver["day_of_week"] = df_driver['date'].dt.dayofweek

    year_target = [int(temp_year)]

    df_target = df_driver[df_driver.columns]
    df_target['date'] = df_driver['date'] + pd.DateOffset(years= (year_target[0] - year_input[0]))
    df_target["day_of_week"] = df_target['date'].dt.dayofweek

    print("ok, will predict for year %s" %temp_year)
    for id in df_driver.index:
        col = df_driver.iloc[id]['col']
        ids = col.split("_")
        if id % 100 == 0:
            print(id)        
        try:
            day_cv = df_driver.iloc[id]['date'] + pd.DateOffset(years= (2016 - df_driver.iloc[id]['date'].year))
            season_tf_driver, day_of_week_driver = lib.find_out_season(period_dict[ids[0]], day_cv)
            season_tf_target, day_of_week_target = lib.find_out_season(period_dict[ids[0]], day_cv)
        except:
            season_tf_driver, day_of_week_driver = [("01-01", "12-31")], df_driver.iloc[id]['date'].weekday()
            season_tf_target, day_of_week_target = [("01-01", "12-31")], df_target.iloc[id]['date'].weekday()
        
        df_driver.loc[id, "season_tf"] = str(season_tf_driver)
        df_target.loc[id, "season_tf"] = str(season_tf_target)

    for id in df_target.index:
        try:
            driver_id = df_driver[(df_driver.col == df_target.iloc[id]["col"]) & \
                                (df_driver.season_tf == df_target.iloc[id]["season_tf"]) & \
                                (df_driver.day_of_week == df_target.iloc[id]["day_of_week"])].index[0]
            # print(driver_id)
            df_target.loc[id] = [df_target.loc[id][df_target.columns[0]]] + \
                                list(df_target.loc[id][["date"]]) + \
                                [df_target.loc[id][df_target.columns[2]]] + \
                                list(df_driver.loc[driver_id][df_driver.columns[3:9]]) + \
                                list(df_target.loc[id][["season_tf", "day_of_week"]])
            if id % 100 == 0:
                print(id)
        except:
            pass
    df_target.to_csv(os.path.join('data', '%s'%hotel_id,'year_{}'.format(file_df_driver)))


def predict_df(file_df_driver_union, df, model = "error"):
    '''
    In case of out_sample predict type, we create a dataframe that is filled by
    our predict method and export it in .csv file as output
    :param file_df_driver_union: name of a .csv file that represents the dataframe
    that has dominant driver and filled arr and capture.
    :param df: a dataframe that has filled data
    :param model: (default "error") type of model, choose either "error", "corr" 
    or "regression" ("corr" and "error" give the same result)
    '''
    
    df_driver_union = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_df_driver_union), index_col=False)

    df_driver_union['date'] = pd.to_datetime(df_driver_union['date'])
    df_driver_union['filled_values'] = -1
    df_driver_union['values'] = -1
    df_driver_union['col_d_values'] = -1
    df_new_data = df[df.columns]
    df_new_data.index = df_new_data["date"]
    for id in df_driver_union.index:
        if id % 100 == 0:
            print(id)
        col = df_driver_union.iloc[id]['col']
        col_d = df_driver_union.iloc[id]['col_d']
        lag = int(col_d.split("_")[3])
        col_d = "_".join(col_d.split("_")[:3])
        type_col = col.split("_")[-1]
        date = df_driver_union.iloc[id]['date']

        col_d_values = df_new_data[df_new_data['date'] == (date-timedelta(days=lag))][col_d]
        # print(date, lag, col_d, col_d_values)
        col_values = df[df['date'] == date][col]
        if ((model == "error") or (model == "corr")):
            if type_col == 'rv':
                ARR = df_driver_union.loc[id]['ARR']
                capture = df_driver_union.loc[id]['capture']
            else:
                ARR = df_driver_union.loc[id]['ARR_unit']
                capture = df_driver_union.loc[id]['capture_unit']
            filled_values = float(ARR*capture*col_d_values)
        elif model == "regression":
            coef = df_driver_union.loc[id]['coef']
            intercept = df_driver_union.loc[id]['intercept']
            filled_values = float(coef * col_d_values + intercept)

        df_driver_union.loc[id, 'filled_values'] = filled_values
        df_driver_union.loc[id, 'values'] = float(col_values)
        df_driver_union.loc[id, 'col_d_values'] = float(col_d_values)
        df_new_data.loc[date, col] = filled_values
    df_driver_union.to_csv(os.path.join('data', '%s'%hotel_id, 'predicted_{}'.format(file_df_driver_union)), index=False)
    df_new_data.to_csv(os.path.join('data', '%s'%hotel_id, 'store_df_{}'.format(file_df_driver_union)), index=False)


def predict_in_sample(file_df_driver_union, df, model = "error"):
    '''
    In case of in_sample predict type, we create a dataframe that is filled by
    our predict method and export it in .csv file as output
    :param file_df_driver_union: name of a .csv file that represents the dataframe
    that has dominant driver and filled arr and capture.
    :param df: a dataframe that has filled data
    :param model: (default "error") type of model, choose either "error", "corr" 
    or "regression" ("corr" and "error" give the same result)
    '''
    df_driver_union = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_df_driver_union), index_col=False)

    df_driver_union['date'] = pd.to_datetime(df_driver_union['date'])
    df_driver_union['filled_values'] = -1
    df_driver_union['values'] = -1
    df_driver_union['col_d_values'] = -1
    for id in df_driver_union.index:
        if id % 100 == 0:
            print(id)
        col = df_driver_union.iloc[id]['col']
        col_d = df_driver_union.iloc[id]['col_d']
        lag = int(col_d.split("_")[3])
        col_d = "_".join(col_d.split("_")[:3])
        type_col = col.split("_")[-1]
        date = df_driver_union.iloc[id]['date']

        col_d_values = df[df['date'] == (date-timedelta(days=lag))][col_d]
        # print(date, lag, col_d, col_d_values)
        col_values = df[df['date'] == date][col]
        if ((model == "error") or (model == "corr")):
            if type_col == 'rv':
                ARR = df_driver_union.loc[id]['ARR']
                capture = df_driver_union.loc[id]['capture']
            else:
                ARR = df_driver_union.loc[id]['ARR_unit']
                capture = df_driver_union.loc[id]['capture_unit']
            filled_values = float(ARR*capture*col_d_values)
        elif model == "regression":
            coef = df_driver_union.loc[id]['coef']
            intercept = df_driver_union.loc[id]['intercept']
            filled_values = float(coef * col_d_values + intercept)

        df_driver_union.loc[id, 'filled_values'] = filled_values
        df_driver_union.loc[id, 'values'] = float(col_values)
        df_driver_union.loc[id, 'col_d_values'] = float(col_d_values)
    df_driver_union.to_csv(os.path.join('data', '%s'%hotel_id, 'predicted_{}'.format(file_df_driver_union)), index=False)



def prepare_plot_df(file_plot_df):
    '''
    load data from csv file and do some preprocessing works
    :param file_plot_df: name of .csv file
    :return: a preprocessed dataframe
    '''
    if type(file_plot_df) == type(None):
        return None
    else:
        plot_df = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_plot_df), index_col=False)
        plot_df["date"] = pd.to_datetime(plot_df['date'])
        plot_df["day_of_week"] = plot_df['date'].dt.dayofweek
        return plot_df


def cv_period_df(temp_df, current_time):
    new_df = temp_df[temp_df["date_from"].dt.year == current_time.year]
    new_df = new_df.sort_values('date_from', ascending=True)
    new_df["from"] = new_df["date_from"].dt.strftime("%m-%d")
    new_df["to"] = new_df["date_to"].dt.strftime("%m-%d")
    new_df["season"] = new_df["period_id"]
    target_df = new_df[["from", "to", "season"]]
    return target_df


def cv_period_id_dict(temp_df, current_time):
    new_df = temp_df[temp_df["date_from"].dt.year == current_time.year]
    new_df = new_df.sort_values('date_from', ascending=True)
    new_df["from"] = new_df["date_from"].dt.strftime("%m-%d")
    new_df["to"] = new_df["date_to"].dt.strftime("%m-%d")
    new_df["season"] = new_df["period_id"]
    target_df = new_df[["from", "to", "season"]]
    seasons = target_df['season'].unique()
    target_df = target_df.set_index('season')
    ss_list = [zip(pd.Series(target_df.loc[season, 'from']),pd.Series(target_df.loc[season, 'to'])) for season in seasons]
    period_df = pd.DataFrame(["period_id", "timeframe"])
    count = 0
    for season in seasons:
        period_df.loc[count, "period_id"] = season
        period_df.loc[count, "timeframe"] = [zip(pd.Series(target_df.loc[season, 'from']),pd.Series(target_df.loc[season, 'to']))]
        count += 1
    return period_df


def cv_top_df(top_df, period_id_dict):
    feature_list = ["h_id", "period_id", "day", "source_h_id", \
        "source_segment_id", "destination_segment_id", "priority", "property", \
        "driver_type", "source_day_offset", "destination_day_offset"]
    driver_type = {"rv": 3,
                    "rn": 1,
                    "gn": 2}
    new_top_df = top_df[top_df.columns]
    new_df = pd.DataFrame(0, index=np.arange(len(new_top_df)), columns=feature_list)
    new_df["priority"]    = new_df.index % 3 + 1
    new_top_df['date']    = pd.to_datetime(new_top_df['date'])
    new_top_df["day"]     = new_top_df['date'].dt.dayofweek
    new_df["day"]         = new_top_df["day"]
    new_df["property"]    = new_top_df["cov_value"]
    new_df["h_id"]        = new_top_df["col"].apply(lambda x: int(x.split("_")[0]))
    # new_df["source_h_id"] = new_top_df["cov_col"].apply(lambda x: int(x.split("_")[0]))
    new_df["source_segment_id"]      = new_top_df["cov_col"].apply(lambda x: int(x.split("_")[1]))
    new_df["destination_segment_id"] = new_top_df["col"].apply(lambda x: int(x.split("_")[1]))
    new_df["driver_type"]            = new_top_df["cov_col"].apply(lambda x: driver_type[x.split("_")[2]])
    new_df["source_day_offset"]      = new_top_df["lag"]
    new_df["key"]                    = new_top_df["col"].apply(lambda x: int(x.split("_")[1]))
    for ind in new_df.index:
        if ind % 100 == 0:
            print(ind)
        col = df_driver_union.iloc[ind]['col']
        dep = col.split("_")[0]
        new_df.loc[ind, "source_h_id"] = dep
        new_df.loc[ind, "period_id"] = lib.find_out_period_id(period_id_dict[dep], new_df.loc(ind, "date"))
    new_df = new_df
    return new_df[feature_list]

if __name__ == "__main__":

    # ===================== PREPARE DATA ========================
    # ===========================================================

    # # ========= INPUT ===============
    # parser = argparse.ArgumentParser("Generates drivers and writes result to database", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # parser.add_argument("-H", "--host", help="Database host", required=True)
    # parser.add_argument("-U", "--user", help="Database user name", required=True)
    # parser.add_argument("-P", "--password", help="Database password", required=True)
    # parser.add_argument("-d", "--database", help="Database", required=True)
    # parser.add_argument("-s", "--schema", help="Database schema", default="dbo")
    # parser.add_argument("-i", "--hotel-ids", help="Comma separated list of hotel id's or 'all'", default="all")
    # parser.add_argument("-Y", "--years", help="Number of years of data to include", default=3)
    # parser.add_argument("-N", "--no-write", help="Run, but do not write results to database", action="store_true", default=False)


    # args = parser.parse_args()

    # hotel_id = args.hotel_ids
    hotel_id = "223"
    print(hotel_id)
    
    # # choose: "in_sample" / "out_sample"
    # type_predict = "in_sample"
    # ## choose: "error" / "regression" / "corr"
    model = "error"
    otb_run = "no"

    years_back = 3
    # years_back = int(args.years)

    current_time = datetime.now()
    # tf_input_raw = "01-01-2012, 12-31-2015"
    # season_target = [("01-01-2015", "12-31-2015")]
    start_str = (current_time - timedelta(days = 365 * years_back + 2)).strftime('%m-%d-%Y')
    end_str = (current_time - timedelta(days = 2)).strftime('%m-%d-%Y')
    curr_str = current_time.strftime('%m-%d-%Y')
    predict_str = (current_time + timedelta(days = 365 * 1 - 1)).strftime('%m-%d-%Y')
    tf_input_raw = "%s, %s" % (start_str, end_str)
    season_target = [(curr_str, predict_str)]

    # list_shifted_names = ["Breakfast"]

    username = "tp7"
    password = "reHj456"
    host = "172.16.0.50"
    database = "pmi_tp7"
    schema = None

    # host = args.host
    # user = args.user
    # password = args.password
    # dbname = args.database
    # schema = args.schema

    # # # # ========== SOME WORK WITH INPUT =========
    start_time1 = time.time()
    start_time = time.time()

    # db_01 = Database(uid=username, pwd=password, host=host, dbname=database, schema=schema)

    # tf_input = tf_input_raw.replace(" ", "")
    # tf_input = tf_input.split(",")

    # start_date = datetime(int(tf_input[0].split("-")[2]), \
    #     int(tf_input[0].split("-")[0]), int(tf_input[0].split("-")[1]))
    # end_date = datetime(int(tf_input[1].split("-")[2]), \
    #     int(tf_input[1].split("-")[0]), int(tf_input[1].split("-")[1])) + timedelta(days = 1)

    # from_date = db_01.earliest_date(hotel_id)
    # to_date   = db_01.latest_date(hotel_id)

    # if start_date < from_date:
    #     print("the earliest date in DB is: %s"%from_date)
    #     start_date = from_date
    # else:
    #     pass
    # if end_date > to_date:
    #     print("the latest date in DB is: %s"%to_date)
    #     end_date   = to_date
    # else:
    #     pass

    # rv_df = db_01.revenue(hotel_id = hotel_id, from_date = start_date, to_date = end_date)

    # # rn_df = db_01.room_nights(hotel_id = hotel_id, from_date = start_date + timedelta(days = 1), to_date = end_date)
    # # gn_df = db_01.guests_night(hotel_id = hotel_id, from_date = start_date + timedelta(days = 1), to_date = end_date)
    # rn_df = db_01.room_nights(hotel_id = hotel_id, from_date = start_date, to_date = end_date)
    # print("lay xong rn")
    # gn_df = db_01.guests_night(hotel_id = hotel_id, from_date = start_date, to_date = end_date)
    # segment_name = db_01.segments(hotel_id)
    # print("lay xong segment")

    # # segment_name = pd.read_csv(os.path.join('data', '%s'%hotel_id,'hotel_14_segment_name.csv'))
    # # # hotel_id = str(segment_name["hotel_id"].iloc[0])

    # # rn_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'14_rn_full.csv'))
    # # rv_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'14_rv_full.csv'))
    # # gn_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'14_gn_full.csv'))

    # rn_df = rn_df.fillna(0)
    # rv_df = rv_df.fillna(0)
    # gn_df = gn_df.fillna(0)
    # rn_df = put.impute(rn_df, method='zero')
    # rn_df[rn_df < 0.0] = 0.0
    # rv_df = put.impute(rv_df, method='zero')
    # rv_df[rv_df < 0.0] = 0.0
    # gn_df = put.impute(gn_df, method='zero')
    # gn_df[gn_df < 0.0] = 0.0

    # rn_df["date"] = rn_df.index
    # rn_df = rn_df.reset_index(drop = True)
    # gn_df = gn_df.reset_index(drop = True)
    # rv_df = rv_df.reset_index(drop = True)

    # segment_name = add_total_segname(segment_name)
    # rn_df = add_total_to_df(rn_df)
    # gn_df = add_total_to_df(gn_df)
    # rv_df = add_total_to_df(rv_df)
    # rn_df = rn_df.loc[:, (rn_df != 0).any()]
    # gn_df = gn_df.loc[:, (gn_df != 0).any()]

    # # rv_df = shift_data(rv_df, segment_name, list_shifted_names)
    # # rv_df = rv_df.drop(rv_df.head(1).index)
    # # rv_df = rv_df.reset_index(drop = True)

    # date_s = rn_df['date']
    # del rn_df['date']
    # # del rv_df['date']
    # # del gn_df['date']

    # rn_df.columns = ["{0}_rn".format(col) for col in rn_df.columns]
    # rv_df.columns = ["{0}_rv".format(col) for col in rv_df.columns]
    # gn_df.columns = ["{0}_gn".format(col) for col in gn_df.columns]

    # df = pd.concat([date_s, rv_df, rn_df, gn_df], axis=1)
    # df['date'] = pd.to_datetime(df['date'])
    # df['day_of_week'] = df['date'].dt.dayofweek


    # year_input = range(int(tf_input[0].split("-")[2]), int(tf_input[1].split("-")[2]) + 1)
    # name_year = str(year_input).replace(" ", "").replace("[", "").replace("]", "").replace(",", "_")


    # year_target = range(int(season_target[0][0].split("-")[2]), int(season_target[0][1].split("-")[2]) + 1)
    # # temp_year = "2015"
    # # year_target = [int(temp_year)]

    # df_full = df[df.columns]
    # df_insample = df[(df['date'] >= tf_input[0]) & (df['date'] <= tf_input[1])]


    # all_col = set(df.columns) - {'date', 'day_of_week'}

    # all_rv_col = list(all_col)
    # temp_list = list(all_col)
    # all_rv_col_without_hid = list(all_rv_col)
    # for i in temp_list:
    #     if (("rn" in i) or ("gn" in i)):
    #         all_rv_col.remove(i)
    #         all_rv_col_without_hid.remove(i)
    #     if (hotel_id in i):
    #         try:
    #             all_rv_col_without_hid.remove(i)
    #         except:
    #             pass
    # all_dep = set([i.split("_")[0] for i in all_col])
    # all_dep_wo_hid = all_dep - {hotel_id}

    # print("hi")
    # # period_dict = {}
    # # for i,col in enumerate(all_rv_col_without_hid):
    # #     ids = col.split("_")
    # #     try:
    # #         # maybe this code make the file slow
    # #         period_dict[ids[0]] = lib.read_dep_season_timeframe(ids[0], hotel_id)
    # #     except:
    # #         pass

    # period_dict = {}
    # period_id_dict = {}
    # for i,dep in enumerate(all_dep_wo_hid):
    #     try:
    #         temp_df = db_01.get_season_all_definition(dep)
    #         period_df = cv_period_df(temp_df, current_time)
    #         period_dict[dep] = lib.read_dep_ss_tf(period_df)
    #         period_id_dict[dep] = cv_period_id_dict(temp_df, current_time)
    #     except:
    #         pass

    # significance_level = 0.05
    # num_day = 10

    # filled_list = create_filled_list(df, hotel_id)
    # print("hi")

    # print("--- %s seconds ---" % (time.time() - start_time))

    # # ================= CREATE CORR DICTIONARY ==================
    # # ===========================================================
    # start_time = time.time()
    # dict_df_error = {}
    # dict_df_corr = {}
    # dict_df_error_week = {}
    # dict_df_corr_week = {}
    # for col in all_rv_col_without_hid: # to do # done
    #     print(col)
    #     dep_id = col.split("_")[0]
    #     seg_id = col.split("_")[1]
    #     # l_season_tf = lib.read_dep_season_timeframe(dep_id, hotel_id)
    #     l_season_tf = period_dict[dep_id]
    #     for season_tf in l_season_tf:
    #         # print(season_tf)
    #         key = "{0}_{1}".format(col, season_tf)
    #         date_ss = lib.generate_season_date(season_tf, year_input)
    #         date_ss.sort()
    #         if model == "error":
    #             # dict_df_error_week[key] = department_correlation.compute_corr_ver_2_week(dep_id, seg_id, season_tf,\
    #             #                                    significance_level, df_insample, year_input)
    #             dict_df_error_week[key] = department_correlation.compute_corr_ver_2_week_1(dep_id, seg_id, season_tf,\
    #                                                significance_level, df_insample, year_input, date_ss)
    #         if model == "regression" or model == "corr":
    #             dict_df_corr_week[key] = department_correlation.compute_corr_ver_3_week_1(dep_id, seg_id, season_tf,\
    #                                                significance_level, df_insample, date_ss)

    #         for day_of_week in xrange(7):
    #             # print col, season_tf, day_of_week
    #             key = "{0}_{1}_{2}".format(col, season_tf, day_of_week)
    #             if model == "error":
    #                 # dict_df_error[key] = department_correlation.compute_corr_ver_2(dep_id, seg_id, season_tf, num_day,\
    #                 #                                    day_of_week, significance_level, df_insample)
    #                 dict_df_error[key] = department_correlation.compute_corr_ver_2_1(dep_id, seg_id, season_tf, num_day,\
    #                                                    day_of_week, significance_level, df_insample, date_ss)
    #             if model == "regression" or model == "corr":
    #                 dict_df_corr[key] = department_correlation.compute_corr_ver_3_1(dep_id, seg_id, season_tf, num_day,\
    #                                                    day_of_week, significance_level, df_insample, date_ss)
    # print("--- %s seconds ---" % (time.time() - start_time))
    # if model == "error":
    #     pickle.dump(dict_df_error, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_error"), "wb"))
    #     pickle.dump(dict_df_error_week, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_error_week"), "wb"))
    # elif ((model == "regression") or (model == "corr")):
    #     pickle.dump(dict_df_corr, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_corr"), "wb"))
    #     pickle.dump(dict_df_corr_week, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_corr_week"), "wb"))

    # print("--- %s seconds ---" % (time.time() - start_time))


    # ==================== CREATE DRIVER DF =====================
    # ===========================================================
    start_time = time.time()
    driver_ver_09_cl = create_driver_df.create_driver_df_cl(df, tf_input, year_input, \
                        filled_list, significance_level, segment_name, period_dict, num_day = num_day, model = model)

    try:
        if model == "error":
            with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_error'), 'rb') as handle:
                dict_store = pickle.load(handle)
                driver_ver_09_cl.df_seg_dict = dict_store
            with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_error_week'), 'rb') as handle:
                dict_store_week = pickle.load(handle)
                driver_ver_09_cl.df_seg_dict_week = dict_store_week
        elif model == "regression" or model == "corr":
            with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_corr'), 'rb') as handle:
                dict_store = pickle.load(handle)
                driver_ver_09_cl.df_seg_dict = dict_store
            with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_corr_week'), 'rb') as handle:
                dict_store_week = pickle.load(handle)
                driver_ver_09_cl.df_seg_dict_week = dict_store_week
    except:
        driver_ver_09_cl.prepare_dict()

    # driver_ver_09, top_ver_09 = driver_ver_09_cl.create_both_driver_top_ver_2(season_target)
    driver_ver_09 = driver_ver_09_cl.create_driver_ver_2(season_target)
    print("--- %s seconds ---" % (time.time() - start_time))

    # driver_ver_09.to_csv(os.path.join('data', '%s'%hotel_id,'driver_%s_%s.csv'%(name_year, model)))
    # top_ver_09.to_csv(os.path.join('data', '%s'%hotel_id,'top_df_%s_%s.csv'%(name_year, model)))
    dominant_df = create_dominant_df(driver_ver_09, period_dict)
    print("--- %s seconds ---" % (time.time() - start_time))
    top_df_01 = driver_ver_09_cl.create_top_all(season_target, dominant_df)

    # top_df_db = cv_top_df(top_ver_09, period_id_dict)
    # if (write):
    #     db_01.write_topdriver(top_df_db)

    print("--- %s seconds ---" % (time.time() - start_time))
    print("--- %s seconds ---" % (time.time() - start_time1))






















    # # df_driver_rn = driver_ver_09[driver_ver_09.columns]
    # # df_driver_gn = driver_ver_09[driver_ver_09.columns]
    # # df_driver_rn['col_d'] = '%s_0_rn_1'%hotel_id
    # # df_driver_gn['col_d'] = '%s_0_gn_1'%hotel_id

    # # df_driver_rn.to_csv(os.path.join('data', '%s'%hotel_id,'rn_1driver_%s_%s.csv'%(name_year, model)))
    # # df_driver_gn.to_csv(os.path.join('data', '%s'%hotel_id,'gn_1driver_%s_%s.csv'%(name_year, model)))
    # # ============= DOMINANT DRIVER + FILL CAP & ARR ============
    # # ===========================================================

    # name_file_driver = 'driver_%s_%s.csv'%(name_year, model)
    # name_file_driver_rn = "rn_1driver_%s_%s.csv"%(name_year, model)
    # name_file_driver_gn = "gn_1driver_%s_%s.csv"%(name_year, model)
    # # df_driver = prepare_plot_df(name_file_driver)
    # # df_driver_rn = df_driver[df_driver.columns]
    # # df_driver_gn = df_driver[df_driver.columns]
    # # df_driver_rn['col_d'] = '%s_0_rn_1'%hotel_id
    # # df_driver_gn['col_d'] = '%s_0_gn_1'%hotel_id


    # # dominant_and_fill(name_file_driver, df_driver, df_insample)
    # # dominant_and_fill(name_file_driver_rn, df_driver_rn, df_insample)
    # # dominant_and_fill(name_file_driver_gn, df_driver_gn, df_insample)

    # if model == "regression":
    #     type_fill = 'regr'
    # else:
    #     type_fill = 'arr'
    # file_driver = os.path.join('data', '%s'%hotel_id, 'driver_%s_%s.csv'%(name_year, model))
    # filled_df_driver = fill_capture_arr.fill_driver(file_driver, df_insample, hotel_id, type= type_fill)
    # filled_df_driver.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_file_driver)), index=False)

    # # file_driver_rn = os.path.join('data', '%s'%hotel_id, 'rn_1driver_%s_%s.csv'%(name_year, model))
    # # filled_df_driver_rn = fill_capture_arr.fill_driver(file_driver_rn, df_insample, hotel_id, type= type_fill)
    # # filled_df_driver_rn.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_file_driver_rn)), index=False)

    # # file_driver_gn = os.path.join('data', '%s'%hotel_id, 'gn_1driver_%s_%s.csv'%(name_year, model))
    # # filled_df_driver_gn = fill_capture_arr.fill_driver(file_driver_gn, df_insample, hotel_id,type= type_fill)
    # # filled_df_driver_gn.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_file_driver_gn)), index=False)




    # # # ======================= PREDICTION ========================
    # # # ===========================================================

    # df_test = df[df.columns]
    # not_change_list = filled_list + ["date", "day_of_week"]
    # df_test[list(set(df.columns) - set(not_change_list))] = 0
    # df_test.ix[df_test.date < pd.to_datetime("2015-01-01")] = df.ix[df.date < pd.to_datetime("2015-01-01")]


    # start_time = time.time()
    # file_df_driver_union = "filled_%s_0_"%hotel_id + name_file_driver
    # file_df_driver_union_rn = "filled_%s_0_"%hotel_id + name_file_driver_rn
    # file_df_driver_union_gn = "filled_%s_0_"%hotel_id + name_file_driver_gn

    # if type_predict == "in_sample":
    #     predict_in_sample(file_df_driver_union, df_full, model = model)
    #     predict_in_sample(file_df_driver_union_rn, df_full, model = model)
    #     predict_in_sample(file_df_driver_union_gn, df_full, model = model)
    #     print("--- %s seconds ---" % (time.time() - start_time))
    # else:
    #     predict_df(file_df_driver_union, df_full, model = model)
    #     predict_df(file_df_driver_union_rn, df_full, model = model)
    #     predict_df(file_df_driver_union_gn, df_full, model = model)
    #     print("--- %s seconds ---" % (time.time() - start_time))




    # # # # ======================= PLOT SAMPLE =======================
    # # # # ===========================================================

    # file_plot_df = "predicted_" + file_df_driver_union
    # file_plot_df_rn = "predicted_" + file_df_driver_union_rn
    # file_plot_df_gn = "predicted_" + file_df_driver_union_gn

    # # file_plot_df = "predicted_filled_223_0_driver_2012_2015_error.csv"
    # # file_plot_df_rn = "predicted_filled_223_0_rn_1driver_2012_2015_error.csv"
    # # file_plot_df_gn = "predicted_filled_223_0_gn_1driver_2012_2015_error.csv"

    # plot_df = prepare_plot_df(file_plot_df)
    # plot_df_rn = prepare_plot_df(file_plot_df_rn)
    # plot_df_gn = prepare_plot_df(file_plot_df_gn)


    # # # ============ OTB ============
    # # # =============================
    # if otb_run == "yes":
    #     otb_dict = {}
    #     snapshot_date = pd.to_datetime(season_target[0][1])
    #     for rv_col in all_rv_col_without_hid:
    #         ids = rv_col.split("_")
    #         otb_dict[rv_col] = lib.compute_otb_adjust_ratio(int(ids[0]), int(ids[1]), snapshot_date, otb_df)
    #     df_otb = lib.create_otb(plot_df, all_rv_col_without_hid, otb_dict)
    # else:
    #     df_otb = None


    # file_top_df = 'top_df_%s_%s.csv'%(name_year, model)
    # top_df = prepare_plot_df(file_top_df)



    # start_time = time.time()
    # hotel_plot_driver = plot_driver_df_ver_1.plot_driver_df(\
    #     years = year_target, df_driver_union = plot_df, df_driver_union_rn = plot_df_rn, \
    #     df_driver_union_gn = plot_df_gn, df_otb = df_otb, \
    #     df_ = df, top_df = top_df, segment_name = segment_name,\
    #     num_of_top = 3, model = model, significance_level = significance_level)

    # for rv_col in all_rv_col_without_hid:
    #     hotel_plot_driver.plot_error(rv_col, df_driver_union = plot_df, \
    #         df_driver_union_rn = plot_df_rn, df_driver_union_gn = plot_df_gn, df_otb = df_otb)
        
    # # hotel_plot_driver.plot_error("235_0_rv", df_driver_union = plot_df, \
    # #     df_driver_union_rn = plot_df_rn, df_driver_union_gn = plot_df_gn, df_otb = df_otb)

    # # hotel_plot_driver.plot_all_table("235_0_rv", df, num_of_top = 3, id_to_name = True)


    # for rv_col in all_rv_col_without_hid:
    #     hotel_plot_driver.plot_all_table(rv_col, df, num_of_top = 3, id_to_name = True)
    #     # hotel_223_plot_driver.plot_all_table("226_18_rv", df, num_of_top = 3, id_to_name = True)
    # print("--- %s seconds ---" % (time.time() - start_time))


    os.system('say "your program has finished"')



