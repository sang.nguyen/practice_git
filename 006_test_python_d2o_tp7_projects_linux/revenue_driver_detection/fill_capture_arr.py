import pandas as pd
from datetime import timedelta, datetime
from capture_arr import *
import project_lib as lib
import argparse
import os
import numpy as np

def fill_driver(file_path, df, hotel_id, num_day=10, type='arr'):
    """
    Fill ARR, capture values or explanation to driver dataframe
    :param file_path: path to driver file
    :param df: all data of one hotel
    :param hotel_id: Hotel id
    :param num_day: num day - number of data points to compute ARR, capture or explanation
    :param type: 'arr' compute ARR, capture. otherwise compute explanation
    :return: dataframe same struter as driver dataframe with filled values ARR, capture or explanation.
    """
    def read_file_driver(file_path):
        df_driver = pd.read_csv(file_path, index_col=False)
        # print df_driver.head()
        df_driver['date'] = pd.to_datetime(df_driver['date'])
        if type == 'arr':
            df_driver['ARR'] = -1
            df_driver['ARR_unit'] = -1
            df_driver['capture_unit'] = -1
            df_driver['capture'] = -1
        else:
            df_driver['coef'] = np.nan
            df_driver['intercept'] = np.nan

        # Save original row order
        df_driver['index_order'] = range(len(df_driver))
        return df_driver

    df_driver = read_file_driver(file_path)
    idx = pd.IndexSlice
    #Find dominant driver
    cols = df_driver['col'].unique()
    df_driver['date'] = pd.to_datetime(df_driver["date"])
    dates_of_df = df_driver['date']
    df_driver = df_driver.set_index(['col', 'date']).sort_index()
    for col in cols:
        dep = col.split('_')[0]
        l_season_tf = lib.read_dep_season_timeframe(dep, hotel_id)
        for i, season_tf in enumerate(l_season_tf):
            dates_in_season = lib.get_season_dates_from_tf(season_tf, dates_of_df)
            for day_of_week in range(7):
                try:
                    weekdays_in_season = lib.get_dates_by_dayWeek(dates_in_season, day_of_week)
                    dominant_driver = df_driver.loc[idx[col, weekdays_in_season],'col_d'].value_counts().index[0]
                    # print col, day_of_week, i, dominant_driver
                    df_driver.loc[idx[col, weekdays_in_season],'col_d'] = dominant_driver
                    lag = int(dominant_driver.split("_")[3])
                    col_d = "_".join(dominant_driver.split("_")[:3])
                    if type == 'arr':
                        result = compute_capture_arr(col, col_d, lag, season_tf, num_day, day_of_week, df)
                        df_driver.loc[idx[col, weekdays_in_season], 'ARR'] = result['ARR']
                        df_driver.loc[idx[col, weekdays_in_season], 'ARR_unit'] = result['ARR_unit']
                        df_driver.loc[idx[col, weekdays_in_season], 'capture_unit'] = result['capture_unit']
                        df_driver.loc[idx[col, weekdays_in_season], 'capture'] = result['capture']
                    else:
                        result = regression(col, col_d, lag, season_tf, num_day, day_of_week, df)
                        # print result
                        df_driver.loc[idx[col, weekdays_in_season], 'coef'] = result['coef']
                        df_driver.loc[idx[col, weekdays_in_season], 'intercept'] = result['intercept']
                except:
                    print(weekdays_in_season)
                    print(season_tf, col, day_of_week)
    df_driver = df_driver.reset_index()
    df_driver = df_driver.set_index('index_order').sort_index()
    return df_driver

if __name__ == "__main__":
    # parser = argparse.ArgumentParser("Generates seasons and writes result to database",
    #                                  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # parser.add_argument("-RV", "--revenue", help="Revenue file path", required=True)




    rn_df = pd.read_csv(os.path.join('data', '223','hotel_223_rn_4y_1Apr_filled.csv'))
    rv_df = pd.read_csv(os.path.join('data', '223','hotel_223_rv_4y_1Apr_filled.csv'))
    gn_df = pd.read_csv(os.path.join('data', '223','hotel_223_guest_4y_1Apr_filled.csv'))

    date_s = rn_df['date']
    del rn_df['date']
    del rv_df['date']
    del gn_df['date']

    rn_df.columns = ["{0}_rn".format(col) for col in rn_df.columns]
    rv_df.columns = ["{0}_rv".format(col) for col in rv_df.columns]
    gn_df.columns = ["{0}_gn".format(col) for col in gn_df.columns]

    df = pd.concat([date_s, rv_df, rn_df, gn_df], axis=1)
    df['date'] = pd.to_datetime(df['date'])

    # Read driver
    file_driver = os.path.join('data', '223', "driver_2012_2014.csv")
    filled_df_driver = fill_driver(file_driver, type='regr')
    filled_df_driver.to_csv('filled_regr_223_0_rn_1.csv', index=False)

