import pandas as pd
from datetime import timedelta
from datetime import date as ddate
import time
import pylab
import matplotlib.pyplot as plt
import textwrap
import matplotlib.cm as cm
import matplotlib as mpl
import os

import department_correlation
import project_lib as lib




def convert_corr(corr):
    '''
    convert a corr value into a catergorical value
    :param corr: corr value
    :return: a constant in some constant numbers.
    '''
    if corr > 0.8:
        return 0.9
    elif corr > 0.6:
        return 0.7
    else:
        return 0.4


def connect_day_of_week(list_day, col = "date"):
    '''
    connect a list dataframe (not necessery for each day of week) into a united dataframe by date order.
    :param list_day: list of dataframe, with same columns
    :param col: (default: "date") the date column name
    :return: a united dataframe is ordered by date
    '''

    new_dataframe = list_day[0]
    for i in xrange(1, len(list_day)):
        new_dataframe = new_dataframe.append(list_day[i])
    new_dataframe['date'] = pd.to_datetime(new_dataframe['date'])  
    new_dataframe = new_dataframe.sort('date', ascending=True)
    new_dataframe = new_dataframe.reset_index(drop= True)
    return new_dataframe

significance_level = 0.05

class plot_driver_df:

    def __init__(self, years, df_driver_union, df_driver_union_rn, df_driver_union_gn, df_otb, df_, top_df, segment_name, num_of_top, model, significance_level = 0.05):
        self.years = years
        self.df_driver_union = df_driver_union
        self.df_driver_union_rn = df_driver_union_rn
        self.df_driver_union_gn = df_driver_union_gn
        self.df_otb = df_otb
        self.segment_name = segment_name
        self.num_of_top = num_of_top
        self.df_ = df_
        self.top_df = top_df
        self.model = model
        self.significance_level = significance_level
        # using in plot
        self.norm = mpl.colors.Normalize(vmin = 0, vmax = 1.5)
        self.cmap = cm.hot
        self.m = cm.ScalarMappable(norm = self.norm, cmap = self.cmap)

        # ticker in xaxis, to plot
        x_tick_init = [4.2, 5.4, 7]
        self.x_tick_list = list(x_tick_init)
        for i in xrange(1, 7):
            for j in x_tick_init:
                self.x_tick_list.append(j + i * 8.5)

        self.hotel_id = str(self.segment_name["hotel_id"].iloc[0])

        self.all_col = set(df_.columns) - {'date', 'day_of_week'}
        self.all_rv_col = list(self.all_col)
        self.temp_list = list(self.all_col)
        self.all_rv_col_without_hid = list(self.all_rv_col)
        for i in self.temp_list:
            if (("rn" in i) or ("gn" in i)):
                self.all_rv_col.remove(i)
                self.all_rv_col_without_hid.remove(i)
            if (self.hotel_id in i):
                try:
                    self.all_rv_col_without_hid.remove(i)
                except:
                    pass
        self.all_dep = set([i.split("_")[0] for i in self.all_col])

        self.period_dict = {}
        for i,col in enumerate(self.all_rv_col):
            ids = col.split("_")
            try:
                # maybe this code make the file slow
                self.period_dict[ids[0]] = pd.read_csv("%s.csv" % ids[0], header=None, prefix = "tp7_")
            except:
                pass
        self.this_df = None
        self.period_dict_ver2 = {}
        for dep in self.all_dep:
            # maybe this code make the file slow
            # self.period_dict[ids[0]] = pd.read_csv("%s.csv" % ids[0], header=None, prefix = "tp7_")
            self.period_dict_ver2[dep] = lib.read_dep_season_timeframe(dep, self.hotel_id)



    def get_top_new(self, dep_seg_type, season_tf):
        '''
        for each specific segment in a specific season, create a dataframe represents the top 3 (corr / 1 - error) rv/rn/gn segments, to plot later
        :param dep_seg_type: a specific segment
        :season_tf: a specific season
        :return: a dataframe represents the top 3 corr or (1 - error) rv/rn/gn segments, to plot later
        '''
        start_month_date = season_tf[0][0].split("-")
        try:
            start_date = ddate(self.years[0], int(start_month_date[0]), int(start_month_date[1]))
            top_df_in_ss = self.top_df.ix[self.top_df.col == dep_seg_type]
            top_df_in_ss = top_df_in_ss.ix[top_df_in_ss.date >= pd.to_datetime(start_date)]
            top_df_in_ss = top_df_in_ss.iloc[0 : 7 * 3]
        except:
            try:
                start_date = ddate(self.years[-1], int(start_month_date[0]), int(start_month_date[1]))
                top_df_in_ss = self.top_df.ix[self.top_df.col == dep_seg_type]
                top_df_in_ss = top_df_in_ss.ix[top_df_in_ss.date >= pd.to_datetime(start_date)]
                top_df_in_ss = top_df_in_ss.iloc[0 : 7 * 3]
            except:
                try:
                    start_date = ddate(self.years[-2], int(start_month_date[0]), int(start_month_date[1]))
                    top_df_in_ss = self.top_df.ix[self.top_df.col == dep_seg_type]
                    top_df_in_ss = top_df_in_ss.ix[top_df_in_ss.date >= pd.to_datetime(start_date)]
                    top_df_in_ss = top_df_in_ss.iloc[0 : 7 * 3]
                except:
                    top_df_in_ss = self.top_df.ix[self.top_df.col == dep_seg_type]
                    top_df_in_ss = top_df_in_ss.iloc[0 : 7 * 3]

        top_df_in_ss = top_df_in_ss.sort(['day_of_week', "cov_value"], ascending=[True, False])
        # print("done capture")
        return top_df_in_ss



    def create_capture_df(self, dep_seg_type, season_tf):
        '''
        get the ARR and capture value of a specific segment in a specific season
        :param dep_seg_type: a specific segment
        :season_tf: a specific season
        :return: a dataframe contains the ARR and capture for all days of week
        '''
        start_month_date = season_tf[0][0].split("-")
        try:
            start_date = ddate(self.years[0], int(start_month_date[0]), int(start_month_date[1]))
            capture_df = self.df_driver_union.ix[self.df_driver_union.col == dep_seg_type]
            capture_df = capture_df.ix[capture_df.date >= pd.to_datetime(start_date)]
            capture_df = capture_df.iloc[0 : 7]
        except:
            try:
                start_date = ddate(self.years[-1], int(start_month_date[0]), int(start_month_date[1]))
                capture_df = self.df_driver_union.ix[self.df_driver_union.col == dep_seg_type]
                capture_df = capture_df.ix[capture_df.date >= pd.to_datetime(start_date)]
                capture_df = capture_df.iloc[0 : 7]
            except:
                try:
                    start_date = ddate(self.years[-2], int(start_month_date[0]), int(start_month_date[1]))
                    capture_df = self.df_driver_union.ix[self.df_driver_union.col == dep_seg_type]
                    capture_df = capture_df.ix[capture_df.date >= pd.to_datetime(start_date)]
                    capture_df = capture_df.iloc[0 : 7]
                except:
                    capture_df = self.df_driver_union.ix[self.df_driver_union.col == dep_seg_type]
                    capture_df = capture_df.iloc[0 : 7]

        capture_df = capture_df.sort('day_of_week', ascending=True)
        # print("done capture")
        return capture_df



    def Id_to_name(self, df):
        '''
        replace segments id with their names
        :param df: the DataFrame needs to replace names.
        :return: a dataframe that replaces segments id with their names
        '''
        for id_number in xrange(0, len(self.segment_name)):
            for lag in [7, 14, 21, 30, 365]:
                ids = self.segment_name["id"][id_number]
                ids_rn = ids + "_rn_%s"%(str(lag))
                ids_rv = ids + "_rv_%s"%(str(lag))
                ids_guest = ids + "_gn_%s"%(str(lag))

                df["adj_corr_col"] = df["adj_corr_col"].replace(ids_rn, self.segment_name["name"][id_number] + "_RN lag %s day"%(str(lag)))
                df["adj_corr_col"] = df["adj_corr_col"].replace(ids_rv, self.segment_name["name"][id_number] + "_RV lag %s day"%(str(lag)))
                df["adj_corr_col"] = df["adj_corr_col"].replace(ids_guest, self.segment_name["name"][id_number] + "_GN lag %s day"%(str(lag)))

            ids = self.segment_name["id"][id_number]
            ids_rn_0 = ids + "_rn_0"
            ids_rv_0 = ids + "_rv_0"
            ids_guest_0 = ids + "_gn_0"

            df["adj_corr_col"] = df["adj_corr_col"].replace(ids_rn_0, self.segment_name["name"][id_number] + "_RN same day")
            df["adj_corr_col"] = df["adj_corr_col"].replace(ids_rv_0, self.segment_name["name"][id_number] + "_RV same day")
            df["adj_corr_col"] = df["adj_corr_col"].replace(ids_guest_0, self.segment_name["name"][id_number] + "_GN same day")
            
            ids_rn_1 = ids + "_rn_1"
            ids_rv_1 = ids + "_rv_1"
            ids_guest_1 = ids + "_gn_1"

            df["adj_corr_col"] = df["adj_corr_col"].replace(ids_rn_1, self.segment_name["name"][id_number] + "_RN previous day")
            df["adj_corr_col"] = df["adj_corr_col"].replace(ids_rv_1, self.segment_name["name"][id_number] + "_RV previous day")
            df["adj_corr_col"] = df["adj_corr_col"].replace(ids_guest_1, self.segment_name["name"][id_number] + "_GN previous day")

        return df

    

    def create_predict_df_new(self, dep_seg_type, day_of_week, season_tf, df_driver_union):
        '''
        prepare predicted data of a segment in a day of week of a season for plotting
        :param dep_seg_type: a specific segment
        :param day_of_week: a day of week
        :param season_tf: a season timeframe
        :param df_driver_union: a dataframe that has the predicted data for some segments in 
        :return: a DataFrame that has the predicted data for a specific segment
        '''
        col_names = ["date", "filled_values", "values", "day_of_week"]

        temp_df = df_driver_union.ix[df_driver_union.col == dep_seg_type][col_names]
        date_list = lib.generate_season_date(season_tf, self.years)
        temp_df = temp_df.ix[temp_df.date.isin(date_list)]
        temp_df = temp_df.ix[temp_df.day_of_week == day_of_week]
        return temp_df



    def plot_for_1_seg(self, result_week, dep_seg_type, season_tf, num_of_top, rate_and_arr, predict_data, predict_data_rn, predict_data_gn, predict_data_regression = None, day_of_week = None, to_not_plot = True):
        '''
        plot for a specific segment
        :param result_week: a top3 dataframe that is created from get_top_new function
        :param dep_seg_type: a specific segment
        :param season_tf: a season timeframe
        :param num_of_top: length of the result_week
        :param rate_and_arr: a DataFrame that is created from create_capture_df function contains ARR and capture values.
        :param predict_data: a DataFrame that is created from create_predict_df_new function contains predicted values.
        :param predict_data_rn: a DataFrame that is created from create_predict_df_new function contains predicted values with main variable is hid_0_rn (to compare).
        :param predict_data_gn: a DataFrame that is created from create_predict_df_new function contains predicted values with main variable is hid_0_gn (to compare).
        :param predict_data_regression: (will be removed) a DataFrame that is created from create_predict_df_new function contains predicted values with main variable from regression model (to compare).
        :param day_of_week: (default None) a day of week, or the whole week if day_of_week = None
        :param to_not_plot: (default True) a option to choose whether plot data or not 
        :return: a .png image to /plot/hid/ that plots the actual data, predicted data, error, and a table represents the top 3 corr or (1 - error) rv/rn/gn segments 
        '''
        if to_not_plot:
            plt.ioff()
        
        day_list = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

        year = self.years[0]
        fig, axarr = plt.subplots(2, sharex= False)
        actual_line, = axarr[0].plot(predict_data["date"], predict_data["values"], color = "blue", label = 'Actual', alpha =0.5)
        arr_line, = axarr[0].plot(predict_data["date"], predict_data["filled_values"], color = 'red', label = 'Revenue Driver', alpha=0.8)
        arr_rn_line, = axarr[0].plot(predict_data_rn["date"], predict_data_rn["filled_values"], color = 'green', label = 'RN predict', alpha=0.8)
        arr_gn_line, = axarr[0].plot(predict_data_gn["date"], predict_data_gn["filled_values"], color = 'magenta', label = 'GN predict', alpha=0.8)

        predict_data["error"] = (predict_data["filled_values"] - predict_data["values"]).abs()
        predict_data_rn["error"] = (predict_data_rn["filled_values"] - predict_data_rn["values"]).abs()
        predict_data_gn["error"] = (predict_data_gn["filled_values"] - predict_data_gn["values"]).abs()

        if type(predict_data_regression) != type(None):
            arr_regression_line, = axarr[0].plot(predict_data_regression["date"], predict_data_regression["filled_values"], color = 'black', label = 'Regression', alpha=0.8)
            predict_data_regression["error"] = (predict_data_regression["filled_values"] - predict_data_regression["values"]).abs()
        else:
            pass

        xmin_1, xmax_1 = axarr[0].get_xlim()
        ymin_1, ymax_1 = axarr[0].get_ylim()
        # error = self.mean_square_error()
        # axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
        #     0.9 * ymin_1 + 0.1 * ymax_1,
        #     "Predicted error: %.2f " % (100 * predict_data["error"].mean()) + "%", 
        #     color = "green", va='center', ha='center')
        if predict_data["values"].mean() == 0:
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.9 * ymin_1 + 0.1 * ymax_1,
                "Predicted error: cant calculate (values mean = 0)", 
                color = "green", va='center', ha='center')
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.85 * ymin_1 + 0.15 * ymax_1,
                "Predicted RN error: cant calculate (values mean = 0)", 
                color = "green", va='center', ha='center')
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.8 * ymin_1 + 0.2 * ymax_1,
                "Predicted GN error: cant calculate (values mean = 0)", 
                color = "green", va='center', ha='center')
            if type(predict_data_regression) != type(None):
                axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                    0.75 * ymin_1 + 0.25 * ymax_1,
                    "Regression error: cant calculate (values mean = 0)", 
                    color = "green", va='center', ha='center')
            else:
                pass


        else:
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.9 * ymin_1 + 0.1 * ymax_1,
                "Predicted error: %.2f " % (100 * predict_data["error"].mean() / predict_data["values"].mean()) + "%", 
                color = "green", va='center', ha='center')
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.85 * ymin_1 + 0.15 * ymax_1,
                "Predicted RN error: %.2f " % (100 * predict_data_rn["error"].mean() / predict_data_rn["values"].mean()) + "%", 
                color = "green", va='center', ha='center')
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.8 * ymin_1 + 0.2 * ymax_1,
                "Predicted GN error: %.2f " % (100 * predict_data_gn["error"].mean() / predict_data_gn["values"].mean()) + "%", 
                color = "green", va='center', ha='center')
            if type(predict_data_regression) != type(None):
                axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                    0.75 * ymin_1 + 0.25 * ymax_1,
                    "Regression error: %.2f " % (100 * predict_data_regression["error"].mean() / predict_data_regression["values"].mean()) + "%", 
                    color = "green", va='center', ha='center')
            else:
                pass

        if day_of_week != None:
            axarr[0].text(0.07 * xmin_1 + 0.93 * xmax_1,
                0.95 * ymin_1 + 0.05 * ymax_1,
                "%s" % day_list[day_of_week], 
                color = "green", va='center', ha='center')

        if type(predict_data_regression) != type(None):
            plt.legend(handles=[actual_line, arr_line, arr_rn_line, arr_gn_line, arr_regression_line], loc = (0.87, 2))
        else:
            plt.legend(handles=[actual_line, arr_line, arr_rn_line, arr_gn_line], loc = (0.87, 2))

        axarr[1].set_xlim(0, 8.5 * 7)
        axarr[1].set_ylim(0, num_of_top + 1)

        for i in xrange(0, 7):
            # just add text to a position in the plot
            axarr[1].text(2.1 + i * 8.5, 0.5 + num_of_top, day_list[i], va='center', ha='center')
            axarr[1].text(4.8 + i * 8.5, 0.5 + num_of_top, "Corr", va='center', ha='center')
            axarr[1].text(6.2 + i * 8.5, 0.5 + num_of_top, "%", va='center', ha='center')
            axarr[1].text(7.8 + i * 8.5, 0.5 + num_of_top, "ARR", va='center', ha='center')

            for j in xrange(0, num_of_top):
                # we should devide the segment name if it is too long
                text = textwrap.wrap(result_week["adj_corr_col"].iloc[num_of_top * i + j], width=14)
                # dumb = textwrap.wrap("aaaaa a a aaaaaaaaaaaa aa a a a a a aaaaaaaa a a a a a a a a a a a aaaaaaaaaaa", width=25)
                if len(text) == 1:
                    axarr[1].text(2.1 + i * 8.5, num_of_top - 0.3 - j, text[0], va='center', ha='center', fontsize=10)
                elif len(text) == 2:
                    axarr[1].text(2.1 + i * 8.5, num_of_top - 0.25 - j, text[0], va='center', ha='center', fontsize=10)
                    axarr[1].text(2.1 + i * 8.5, num_of_top - 0.5 - j, text[1], va='center', ha='center', fontsize=10)
                else:
                    axarr[1].text(2.1 + i * 8.5, num_of_top - 0.25 - j, text[0], va='center', ha='center', fontsize=10)
                    axarr[1].text(2.1 + i * 8.5, num_of_top - 0.5 - j, text[1], va='center', ha='center', fontsize=10)
                    axarr[1].text(2.1 + i * 8.5, num_of_top - 0.75 - j, text[2], va='center', ha='center', fontsize=10)
                axarr[1].text(4.8 + i * 8.5, num_of_top - 0.5 - j, "%.3f" % result_week["cov_value"].iloc[num_of_top * i + j], va='center', ha='center', fontsize=10)
                # pylab.bar([4.2 + 8.5 * i], color = self.m.to_rgba(1.5 - result_week["cov_value"].iloc[num_of_top * i + j]), **dict(height = [1], bottom = num_of_top - 1 - j, width = 1.2))
                pylab.bar([4.2 + 8.5 * i], color = self.m.to_rgba(1.5 - convert_corr(result_week["cov_value"].iloc[num_of_top * i + j])), **dict(height = [1], bottom = num_of_top - 1 - j, width = 1.2))

            if type(rate_and_arr) != type(None):
                axarr[1].text(6.2 + i * 8.5, num_of_top - 0.5, "%.1f" % (rate_and_arr["capture"].iloc[i] * 100), va='center', ha='center', fontsize=10)
                axarr[1].text(7.8 + i * 8.5, num_of_top - 0.5, "%.2f" % rate_and_arr["ARR"].iloc[i], va='center', ha='center', fontsize=10)


    #    ax.set_xticks([0, 4, 5, 6, 7, 11, 12, 12, 14, 21, 20, 19, 18, 28, 27, 26, 25, 35, 34, 33, 32, 42, 41, 40, 39, 49, 48, 47, 46])
        axarr[1].set_xticks([0,8.5,17,25.5,34,42.5,51,59.5])                                                       
        axarr[1].set_xticks(self.x_tick_list, minor=True)                                           
        axarr[1].set_yticks(range(0, num_of_top + 2))

        # ax.grid(True, linestyle='-')
        axarr[1].grid(which='minor', alpha=0.9)                                                
        axarr[1].grid(which='major', linestyle='-')                                                

        axarr[1].set_xticklabels([])
        axarr[1].set_yticklabels([])
        
        ids = dep_seg_type.split("_")
        try: 
            replace_name = self.segment_name.ix[self.segment_name.id == (ids[0] + "_" + ids[1])]["name"].iloc[0]
            plt.title(replace_name)
        except:
            plt.title(dep_seg_type)

        fig.set_size_inches(25, num_of_top + 10)

        # plt.show()
        if day_of_week == None:
            plt.savefig(os.path.join('plot', self.hotel_id, "%s_%s_%s.png"%(dep_seg_type, season_tf, str(year))))
        else:
            plt.savefig(os.path.join('plot', self.hotel_id, "%s_%s_%s_%s.png"%(dep_seg_type, season_tf, day_list[day_of_week], str(year))))
        # if to_not_plot:
        plt.close(fig)



    def plot_all_table(self, dep_seg_type, df_, num_of_top, id_to_name = True):
        '''
        plot all image of a segment over all seasons
        :param dep_seg_type: a specific segment
        :param df_: (will be removed) the DataFrame that have all rv/rn/gn data
        :param num_of_top: number of row of the table represents top corr or (1 - error) rv/rn/gn segments
        :param id_to_name: (default True) a option to choose whether replace segment id with its name or not
        :return: all the .png image of a specific segment over all seasons, include both for days of week and the whole week
        '''
        ids = dep_seg_type.split("_")
        try:
            season_all_tf = self.period_dict_ver2[ids[0]]
        except:
            season_all_tf = [("01-01", "12-31")]
            print("there are some errors in line 349")
        for index in xrange(0, len(season_all_tf)):
            if self.model == "regression":
                rate_and_arr = None
            else:
                rate_and_arr = self.create_capture_df(dep_seg_type, season_all_tf[index])

            try:
                top_converted = self.get_top_new(dep_seg_type, season_all_tf[index])
            except:
                print(ids[0], ids[1], season_all_tf[index], rate_and_arr, num_of_top)
            if id_to_name:
                top_converted = self.Id_to_name(top_converted)
            fitted_data = []
            fitted_data_rn = []
            fitted_data_gn = []
            fitted_data_regression = []

            for day_of_week in xrange(0, 7):
                predict_for_dow = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], self.df_driver_union)
                predict_for_dow_rn = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], self.df_driver_union_rn)
                predict_for_dow_gn = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], self.df_driver_union_gn)
                fitted_data.append(predict_for_dow)
                fitted_data_rn.append(predict_for_dow_rn)
                fitted_data_gn.append(predict_for_dow_gn)
                if type(self.df_otb) != type(None):
                    predict_for_dow_regression = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], self.df_otb)
                    fitted_data_regression.append(predict_for_dow_regression)
                else:
                    predict_for_dow_regression = None

                self.plot_for_1_seg(top_converted, dep_seg_type, season_all_tf[index],\
                                num_of_top, rate_and_arr, predict_for_dow, predict_for_dow_rn, predict_for_dow_gn, predict_for_dow_regression, day_of_week)

                # try:
                #     print("line 368")
                #     self.plot_for_1_seg(top_converted, dep_seg_type, season_all_tf[index],\
                #                     num_of_top, rate_and_arr, predict_for_dow, predict_for_dow_rn, predict_for_dow_gn, day_of_week)
                # except:
                #     print("there are some errors in line 371")
                #     pass

            predicted_data = connect_day_of_week(fitted_data, col = "date")
            predicted_data_rn = connect_day_of_week(fitted_data_rn, col = "date")
            predicted_data_gn = connect_day_of_week(fitted_data_gn, col = "date")
            if type(self.df_otb) != type(None):
                predict_data_regression = connect_day_of_week(fitted_data_regression, col = "date")
            else:
                predict_data_regression = None

            try:
                self.plot_for_1_seg(top_converted, dep_seg_type, season_all_tf[index],\
                                    num_of_top, rate_and_arr, predicted_data, predicted_data_rn, predicted_data_gn, predict_data_regression)
            except:
                pass



    def compute_error(self, predict_df):
        '''
        to calculate error for a prediction DataFrame
        :param predict_df: a prediction DataFrame that is created from create_predict_df_new function
        :return: error (in percentage) between predicted values and actual values, or 10000 if the actual values mean equals 0
        '''
        predict_df["error"] = (predict_df["filled_values"] - predict_df["values"]).abs()
        try:
            error = 100 * predict_df["error"].mean() / predict_df["values"].mean()
        except:
            error = 10000
        return error

    def create_error_df(self, dep_seg_type, df_driver_union, df_driver_union_rn, df_driver_union_gn, df_otb = None):
        '''
        to create a DataFrame that stores the error data for a specific segment
        :param dep_seg_type: a specific segment
        :param df_driver_union: a DataFrame have predicted data.
        :param df_driver_union_rn: a DataFrame have predicted data with main variable is hid_0_rn (to compare).
        :param df_driver_union_gn: a DataFrame have predicted data with main variable is hid_0_gn (to compare).
        :return: a DataFrame that stores the error data for a specific segment over all seasons, include both for days of week and the whole week
        '''
        ids = dep_seg_type.split("_")
        if type(df_otb) != type(None):
            column_names = ["season", "driver", "rn", "gn", "otb"]
        else:
            column_names = ["season", "driver", "rn", "gn"]
        for day_of_week in xrange(0, 7):
            column_names.append("driver_%s"%day_of_week)
            column_names.append("rn_%s"%day_of_week)
            column_names.append("gn_%s"%day_of_week)
            if type(df_otb) != type(None):
                column_names.append("otb_%s"%day_of_week)

        error_df = pd.DataFrame(columns = column_names)

        try:
            season_all_tf = self.period_dict_ver2[ids[0]]
        except:
            season_all_tf = [("01-01", "12-31")]
            print("there are some errors in line 500")
        for index in xrange(0, len(season_all_tf)):
            error_df.loc[index] = [index] * len(column_names)

            fitted_data = []
            fitted_data_rn = []
            fitted_data_gn = []
            fitted_data_otb = []

            for day_of_week in xrange(0, 7):
                predict_for_dow = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], df_driver_union)
                predict_for_dow_rn = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], df_driver_union_rn)
                predict_for_dow_gn = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], df_driver_union_gn)
                error_df.loc[index, "driver_%s"%day_of_week] = self.compute_error(predict_for_dow)
                error_df.loc[index, "rn_%s"%day_of_week] = self.compute_error(predict_for_dow_rn)
                error_df.loc[index, "gn_%s"%day_of_week] = self.compute_error(predict_for_dow_gn)
                fitted_data.append(predict_for_dow)
                fitted_data_rn.append(predict_for_dow_rn)
                fitted_data_gn.append(predict_for_dow_gn)
                
                if type(df_otb) != type(None):
                    predict_for_dow_otb = self.create_predict_df_new(dep_seg_type, day_of_week, season_all_tf[index], df_otb)
                    error_df.loc[index, "otb_%s"%day_of_week] = self.compute_error(predict_for_dow_otb)
                    fitted_data_otb.append(predict_for_dow_otb)


            predicted_data = connect_day_of_week(fitted_data, col = "date")
            predicted_data_rn = connect_day_of_week(fitted_data_rn, col = "date")
            predicted_data_gn = connect_day_of_week(fitted_data_gn, col = "date")
            error_df.loc[index, "driver"] = self.compute_error(predicted_data)
            error_df.loc[index, "rn"] = self.compute_error(predicted_data_rn)
            error_df.loc[index, "gn"] = self.compute_error(predicted_data_gn)
            if type(df_otb) != type(None):
                predicted_data_otb = connect_day_of_week(fitted_data_otb, col = "date")
                error_df.loc[index, "otb"] = self.compute_error(predicted_data_otb)

        return error_df

    def plot_error(self, dep_seg_type, df_driver_union, df_driver_union_rn, df_driver_union_gn, df_otb = None, to_not_plot = True):
        '''
        plot a image that represents the error of 3 models of a segment over all seasons, include both for days of week and the whole week
        :param dep_seg_type: a specific segment
        :param df_driver_union: a DataFrame have predicted data.
        :param df_driver_union_rn: a DataFrame have predicted data with main variable is hid_0_rn (to compare).
        :param df_driver_union_gn: a DataFrame have predicted data with main variable is hid_0_gn (to compare).
        :param to_not_plot: (default True) a option to choose whether plot data or not 
        :return: a .png image to /plot/hid/ that plots the error of 3 models of a segment over all seasons, include both for days of week and the whole week
        '''
        if to_not_plot:
            plt.ioff()

        error_df = self.create_error_df(dep_seg_type, df_driver_union, df_driver_union_rn, df_driver_union_gn, df_otb)
        # error_df["season"] = error_df["season"] + 1
        day_list = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

        fig, axarr = plt.subplots(3, 3, sharex= False, sharey = False)
        plot_dict = {}

        key_name = "line"
        key_name_rn = "line_rn"
        key_name_gn = "line_gn"
        key_name_otb = "line_otb"
        plot_dict[key_name], = axarr[0,0].plot(error_df["season"], \
            error_df["driver"], color = "red", label = 'error', alpha =0.5)
        plot_dict[key_name_rn], = axarr[0,0].plot(error_df["season"], \
            error_df["rn"], color = "green", label = 'error rn', alpha =0.5)
        plot_dict[key_name_gn], = axarr[0,0].plot(error_df["season"], \
            error_df["gn"], color = "blue", label = 'error gn', alpha =0.5)
        if type(df_otb) != type(None):
            plot_dict[key_name_otb], = axarr[0,0].plot(error_df["season"], \
                error_df["otb"], color = "black", label = 'error otb', alpha =0.5)
        axarr[0, 0].set_title('Week')

        for day_of_week in xrange(0, 7):
            loc = [(day_of_week + 1) / 3, (day_of_week + 1) % 3]
            key_name = "line_%s"%day_of_week
            key_name_rn = "line_rn_%s"%day_of_week
            key_name_gn = "line_gn_%s"%day_of_week
            key_name_otb = "line_otb_%s"%day_of_week
            plot_dict[key_name], = axarr[loc[0], loc[1]].plot(error_df["season"], \
                error_df["driver_%s"%day_of_week], color = "red", label = 'error day: %s'%day_of_week, alpha =0.5)
            plot_dict[key_name_rn], = axarr[loc[0], loc[1]].plot(error_df["season"], \
                error_df["rn_%s"%day_of_week], color = "green", label = 'error rn day: %s'%day_of_week, alpha =0.5)
            plot_dict[key_name_gn], = axarr[loc[0], loc[1]].plot(error_df["season"], \
                error_df["gn_%s"%day_of_week], color = "blue", label = 'error gn day: %s'%day_of_week, alpha =0.5)
            if type(df_otb) != type(None):
                plot_dict[key_name_otb], = axarr[loc[0], loc[1]].plot(error_df["season"], \
                    error_df["otb_%s"%day_of_week], color = "black", label = 'error otb day: %s'%day_of_week, alpha =0.5)
            axarr[loc[0], loc[1]].set_title(day_list[day_of_week])
        
        if type(df_otb) != type(None):
            plt.legend(handles=[plot_dict["line"], plot_dict["line_rn"], plot_dict["line_gn"], plot_dict["line_otb"]])
        else:
            plt.legend(handles=[plot_dict["line"], plot_dict["line_rn"], plot_dict["line_gn"]])
        fig.set_size_inches(20, 16)

        plt.savefig(os.path.join('plot', self.hotel_id, "%s_error.png"%(dep_seg_type)))
        plt.close(fig)
        print("done for %s"%dep_seg_type)


if __name__ == "__main__":


    rn_df = pd.read_csv('hotel_223_corr_rn_without_zero.csv')
    rv_df = pd.read_csv('hotel_223_corr_rv_without_zero.csv')
    gn_df = pd.read_csv('hotel_223_guest_4y_1Apr.csv')
    hotel_223_segment_name = pd.read_csv("hotel_233_segment_name.csv")
    period = pd.read_csv("223_FH_CPH_Breakfast_df_plot_period.csv")

    date_s = rn_df['date']
    del rn_df['date']
    del rv_df['date']
    del gn_df['date']

    rn_df.columns = ["{0}_rn".format(col) for col in rn_df.columns]
    rv_df.columns = ["{0}_rv".format(col) for col in rv_df.columns]
    gn_df.columns = ["{0}_gn".format(col) for col in gn_df.columns]

    df = pd.concat([date_s, rv_df, rn_df, gn_df], axis=1)
    df['date'] = pd.to_datetime(df['date'])
    df['day_of_week'] = df['date'].dt.dayofweek

    all_col = set(df.columns) - {'date', 'day_of_week'}

    years = [2014]
    significance_level = 0.05
    season_tf = ['03-10', '05-04']
    

    df_driver_union = pd.read_csv("predicted_223_ver2.2.csv")
    df_driver_union["date"] = pd.to_datetime(df_driver_union['date'])
    df_driver_union["day_of_week"] = df_driver_union['date'].dt.dayofweek

    df_driver_union_rn = pd.read_csv("predicted_223_rn_ver2.2.csv")
    df_driver_union_rn["date"] = pd.to_datetime(df_driver_union_rn['date'])
    df_driver_union_rn["day_of_week"] = df_driver_union_rn['date'].dt.dayofweek

    df_driver_union_gn = pd.read_csv("predicted_223_gn_ver2.2.csv")
    df_driver_union_gn["date"] = pd.to_datetime(df_driver_union_gn['date'])
    df_driver_union_gn["day_of_week"] = df_driver_union_gn['date'].dt.dayofweek

    top_df = pd.read_csv("top_df_2013_2014_01-01_12-31_test.csv")
    top_df["date"] = pd.to_datetime(top_df['date'])
    top_df["day_of_week"] = top_df['date'].dt.dayofweek

    start_time = time.time()
    hotel_223_plot_driver = plot_driver_df_ver_1.plot_driver_df(years = year_target, df_driver_union = df_driver_union, df_driver_union_rn = df_driver_union_rn, df_driver_union_gn = df_driver_union_gn, df_ = df,\
                                                                top_df = top_df, all_col = all_col, segment_name = hotel_223_segment_name,\
                                                                num_of_top = 3, significance_level = significance_level)
    # hotel_223_plot_driver.plot_for_1_seg_1_season("224_0_rv", season_tf)
    # hotel_223_plot_driver.plot_for_1_seg_1_day("224_0_rv", season_tf, 2)
    # hotel_223_plot_driver.run_all()
    hotel_223_plot_driver.plot_all_table("224_0_rv", df, num_of_top = 3, id_to_name = True)
    hotel_223_plot_driver.plot_all_table("226_18_rv", df, num_of_top = 3, id_to_name = True)
    print("--- %s seconds ---" % (time.time() - start_time))






