import pandas as pd
from datetime import timedelta, datetime
import time
import os
import pickle

import plot_driver_df_ver_1
import create_driver_df
import fill_capture_arr
import capture_arr
import project_lib as lib
import department_correlation


def create_filled_list(df, hotel_id):
    '''
    create a filled list for a hotel
    :param df: a dataframe that has rn, gn, rv data
    :param hotel_id: hotel_id in string type
    :return: a list of filled data column names
    '''
    filled_list = []
    for i in df.columns:
        if hotel_id in i:
            filled_list.append(i)
    return filled_list


def add_total_to_df(df):
    '''
    add total columns for dataframe
    :param df: the dataframe need to add total columns
    :return: a data frame that have total columns for all departments
    '''
    new_df = df[df.columns]
    dep_list = set(i.split("_")[0] for i in new_df.columns) - {"date"}
    for dep in dep_list:
        other_seg = []
        if (dep + "_0") not in new_df.columns:
            other_seg = [i for i in new_df.columns if dep in i]
            new_df[dep + "_0"] = new_df[other_seg].sum(axis = 1)
    return new_df


def add_total_segname(segment_name):
    '''
    add a row for total segment of all departments for a dataframe
    :param segment_name: a dataframe that has the segment names data
    :return: a data frame that have total segment rows of all departments for a dataframe
    '''
    new_df = segment_name[segment_name.columns]
    dep_list = set(i.split("_")[0] for i in new_df["id"])
    for dep in dep_list:
        if (dep + "_0") not in list(new_df["id"]):
            print(dep)
            print((dep + "_0") in list(new_df["id"]))
            count = len(new_df)
            other_seg = [i for i in new_df["id"] if dep in i]
            copy_id =  new_df[new_df.id == other_seg[0]].index[0]
            new_df.loc[count] = new_df.loc[copy_id]
            new_df.loc[count, ["id", "name"]] = [dep + "_0", new_df.loc[copy_id, "name"].split("_")[0] + "_Total"]
    new_df = new_df.reset_index()
    return new_df


def shift_data(df, segment_name, list_shifted_names):
    '''
    we shift some columns of a dataframe to 1 day later
    :param df: a pandas dataframe
    :param segment_name: segment_name of a hotel
    :param list_names: a list of segment names needs to shift
    :return: dataframe that has shifted columns where needed
    '''
    new_df = df[df.columns]
    for name in list_shifted_names:
        list_col_names = [i for i in segment_name["name"] if name in i]        
        list_id = []
        for seg_name in list(segment_name["name"]):
            if name in seg_name:
                id_ = segment_name.ix[segment_name.name == seg_name]["id"].iloc[0]
                new_df[id_] = new_df[id_].shift(1)

    return new_df

def dominant_and_fill(file_df_driver, hotel_id, df_driver, df_insample):
    # # # WARNING: 
    # # # this code is no longer used, we use fill_capture_arr.fill_capture_arr instead
    df_driver['index_order'] = range(len(df_driver))

    df_driver['ARR'] = -1
    df_driver['ARR_unit'] = -1
    df_driver['capture_unit'] = -1
    df_driver['capture'] = -1

    idx = pd.IndexSlice
    #Find dominant driver
    cols = df_driver['col'].unique()
    dates_of_df = df_driver['date']
    df_driver = df_driver.set_index(['col', 'date']).sort_index()
    for col in cols:
        dep = col.split('_')[0]
        print col
        l_season_tf = lib.read_dep_season_timeframe(dep, hotel_id)
        for i, season_tf in enumerate(l_season_tf):
            dates_in_season = lib.get_season_dates_from_tf(season_tf, dates_of_df)
            for day_of_week in range(7):
                weekdays_in_season = lib.get_dates_by_dayWeek(dates_in_season, day_of_week)
                dominant_driver = df_driver.loc[idx[col, weekdays_in_season],'col_d'].value_counts().index[0]
                df_driver.loc[idx[col, weekdays_in_season],'col_d'] = dominant_driver
                lag = int(dominant_driver.split("_")[3])
                col_d = "_".join(dominant_driver.split("_")[:3])
                result = capture_arr.compute_capture_arr(col, col_d, lag, season_tf, num_day, day_of_week, df_insample)
                df_driver.loc[idx[col, weekdays_in_season], 'ARR'] = result['ARR']
                df_driver.loc[idx[col, weekdays_in_season], 'ARR_unit'] = result['ARR_unit']
                df_driver.loc[idx[col, weekdays_in_season], 'capture_unit'] = result['capture_unit']
                df_driver.loc[idx[col, weekdays_in_season], 'capture'] = result['capture']

    df_driver = df_driver.reset_index()
    df_driver = df_driver.set_index('index_order').sort_index()
    df_driver.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, file_df_driver)), index=False)


def convert_date_df(file_df_driver, period_dict, temp_year):
    # # # WARNING: 
    # # # this code is no longer used, because we do not need to 
    # # # change date of driver df anymore
    df_driver = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_df_driver), index_col=False)
    df_driver['date'] = pd.to_datetime(df_driver['date'])
    ### add more
    df_driver["season_tf"] = 1
    df_driver["day_of_week"] = df_driver['date'].dt.dayofweek

    year_target = [int(temp_year)]

    df_target = df_driver[df_driver.columns]
    df_target['date'] = df_driver['date'] + pd.DateOffset(years= (year_target[0] - year_input[0]))
    df_target["day_of_week"] = df_target['date'].dt.dayofweek

    print("ok, will predict for year %s" %temp_year)
    for id in df_driver.index:
        col = df_driver.iloc[id]['col']
        ids = col.split("_")
        if id % 100 == 0:
            print(id)        
        try:
            day_cv = df_driver.iloc[id]['date'] + pd.DateOffset(years= (2016 - df_driver.iloc[id]['date'].year))
            season_tf_driver, day_of_week_driver = lib.find_out_season(period_dict[ids[0]], day_cv)
            season_tf_target, day_of_week_target = lib.find_out_season(period_dict[ids[0]], day_cv)
        except:
            season_tf_driver, day_of_week_driver = [("01-01", "12-31")], df_driver.iloc[id]['date'].weekday()
            season_tf_target, day_of_week_target = [("01-01", "12-31")], df_target.iloc[id]['date'].weekday()
        
        df_driver.loc[id, "season_tf"] = str(season_tf_driver)
        df_target.loc[id, "season_tf"] = str(season_tf_target)

    for id in df_target.index:
        try:
            driver_id = df_driver[(df_driver.col == df_target.iloc[id]["col"]) & \
                                (df_driver.season_tf == df_target.iloc[id]["season_tf"]) & \
                                (df_driver.day_of_week == df_target.iloc[id]["day_of_week"])].index[0]
            # print(driver_id)
            df_target.loc[id] = [df_target.loc[id][df_target.columns[0]]] + \
                                list(df_target.loc[id][["date"]]) + \
                                [df_target.loc[id][df_target.columns[2]]] + \
                                list(df_driver.loc[driver_id][df_driver.columns[3:9]]) + \
                                list(df_target.loc[id][["season_tf", "day_of_week"]])
            if id % 100 == 0:
                print(id)
        except:
            pass
    df_target.to_csv(os.path.join('data', '%s'%hotel_id,'year_{}'.format(file_df_driver)))


def predict_df(file_df_driver_union, df, model = "error"):
    '''
    In case of out_sample predict type, we create a dataframe that is filled by
    our predict method and export it in .csv file as output
    :param file_df_driver_union: name of a .csv file that represents the dataframe
    that has dominant driver and filled arr and capture.
    :param df: a dataframe that has filled data
    :param model: (default "error") type of model, choose either "error", "corr" 
    or "regression" ("corr" and "error" give the same result)
    '''
    
    df_driver_union = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_df_driver_union), index_col=False)

    df_driver_union['date'] = pd.to_datetime(df_driver_union['date'])
    df_driver_union['filled_values'] = -1
    df_driver_union['values'] = -1
    df_driver_union['col_d_values'] = -1
    df_new_data = df[df.columns]
    df_new_data.index = df_new_data["date"]
    for id in df_driver_union.index:
        if id % 100 == 0:
            print(id)
        col = df_driver_union.iloc[id]['col']
        col_d = df_driver_union.iloc[id]['col_d']
        lag = int(col_d.split("_")[3])
        col_d = "_".join(col_d.split("_")[:3])
        type_col = col.split("_")[-1]
        date = df_driver_union.iloc[id]['date']

        col_d_values = df_new_data[df_new_data['date'] == (date-timedelta(days=lag))][col_d]
        # print(date, lag, col_d, col_d_values)
        col_values = df[df['date'] == date][col]
        if ((model == "error") or (model == "corr")):
            if type_col == 'rv':
                ARR = df_driver_union.loc[id]['ARR']
                capture = df_driver_union.loc[id]['capture']
            else:
                ARR = df_driver_union.loc[id]['ARR_unit']
                capture = df_driver_union.loc[id]['capture_unit']
            filled_values = float(ARR*capture*col_d_values)
        elif model == "regression":
            coef = df_driver_union.loc[id]['coef']
            intercept = df_driver_union.loc[id]['intercept']
            filled_values = float(coef * col_d_values + intercept)

        df_driver_union.loc[id, 'filled_values'] = filled_values
        df_driver_union.loc[id, 'values'] = float(col_values)
        df_driver_union.loc[id, 'col_d_values'] = float(col_d_values)
        df_new_data.loc[date, col] = filled_values
    df_driver_union.to_csv(os.path.join('data', '%s'%hotel_id, 'predicted_{}'.format(file_df_driver_union)), index=False)
    df_new_data.to_csv(os.path.join('data', '%s'%hotel_id, 'store_df_{}'.format(file_df_driver_union)), index=False)


def predict_in_sample(file_df_driver_union, df, model = "error"):
    '''
    In case of in_sample predict type, we create a dataframe that is filled by
    our predict method and export it in .csv file as output
    :param file_df_driver_union: name of a .csv file that represents the dataframe
    that has dominant driver and filled arr and capture.
    :param df: a dataframe that has filled data
    :param model: (default "error") type of model, choose either "error", "corr" 
    or "regression" ("corr" and "error" give the same result)
    '''
    df_driver_union = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_df_driver_union), index_col=False)

    df_driver_union['date'] = pd.to_datetime(df_driver_union['date'])
    df_driver_union['filled_values'] = -1
    df_driver_union['values'] = -1
    df_driver_union['col_d_values'] = -1
    for id in df_driver_union.index:
        if id % 100 == 0:
            print(id)
        col = df_driver_union.iloc[id]['col']
        col_d = df_driver_union.iloc[id]['col_d']
        lag = int(col_d.split("_")[3])
        col_d = "_".join(col_d.split("_")[:3])
        type_col = col.split("_")[-1]
        date = df_driver_union.iloc[id]['date']

        col_d_values = df[df['date'] == (date-timedelta(days=lag))][col_d]
        # print(date, lag, col_d, col_d_values)
        col_values = df[df['date'] == date][col]
        if ((model == "error") or (model == "corr")):
            if type_col == 'rv':
                ARR = df_driver_union.loc[id]['ARR']
                capture = df_driver_union.loc[id]['capture']
            else:
                ARR = df_driver_union.loc[id]['ARR_unit']
                capture = df_driver_union.loc[id]['capture_unit']
            filled_values = float(ARR*capture*col_d_values)
        elif model == "regression":
            coef = df_driver_union.loc[id]['coef']
            intercept = df_driver_union.loc[id]['intercept']
            filled_values = float(coef * col_d_values + intercept)

        df_driver_union.loc[id, 'filled_values'] = filled_values
        df_driver_union.loc[id, 'values'] = float(col_values)
        df_driver_union.loc[id, 'col_d_values'] = float(col_d_values)
    df_driver_union.to_csv(os.path.join('data', '%s'%hotel_id, 'predicted_{}'.format(file_df_driver_union)), index=False)



def prepare_plot_df(file_plot_df):
    '''
    load data from csv file and do some preprocessing works
    :param file_plot_df: name of .csv file
    :return: a preprocessed dataframe
    '''
    if type(file_plot_df) == type(None):
        return None
    else:
        plot_df = pd.read_csv(os.path.join('data', '%s'%hotel_id, file_plot_df), index_col=False)
        plot_df["date"] = pd.to_datetime(plot_df['date'])
        plot_df["day_of_week"] = plot_df['date'].dt.dayofweek
        return plot_df



if __name__ == "__main__":

    # ===================== PREPARE DATA ========================
    # ===========================================================


    # # ========= INPUT ===============

    segment_name = pd.read_csv(os.path.join('data', '223','hotel_233_segment_name.csv'))
    hotel_id = str(segment_name["hotel_id"].iloc[0])

    rn_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'223_rn_full.csv'))
    rv_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'223_rv_full.csv'))
    gn_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'223_gn_full.csv'))
    

    # choose: "in_sample" / "out_sample"
    type_predict = "in_sample"
    ## choose: "error" / "regression" / "corr"
    model = "error"
    # choose: "yes" / "no"
    otb_run = "yes"

    tf_input_raw = "01-01-2012, 12-31-2015"
    season_target = [("01-01-2012", "12-31-2015")]

    list_shifted_names = ["Breakfast"]

    # # ========== SOME WORK WITH INPUT =========
    segment_name = add_total_segname(segment_name)

    rn_df = add_total_to_df(rn_df)
    gn_df = add_total_to_df(gn_df)
    rv_df = add_total_to_df(rv_df)

    rv_df = shift_data(rv_df, segment_name, list_shifted_names)

    date_s = rn_df['date']
    del rn_df['date']
    del rv_df['date']
    del gn_df['date']

    rn_df.columns = ["{0}_rn".format(col) for col in rn_df.columns]
    rv_df.columns = ["{0}_rv".format(col) for col in rv_df.columns]
    gn_df.columns = ["{0}_gn".format(col) for col in gn_df.columns]

    df = pd.concat([date_s, rv_df, rn_df, gn_df], axis=1)
    df['date'] = pd.to_datetime(df['date'])
    df['day_of_week'] = df['date'].dt.dayofweek


    tf_input = tf_input_raw.replace(" ", "")
    tf_input = tf_input.split(",")
    year_input = [int(tf_input[0].split("-")[2]), int(tf_input[1].split("-")[2])]
    name_year = str(year_input).replace(" ", "").replace("[", "").replace("]", "").replace(",", "_")


    year_target = range(int(season_target[0][0].split("-")[2]), int(season_target[0][1].split("-")[2]) + 1)
    # temp_year = "2015"
    # year_target = [int(temp_year)]

    df_full = df[df.columns]
    df_insample = df[(df['date'] >= tf_input[0]) & (df['date'] <= tf_input[1])]

    if otb_run == "yes":
        otb_df = pd.read_csv(os.path.join('data', '%s'%hotel_id,'otb_all_dep_hotel_223.csv'))


    all_col = set(df.columns) - {'date', 'day_of_week'}

    all_rv_col = list(all_col)
    temp_list = list(all_col)
    all_rv_col_without_hid = list(all_rv_col)
    for i in temp_list:
        if (("rn" in i) or ("gn" in i)):
            all_rv_col.remove(i)
            all_rv_col_without_hid.remove(i)
        if (hotel_id in i):
            try:
                all_rv_col_without_hid.remove(i)
            except:
                pass
    all_dep = set([i.split("_")[0] for i in all_col])

    period_dict = {}
    for i,col in enumerate(all_rv_col_without_hid):
        ids = col.split("_")
        try:
            # maybe this code make the file slow
            period_dict[ids[0]] = lib.read_dep_season_timeframe(ids[0], hotel_id)
        except:
            pass

    significance_level = 0.05
    num_day = 10

    filled_list = create_filled_list(df, hotel_id)
    print("hi")


    # # # ================= CREATE CORR DICTIONARY ==================
    # # # ===========================================================
    # dict_df_error = {}
    # dict_df_corr = {}
    # dict_df_error_week = {}
    # dict_df_corr_week = {}

    # for col in all_rv_col_without_hid: # to do # done
    #     print(col)
    #     dep_id = col.split("_")[0]
    #     seg_id = col.split("_")[1]
    #     l_season_tf = lib.read_dep_season_timeframe(dep_id, hotel_id)
    #     for season_tf in l_season_tf:
    #         # print(season_tf)
    #         key = "{0}_{1}".format(col, season_tf)
    #         # if model == "error":
    #         dict_df_error_week[key] = department_correlation.compute_corr_ver_2_week(dep_id, seg_id, season_tf, num_day,\
    #                                            significance_level, df_insample, year_input)
    #         # if model == "regression" or model == "corr":
    #         dict_df_corr_week[key] = department_correlation.compute_corr_ver_3_week(dep_id, seg_id, season_tf, num_day,\
    #                                            significance_level, df_insample)

    #         for day_of_week in xrange(7):
    #             # print col, season_tf, day_of_week
    #             key = "{0}_{1}_{2}".format(col, season_tf, day_of_week)
    #             # if model == "error":
    #             dict_df_error[key] = department_correlation.compute_corr_ver_2(dep_id, seg_id, season_tf, num_day,\
    #                                                day_of_week, significance_level, df_insample)
    #             # if model == "regression" or model == "corr":
    #             dict_df_corr[key] = department_correlation.compute_corr_ver_3(dep_id, seg_id, season_tf, num_day,\
    #                                                day_of_week, significance_level, df_insample)
    # # if model == "error":
    # pickle.dump(dict_df_error, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_error"), "wb"))
    # pickle.dump(dict_df_error_week, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_error_week"), "wb"))
    # # elif ((model == "regression") or (model == "corr")):
    # pickle.dump(dict_df_corr, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_corr"), "wb"))
    # pickle.dump(dict_df_corr_week, open(os.path.join('data', '%s'%hotel_id, "pickle_dict_df_corr_week"), "wb"))



    # # ==================== CREATE DRIVER DF =====================
    # # ===========================================================
    # driver_ver_09_cl = create_driver_df.create_driver_df_cl(df, tf_input, year_input, \
    #                     filled_list, significance_level, segment_name, num_day = num_day, model = model)

    # try:
    #     if model == "error":
    #         with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_error.txt'), 'rb') as handle:
    #             dict_store = pickle.load(handle)
    #             driver_ver_09_cl.df_seg_dict = dict_store
    #         with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_error_week.txt'), 'rb') as handle:
    #             dict_store_week = pickle.load(handle)
    #             driver_ver_09_cl.df_seg_dict_week = dict_store_week
    #     elif model == "regression" or model == "corr":
    #         with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_corr.txt'), 'rb') as handle:
    #             dict_store = pickle.load(handle)
    #             driver_ver_09_cl.df_seg_dict = dict_store
    #         with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_corr_week.txt'), 'rb') as handle:
    #             dict_store_week = pickle.load(handle)
    #             driver_ver_09_cl.df_seg_dict_week = dict_store_week
    # except:
    #     print("hey, look at me")
    #     driver_ver_09_cl.prepare_dict()
    #     driver_ver_09_cl.prepare_dict_week()


    # # driver_ver_09, top_ver_09 = driver_ver_09_cl.create_both_driver_top([("01-01", "12-31")], year_target[0])
    # driver_ver_09, top_ver_09 = driver_ver_09_cl.create_both_driver_top(season_target)
    
    # driver_ver_09.to_csv(os.path.join('data', '%s'%hotel_id,'driver_%s_%s.csv'%(name_year, model)))
    # top_ver_09.to_csv(os.path.join('data', '%s'%hotel_id,'top_df_%s_%s.csv'%(name_year, model)))


    # df_driver_rn = driver_ver_09[driver_ver_09.columns]
    # df_driver_gn = driver_ver_09[driver_ver_09.columns]
    # df_driver_rn['col_d'] = '%s_0_rn_1'%hotel_id
    # df_driver_gn['col_d'] = '%s_0_gn_1'%hotel_id

    # df_driver_rn.to_csv(os.path.join('data', '%s'%hotel_id,'rn_1driver_%s_%s.csv'%(name_year, model)))
    # df_driver_gn.to_csv(os.path.join('data', '%s'%hotel_id,'gn_1driver_%s_%s.csv'%(name_year, model)))


    # # # ============= DOMINANT DRIVER + FILL CAP & ARR ============
    # # # ===========================================================

    # name_file_driver = 'driver_%s_%s.csv'%(name_year, model)
    # name_file_driver_rn = "rn_1driver_%s_%s.csv"%(name_year, model)
    # name_file_driver_gn = "gn_1driver_%s_%s.csv"%(name_year, model)
    # # df_driver = prepare_plot_df(name_file_driver)
    # # df_driver_rn = df_driver[df_driver.columns]
    # # df_driver_gn = df_driver[df_driver.columns]
    # # df_driver_rn['col_d'] = '%s_0_rn_1'%hotel_id
    # # df_driver_gn['col_d'] = '%s_0_gn_1'%hotel_id


    # # dominant_and_fill(name_file_driver, df_driver, df_insample)
    # # dominant_and_fill(name_file_driver_rn, df_driver_rn, df_insample)
    # # dominant_and_fill(name_file_driver_gn, df_driver_gn, df_insample)

    # if model == "regression":
    #     type_fill = 'regr'
    # else:
    #     type_fill = 'arr'
    # file_driver = os.path.join('data', '%s'%hotel_id, 'driver_%s_%s.csv'%(name_year, model))
    # filled_df_driver = fill_capture_arr.fill_driver(file_driver, df_insample, hotel_id, type= type_fill)
    # filled_df_driver.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_file_driver)), index=False)

    # file_driver_rn = os.path.join('data', '%s'%hotel_id, 'rn_1driver_%s_%s.csv'%(name_year, model))
    # filled_df_driver_rn = fill_capture_arr.fill_driver(file_driver_rn, df_insample, hotel_id, type= type_fill)
    # filled_df_driver_rn.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_file_driver_rn)), index=False)

    # file_driver_gn = os.path.join('data', '%s'%hotel_id, 'gn_1driver_%s_%s.csv'%(name_year, model))
    # filled_df_driver_gn = fill_capture_arr.fill_driver(file_driver_gn, df_insample, hotel_id,type= type_fill)
    # filled_df_driver_gn.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_file_driver_gn)), index=False)
    

    # # # # # =================== CONVERT DATE DF =======================
    # # # # # ===========================================================
    # # # # # # WARNING: 
    # # # # # # this code is no longer used, because we do not need to 
    # # # # # # change date of driver df

    # # # # # file_df_driver = "filled_223_0_driver_2012_2014.csv"
    # # # # # convert_date_df(file_df_driver, period_dict, temp_year)

    # # # # # # fill _rn
    # # # # # start_time = time.time()
    # # # # # file_df_driver_rn = "filled_223_0_rn_1driver_2012_2014.csv"
    # # # # # convert_date_df(file_df_driver_rn, period_dict, temp_year)

    # # # # # print("--- %s seconds ---" % (time.time() - start_time))

    # # # # # # fill _gn
    # # # # # start_time = time.time()

    # # # # # file_df_driver_gn = "filled_223_0_gn_1driver_2012_2014.csv"
    # # # # # convert_date_df(file_df_driver_gn, period_dict, temp_year)
    # # # # # print("--- %s seconds ---" % (time.time() - start_time))



    # # # # # ======================= PREDICTION ========================
    # # # # # ===========================================================

    # df_test = df[df.columns]
    # not_change_list = filled_list + ["date", "day_of_week"]
    # df_test[list(set(df.columns) - set(not_change_list))] = 0
    # df_test.ix[df_test.date < pd.to_datetime("2015-01-01")] = df.ix[df.date < pd.to_datetime("2015-01-01")]


    # start_time = time.time()
    # file_df_driver_union = "filled_%s_0_"%hotel_id + name_file_driver
    # file_df_driver_union_rn = "filled_%s_0_"%hotel_id + name_file_driver_rn
    # file_df_driver_union_gn = "filled_%s_0_"%hotel_id + name_file_driver_gn

    # if type_predict == "in_sample":
    #     predict_in_sample(file_df_driver_union, df_full, model = model)
    #     predict_in_sample(file_df_driver_union_rn, df_full, model = model)
    #     predict_in_sample(file_df_driver_union_gn, df_full, model = model)
    #     print("--- %s seconds ---" % (time.time() - start_time))
    # else:
    #     predict_df(file_df_driver_union, df_full, model = model)
    #     predict_df(file_df_driver_union_rn, df_full, model = model)
    #     predict_df(file_df_driver_union_gn, df_full, model = model)
    #     print("--- %s seconds ---" % (time.time() - start_time))




    # # # # ======================= PLOT SAMPLE =======================
    # # # # ===========================================================

    # file_plot_df = "predicted_" + file_df_driver_union
    # file_plot_df_rn = "predicted_" + file_df_driver_union_rn
    # file_plot_df_gn = "predicted_" + file_df_driver_union_gn

    file_plot_df = "predicted_filled_223_0_driver_2012_2015_error.csv"
    file_plot_df_rn = "predicted_filled_223_0_rn_1driver_2012_2015_error.csv"
    file_plot_df_gn = "predicted_filled_223_0_gn_1driver_2012_2015_error.csv"
    print("hi, again")

    plot_df = prepare_plot_df(file_plot_df)
    plot_df_rn = prepare_plot_df(file_plot_df_rn)
    plot_df_gn = prepare_plot_df(file_plot_df_gn)


    # # ============ OTB ============
    # # =============================
    print("2")
    start_time = time.time()

    if otb_run == "yes":
        # driver_otb_cl = create_driver_df.create_driver_df_cl(df, tf_input, year_input, \
        #             filled_list, significance_level, segment_name, num_day = num_day, model = "corr")
        # try:
        #     print("3")
        #     with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_corr.txt'), 'rb') as handle:
        #         otb_corr = pickle.load(handle)
        #         driver_otb_cl.df_seg_dict = otb_corr
        #     with open(os.path.join('data', '%s'%hotel_id,'pickle_dict_df_corr_week.txt'), 'rb') as handle:
        #         otb_corr_week = pickle.load(handle)
        #         driver_otb_cl.df_seg_dict_week = otb_corr_week    
        #     print("4")
        # except:
        #     print("it's suck")
        #     driver_otb_cl.prepare_dict()
        #     driver_otb_cl.prepare_dict_week()
        # # can create new function to not create top --> faster
        # print("5")
        # driver_otb, top_otb = driver_otb_cl.create_both_driver_top(season_target)
        # name_otb = 'driver_otb_%s.csv'%(name_year)
        # driver_otb.to_csv(os.path.join('data', '%s'%hotel_id, name_otb))

        file_otb = os.path.join('data', '%s'%hotel_id, name_otb)
        filled_df_otb = fill_capture_arr.fill_driver(file_otb, df_insample, hotel_id, type= 'regr')
        filled_df_otb.to_csv(os.path.join('data', '%s'%hotel_id,'filled_{}_0_{}'.format(hotel_id, name_otb)), index=False)

        file_df_otb = "filled_%s_0_"%hotel_id + name_otb

        if type_predict == "in_sample":
            predict_in_sample(file_df_otb, df_full, model = 'regression')
            print("--- %s seconds ---" % (time.time() - start_time))
        else:
            predict_df(file_df_otb, df_full, model = 'regression')
            print("--- %s seconds ---" % (time.time() - start_time))

        file_plot_otb = "predicted_" + file_df_otb
        plot_df_regression = prepare_plot_df(file_plot_otb)
        # run change predict_df
        otb_dict = {}
        snapshot_date = pd.to_datetime(season_target[0][0])
        for rv_col in all_rv_col_without_hid:
            ids = rv_col.split("_")
            otb_dict[rv_col] = lib.compute_otb_adjust_ratio(int(ids[0]), int(ids[1]), snapshot_date, otb_df)
        df_otb = lib.create_otb(plot_df_regression, all_rv_col_without_hid, otb_dict)
    else:
        df_otb = None


    file_top_df = 'top_df_%s_%s.csv'%(name_year, model)
    top_df = prepare_plot_df(file_top_df)



    start_time = time.time()
    hotel_plot_driver = plot_driver_df_ver_1.plot_driver_df(\
        years = year_target, df_driver_union = plot_df, df_driver_union_rn = plot_df_rn, \
        df_driver_union_gn = plot_df_gn, df_otb = df_otb, \
        df_ = df, top_df = top_df, segment_name = segment_name,\
        num_of_top = 3, model = model, significance_level = significance_level)

    for rv_col in all_rv_col_without_hid:
        hotel_plot_driver.plot_error(rv_col, df_driver_union = plot_df, \
            df_driver_union_rn = plot_df_rn, df_driver_union_gn = plot_df_gn, df_otb = df_otb)
        
    # hotel_plot_driver.plot_error("235_0_rv", df_driver_union = plot_df, \
    #     df_driver_union_rn = plot_df_rn, df_driver_union_gn = plot_df_gn, df_otb = df_otb)

    # hotel_plot_driver.plot_all_table("235_0_rv", df, num_of_top = 3, id_to_name = True)


    # for rv_col in all_rv_col_without_hid:
    #     hotel_plot_driver.plot_all_table(rv_col, df, num_of_top = 3, id_to_name = True)
    #     # hotel_223_plot_driver.plot_all_table("226_18_rv", df, num_of_top = 3, id_to_name = True)
    # print("--- %s seconds ---" % (time.time() - start_time))

    os.system('say "your program has finished"')


















