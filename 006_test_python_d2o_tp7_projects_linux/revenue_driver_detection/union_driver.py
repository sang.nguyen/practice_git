import pandas as pd
from datetime import timedelta
from capture_arr import *



if __name__ == "__main__":
    rn_df = pd.read_csv('hotel_223_corr_rn_without_zero.csv')
    rv_df = pd.read_csv('hotel_223_corr_rv_without_zero.csv')
    gn_df = pd.read_csv('hotel_223_guest_4y_1Apr.csv')

    years = [2015, 2014]
    day_of_week = 0
    num_day = 10
    hotel_id = 223
    season_tf = ['01-16', '05-01']

    date_s = rn_df['date']
    del rn_df['date']
    del rv_df['date']
    del gn_df['date']

    rn_df.columns = ["{0}_rn".format(col) for col in rn_df.columns]
    rv_df.columns = ["{0}_rv".format(col) for col in rv_df.columns]
    gn_df.columns = ["{0}_gn".format(col) for col in gn_df.columns]

    df = pd.concat([date_s, rv_df, rn_df, gn_df], axis=1)
    df['date'] = pd.to_datetime(df['date'])
    #df = df.set_index('date')

    # Read driver
    df_driver_union = pd.read_csv('filled_driver_2014_223_ver2.csv', index_col=False)
    df_driver_union['date'] = pd.to_datetime(df_driver_union['date'])
    df_driver_union['filled_values'] = -1
    df_driver_union['values'] = -1
    df_driver_union['col_d_value'] = -1
    for id in df_driver_union.index:
        print id
        col = df_driver_union.iloc[id]['col']
        col_d = df_driver_union.iloc[id]['col_d']
        lag = int(col_d.split("_")[3])
        col_d = "_".join(col_d.split("_")[:3])
        type_col = col.split("_")[-1]
        date = df_driver_union.iloc[id]['date']
        col_d_values = df[df['date'] == (date-timedelta(days=lag))][col_d]
        col_values = df[df['date'] == date][col]
        if type_col == 'rv':
            ARR = df_driver_union.loc[id]['ARR']
            capture = df_driver_union.loc[id]['capture']
        else:
            ARR = df_driver_union.loc[id]['ARR_unit']
            capture = df_driver_union.loc[id]['capture_unit']
        df_driver_union.loc[id, 'filled_values'] = float(ARR*capture*col_d_values)
        df_driver_union.loc[id, 'values'] = float(col_values)
        df_driver_union.loc[id, 'col_d_value'] = float(col_d_values)
    df_driver_union.to_csv('predicted_223.csv', index=False)