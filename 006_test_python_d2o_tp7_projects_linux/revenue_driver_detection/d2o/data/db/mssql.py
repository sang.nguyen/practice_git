from d2o.utils import putils as put
from d2o.utils import logger as log
from d2o.utils.timeout import timeout
from d2o.utils.handlers import ExceptionHandler

import sqlalchemy as sqla
from sqlalchemy.orm import sessionmaker
import pyodbc
import re
import decimal
import datetime
import numpy as np
import pandas as pd

from IPy import IP

def valid_ip(s):
  try:
    IP(s)
    return True
  except:
    return False

class MSSQL:
  def __init__(self, user, password, host, database=None, driver='FreeTDS'):
    host_is_ip = valid_ip(host)
    self.user = user
    self.password = password
    self.host = host
    self.database = database
    self.driver = driver

    # TODO Check for windows -> driver = 'SQL Server'
    connection_string = ""
    if (host_is_ip):
      connection_string = 'mssql+pymssql://%s:%s@%s:1433/%s' % (user,password,host,database)
    else:
      connection_string = 'mssql+pyodbc://%s:%s@%s' % (user,password,host)

    self.engine = sqla.create_engine(connection_string, encoding='utf-8', pool_timeout=5, connect_args = {'timeout':5})

  def pyodbc_connection(self):
    connection_string = 'DRIVER=%s;SERVER=%s;PORT=1433;DATABASE=%s;UID=%s;PWD=%s;TDS_Version=8.0;' % (self.driver, self.host, self.database, self.user, self.password)
    try:
      return pyodbc.connect(connection_string)
    except Exception as e:
      log.err(str(ExceptionHandler(e)) + "\n Make sure you're connecting using IP and not DSN")

  @timeout(5)
  def is_connected(self):
    try:
      connection = self.engine.raw_connection()
      cursor = connection.cursor()
      cursor.close()
      connection.commit()
      connection.close()
      return True
    except Exception as e:
      log.err(ExceptionHandler(e))
      return False

  def execute(self, query):
    try:
      self.engine.execute(query)
    except:
      raise

  def select(self, query):
    try:
      result = self.engine.execute(query)
      return result
    except:
      raise

  def execute_raw(self, query):
    try:
      connection = self.engine.raw_connection()
      cursor = connection.cursor()
      cursor.execute(query)
      cursor.close()
      connection.commit()
    except Exception as e:
      log.err(ExceptionHandler(e))
    finally:
      connection.close()

  def select_raw(self, query):
    results = None
    try:
      connection = self.engine.raw_connection()
      cursor = connection.cursor()
      cursor.execute(query)
      results = cursor.fetchall()
      cursor.close()
      connection.commit()
    except Exception as e:
      log.err(ExceptionHandler(e))
    finally:
      connection.close()
    log.dbg("RESULT:\n %s" % results)
    return results

  def select_df(self, query, index_col=None, parse_dates=None):
    df = pd.read_sql(query, self.engine, index_col=index_col, parse_dates=parse_dates)
    log.dbg("RESULT df:\n %s" % df)

    return df
    # df = None
    # try:
    #   connection = self.engine.raw_connection()
    #   cursor = connection.cursor()
    #   cursor.execute(query)
    #   df = self.__processCursor(cursor, index_col=index_col)
    # finally:
    #   connection.close()
    # return df

  def insert(self, query):
    return NotImplementedError

  def __processCursor(self, cur, index_col=None):
    datatypes = []
    colinfo = cur.description
    for col in colinfo:
      if col[1] == unicode:
        datatypes.append((col[0], 'U%d' % col[3]))
      elif col[1] == str:
        datatypes.append((col[0], 'S%d' % col[3]))
      elif col[1] in [float, decimal.Decimal]:
        datatypes.append((col[0], 'f4'))
      elif col[1] == datetime.datetime:
        datatypes.append((col[0], 'O4'))
      elif col[1] == int:
        datatypes.append((col[0], 'i4'))

    data = []
    for row in cur:
      data.append(tuple(row))

    array = np.array(data, dtype=datatypes)
    df = pd.DataFrame.from_records(array)

    if index_col is not None:
      df = df.set_index(index_col)

    cur.close()
    return df



