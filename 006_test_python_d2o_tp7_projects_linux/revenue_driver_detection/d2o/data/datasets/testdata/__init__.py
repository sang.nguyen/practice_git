"""
print "WARNING: /clib/data/datasets/testdata/__init__.py: Initializes matrixes at import"
print "         TODO: Create dataset class to handle loading of datasets"
import pandas as pd
from os import path
file_path = path.dirname(path.realpath(__file__)) + "/correlation_matrix_test_data.csv"

correlation_matrix_test_data = pd.read_csv(file_path, index_col=0)
"""