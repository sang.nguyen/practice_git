from d2o.prep.scaling import Scaler
from d2o.utils.handlers import ExceptionHandler
from d2o.utils import logger as log
from d2o.prep import scaling

from matplotlib.colors import hsv_to_rgb, hex2color, rgb2hex
from datetime import datetime, timedelta
import sklearn.preprocessing as pre
import pandas as pd
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import itertools
import copy

MIN_PERIOD_LENGTH      = 8
MAX_TOPS               = 2
CP_DISTANCE            = 7

SEAM_SIZE_PERCENT = 0.2

rn_ranges = {
  .90:'peak',
  .70:'high',
  .50:'medium',
  .30:'low',
  .10:'dip'
  }

rv_ranges = {
  .90:'expensive',
  .70:'above',
  .50:'normal',
  .30:'below',
  .10:'cheap'
  }

def get_closest_in_range(val, rv=False):
  if (rv):
    closest_val = min(rv_ranges.keys(), key=lambda x:abs(x-val))
    closest_key = rv_ranges[closest_val]
  else:
    closest_val = min(rn_ranges.keys(), key=lambda x:abs(x-val))
    closest_key = rn_ranges[closest_val]
  return (closest_key, closest_val)

class Period:
  def __init__(self, x0, x1, df=[]):
    self.x0 = x0
    self.x1 = x1
    self.df = df

    self.median_sig = None
    self.avg_sig = None
    self.name = None

    self.vspan = None
    self.rgb  = np.array([])
    self.hex = None

  def length(self):
    return abs(self.x0 - self.x1)

  def middle(self):
    return (self.x0 + self.x1)/2.0

  def add_vspan(self, pltobj):
    self.vspan = pltobj.axvspan(self.x0,self.x1, alpha=0.7)
    if (self.rgb.size > 0):
      self.vspan.set_color(self.rgb)
    return self.vspan

  def update_pos(self, x):
    self.x0 = x[0]
    self.x1 = x[1]
    y0 = 0.0
    y1 = 1.0

    if (self.vspan != None):
      xy = np.array([
        [self.x0,y0],
        [self.x0,y1],
        [self.x1,y1],
        [self.x1,y0],
        [self.x0,y0]])

      self.vspan.xy = xy

  def update_color(self, color_cols=[None,None,None], color_map=None):
    if (color_map != None):
      rn_n = None
      rv_n = None
      for key,val in self.median_sig.iteritems():
        if ('rate' in key.lower()):
          rv_n = get_closest_in_range(val, rv=True)[0]
        else:
          rn_n = get_closest_in_range(val, rv=False)[0]

      self.hex = color_map[rn_n][rv_n]
      self.rgb = np.array(hex2color(self.hex))

    elif (len(self.df) > 0):
      h = get_closest_in_range(np.median(self.df[color_cols[0]].values.tolist()[self.x0:self.x1]))[1] if (color_cols[0] != None) else 1.0
      s = get_closest_in_range(np.median(self.df[color_cols[1]].values.tolist()[self.x0:self.x1]))[1] if (color_cols[1] != None) else 1.0
      v = get_closest_in_range(np.median(self.df[color_cols[2]].values.tolist()[self.x0:self.x1]))[1] if (color_cols[2] != None) else 1.0

      hsv_array = np.array([h,s,v])
      self.rgb = hsv_to_rgb(hsv_array)
      self.hex = rgb2hex(self.rgb)
    if (self.vspan != None):
      self.vspan.set_color(self.rgb)

  def update(self, cols):
    # Median sig
    sig_dict = dict()
    for col in cols:
      if (col == None):
        continue
      sig_dict[col] = np.median(self.df[col].values.tolist()[self.x0:self.x1])
    self.median_sig = sig_dict

    # Avg sig
    sig_dict = dict()
    for col in cols:
      if (col == None):
        continue
      sig_dict[col] = np.average(self.df[col].values.tolist()[self.x0:self.x1])
    self.avg_sig = sig_dict

    # Name
    avg    = self.median_sig
    names  = []
    for key,val in avg.iteritems():
      identifier = key
      if (identifier == 'Rate'):
        identifier = 'ARR'
      elif (identifier == 'Room Nights'):
        identifier = '%'

      if ('rate' in key.lower()):
        n = get_closest_in_range(val, rv=True)
      else:
        n = get_closest_in_range(val, rv=False)
      name = '{0}: {1}'.format(identifier,n[0].title())
      names.append(name)
    ret = '\n'.join(names)

    self.name = ret

  def rgb_string(self):
    return 'r:%s, g:%s, b:%s' % tuple(self.rgb)

  def __repr__(self):
    s  = "Coords: %s -> %s\n" % (self.x0, self.x1)
    s += "Msig  : %s\n" % (self.median_sig)
    s += "Asig  : %s\n" % (self.avg_sig)
    s += "Color : %s\n" % (self.rgb)
    return s


class ZScoreDetector():
  def __init__(self, df, color_cols=[], color_map=None, circular=False):

    self.is_circular = circular
    self.overlap = None

    if (self.is_circular):
      self.overlap = df[:60]
      self.overlap.index += 366
      df = pd.concat([df,self.overlap], axis=0)

    if (len(color_cols) == 3):
      self.use_colors  = True
      self.color_cols = color_cols
    else:
      self.use_colors = False

    if (color_map != None):
      self.color_map = color_map
    else:
      self.color_map = {
        'peak'  : {'expensive':'#98edb5', 'above':'#9de6fe', 'normal':'#fffeab', 'below':'#ffc0a4', 'cheap':'#ff96d1' },
        'high'  : {'expensive':'#88d63c', 'above':'#3fcdfd', 'normal':'#fffe69', 'below':'#fb9262', 'cheap':'#fc4eb0' },
        'medium': {'expensive':'#3cc923', 'above':'#3d8ddc', 'normal':'#f1ef00', 'below':'#fc6621', 'cheap':'#d8017a' },
        'low'   : {'expensive':'#20a308', 'above':'#454ea0', 'normal':'#ffdd1c', 'below':'#d85013', 'cheap':'#a0300e' },
        'low'   : {'expensive':'#20a308', 'above':'#454ea0', 'normal':'#ffdd1c', 'below':'#db650a', 'cheap':'#e4000d' },
        'dip'   : {'expensive':'#026f02', 'above':'#0515a9', 'normal':'#d4b400', 'below':'#9d4b00', 'cheap':'#97040c' }
        }

    self.df = df
    self.y = None
    self.diffs = None

  def calculate_zscores(self, iwindow_size, owindow_size):
    z_scores = [0.0]*len(self.y)
    for i in range(0, len(self.y)):
      ostart  = (i-owindow_size/2)
      ostart  = ostart if  (ostart > 0) else 0
      oend    = (i+owindow_size/2)
      oend    = oend if (oend < len(self.y)) else len(self.y)
      owindow = self.y[ostart:oend]

      iend    = i + int(iwindow_size/2)
      #iend    = i+int(iwindow_size)
      iend    = iend if (iend < len(self.y)) else len(self.y)
      istart  = i - int(iwindow_size/2)
      #istart  = ostart
      istart  = istart if (istart > 0) else 0
      iwindow = self.y[istart:iend]

      iwindow_mean = np.average(iwindow)
      iwindow_std  = np.std(iwindow)

      owindow_mean = np.average(owindow)
      owindow_std  = np.std(owindow)

      SE = iwindow_std/np.sqrt(len(iwindow))
      zs = (iwindow_mean - owindow_mean)/SE if (SE > 0.0) else 0.0

      z_scores[i] = np.abs(zs)

      # try:
      #   z_scores[i-1] = z_scores[i-1] + np.abs(zs)/1.5
      # except:
      #   pass
    return z_scores

  def add_padding(self, window, padding_size, min_size, max_size):
    window = sorted(list(set(window)))
    wstart = window[0]
    wend   = window[-1]
    head = []
    tail = []
    for i in range(0,padding_size):
      if (wstart-i >= min_size):
        head.append(wstart-i)
      if (wend+i <= max_size):
        tail.append(wend+i)
    window = sorted(list(set(head + window + tail)))
    return window

  def calculate_change_windows(self, z_scores, owindow_size):
    windows     = []
    curr_window = []
    for i in range(0,len(self.y)):
      wstart = i - owindow_size/2
      wend   = i + owindow_size/2
      wstart = wstart if wstart > 0 else 0
      wend   = wend if (wend < len(z_scores)) else len(z_scores)

      window_zscores = z_scores[wstart:wend]
      z_treshold = np.median(window_zscores)/1.5

      if (z_scores[i] > z_treshold):
        curr_window.append(i)
      else:
        if (len(curr_window) > 0):
          curr_window = self.add_padding(curr_window, 2, 0, len(self.y))
          windows.append(curr_window)
          curr_window = []

    return windows

  def calculate_changepoints(self, windows):
    cps = []
    for w in windows:
      try:
        diff_range = self.diffs[w[0]:w[-1]]
        sorted_diff_range = sorted(diff_range, reverse=True)
        diff_threshold = np.median(self.diffs)
        for i in range(0,MAX_TOPS):
          top = sorted_diff_range[i]
          if (top > diff_threshold):
            index = w[diff_range.index(top)]
            cps.append(index)
      except Exception as e:
        log.dbg(ExceptionHandler(e))
        continue

    cps = sorted(list(set([0] + cps + [len(self.y)-1])))
    return cps

  def define_periods(self, changepoints):
    periods = []
    delims = [(changepoints[j], changepoints[j+1]) for j in range(0, len(changepoints)-1)]
    for d in delims:
      if (abs(d[1] - d[0]) > 0):
        p = Period(d[0],d[1],self.df)
        p.update(self.color_cols)
        p.update_color(self.color_cols, color_map=self.color_map)
        periods.append(p)
    return periods

  def merge_similar(self, periods, min_length):
    periods_copy  = periods
    short_periods = []
    for p in periods[1:-1]:
      if (p.length() < min_length):
        short_periods.append(p)

    if (len(short_periods) <= 0):
      return periods_copy

    for curr_p in short_periods:
      try:
        curr_sig   = curr_p.rgb
        skip = False
        prev_p = [x for x in periods if x.x1 == curr_p.x0][0]
        prev_sig = prev_p.rgb

        next_p = [x for x in periods if x.x0 == curr_p.x1][0]
        next_sig = next_p.rgb

        if (np.linalg.norm(curr_sig - prev_sig) > np.linalg.norm(next_sig - curr_sig)):
          next_p.update_pos([curr_p.x0, next_p.x1])
          next_p.update_color(color_cols=self.color_cols, color_map=self.color_map)
        else:
          prev_p.update_pos([prev_p.x0, curr_p.x1])
          prev_p.update_color(color_cols=self.color_cols, color_map=self.color_map)

        if (curr_p.vspan != None):
          curr_p.vspan.remove()
        del periods_copy[periods_copy.index(curr_p)]
      except Exception as e:
        log.dbg(ExceptionHandler(e))
        continue

    return self.merge_similar(periods_copy, min_length)

  def merge_equal(self, periods):
    zipped = zip(periods, periods)
    nperiods = []
    for p in zipped:
      if (p[0].hex == p[1]):
        p[0].update_pos((p[0].x0,p[1].x1))
        nperiods.append(p[0])
      else:
        nperiods += list(p)
    return nperiods

  def finalize(self, operiods, periods):
    if (self.is_circular):
      for p in periods:
        if (p.x0 >= 366):
          p.x0 -= 366
        if (p.x1 >= 366):
          p.x1 -= 366

      new_periods = []
      for p in periods:
        if (p.x1 > p.x0):
          new_periods.append(p)
        else:
          # if (new_periods[0].x1 < p.x1):
          #   new_periods[0].x1 = int(p.x1)
          p.x1 = 366
          new_periods.append(p)

          new_periods[0].median_sig = p.median_sig
          new_periods[0].avg_sig = p.avg_sig
          new_periods[0].rgb = p.rgb
          new_periods[0].hex = p.hex
          new_periods[0].name = p.name
          break

      periods = new_periods

    for p in periods:
      p.x1 -= 1

    for o in operiods:
      o.x1 -= 1

    return (operiods, periods)

  def run(self, ycol, iwindow_size, owindow_size, min_length):
    self.y = self.df[ycol].values.tolist()
    self.diffs   = np.abs([0.0] + np.diff(self.y,2).tolist() + [0.0])
    z_scores = self.calculate_zscores(iwindow_size, owindow_size)

    # Scale
    df = pd.DataFrame.from_dict({'y': self.y, 'z_scores':z_scores, 'diffs':self.diffs})
    df = scaling.Scaler(method='min_max').fit(df, low=0, high=1).transform(df)

    self.y     = df['y'].values.tolist()
    self.diffs = df['diffs'].values.tolist()
    z_scores   = df['z_scores'].values.tolist()

    cws      = self.calculate_change_windows(z_scores, owindow_size)
    cps      = self.calculate_changepoints(cws)
    operiods = self.define_periods(cps)

    periods  = self.merge_similar(copy.deepcopy(operiods), min_length=min_length)

    operiods, periods  = self.finalize(operiods, periods)

    if (self.is_circular):
      self.df = self.df[:366]
      operiods = [p for p in operiods if p.x1 <= 365]
      cps = [cp for cp in cps if cp <= 365]
      self.diffs = self.diffs[:366]
      z_scores = z_scores[:366]

    resdict = {
      'df':self.df,
      'color_cols':self.color_cols,
      'ycol':ycol,
      'diffs':self.diffs,
      'z_scores':z_scores,
      'cws':cws,
      'cps':cps,
      'operiods':operiods,
      'periods':periods,
      'iw_size':iwindow_size,
      'ow_size':owindow_size,
      'min_length':min_length
    }

    return resdict


