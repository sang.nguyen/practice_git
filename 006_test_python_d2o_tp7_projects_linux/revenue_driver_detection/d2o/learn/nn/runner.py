import matplotlib

import warnings
warnings.filterwarnings("ignore", module="matplotlib") # silence

import matplotlib.pyplot as plt
import d2o.utils.errors as err
import d2o.utils.logger as log
import numpy as np

class Runner:
  def __init__(self, network):
    self.network = network

  def serialize(self, results, descriptors):
    results = pd.DataFrame()

  def run(self, dataset, scale=True):
    """ Per row index:[inputs] for the neural network
    Creates a index:[outputs] result set after running through every row """

    results = self.network.run(dataset)