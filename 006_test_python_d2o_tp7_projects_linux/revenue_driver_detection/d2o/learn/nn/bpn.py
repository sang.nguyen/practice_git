from d2o.learn.nn import transfer_functions as tfs
from d2o.utils import logger as log
from d2o.utils import calc

import numpy as np

class BackpropagationNetwork:
  """
  Neural network utilizing the back propagation (error-propagation) method.
  Inspired by: https://www.youtube.com/user/nqramjets
  """
  def __init__(self, input_mask, target_mask, num_hidden=1, zero_band=False):
    # Network info
    self.zero_band = zero_band
    self.num_hidden = num_hidden

    self.target_mask = target_mask
    self.input_mask = input_mask

    self.input_scaler = None
    self.output_scaler = None
    self.scalers_fitted = False

    hidden_sizes = calc.exponential_decay(len(input_mask) + len(target_mask), self.num_hidden, decay_rate=0.5)
    self.hidden_sizes = [int(x)+1 for x in hidden_sizes]

    self.shape = tuple([len(input_mask)] + self.hidden_sizes + [len(target_mask)])
    self.num_layers = len(self.shape) - 1 # Minus the input layer

    # Input/Output data from the last run
    self.layer_input = []
    self.layer_output = []
    self.weights = []
    self.previous_weight_delta = []

    for (l1,l2) in zip(self.shape[:-1],self.shape[1:]):
      w = np.random.normal(scale=0.01,size=(l2,l1+1))
      # lim = 4 * np.sqrt( 6 / (len(input_mask)+len(target_mask)))
      # lim = 0.9
      # w = np.random.uniform(-lim,lim,size=(l2,l1+1))
      self.weights.append(w)
      self.previous_weight_delta.append(np.zeros((l2,l1+1)))

  def shake_weights(self, amount=0.1):
    for w in self.weights:
      w += w*amount

  def run(self, inputs, inverse_transform=True):
    if (not self.scalers_fitted):
      raise Exception("No scalers prepared")

    num_runs = inputs.shape[0] # Number of rows

    # Clear previous
    self.layer_input  = []
    self.layer_output = []

    # Run the network
    for i in range(self.num_layers):
      if (i == 0): # Input (layer between input and the first hidden layer)
        weights = self.weights[0]

        # Create the input matrix by transposing the input vectors
        # and adding the  bias node (1.0 per input vector)
        _input = np.vstack([inputs.T, np.ones([1,num_runs])]) # Add bias node
      else:
        weights = self.weights[i]
        # Input is the output from the preceding layer
        _input = np.vstack([self.layer_output[-1], np.ones([1,num_runs])]) # Add bias node

      # The nodes' attached input is the input (from the previous layer) multiplied
      # by the weight for that input
      weighted_input = weights.dot(_input)
      self.layer_input.append(weighted_input)

      # The nodes' output is the transformed input values after
      # they have been sent through that node's transfer function
      tf_output = tfs.tanh(weighted_input)
      self.layer_output.append(tf_output)
    if (inverse_transform):
      results = self.output_scaler.inverse_transform(self.layer_output[-1].T) # Append the results column
      if (self.zero_band):
        results = results.clip(min=0)
    else:
      results = self.layer_output[-1].T
    return results

  def train_epoch(self, dataset, learning_rate = 0.2, momentum = 0.5):
    if (not self.scalers_fitted):
      self.input_scaler = dataset.input_scaler
      self.output_scaler = dataset.output_scaler
      self.scalers_fitted = True

    delta = []

    # TODO: Check if input/target masks match the dataset
    inputs  = dataset.training_inputs()
    targets = dataset.training_targets()

    num_runs = inputs.shape[0]

    # Run the network once (on all the training inputs)
    self.run(inputs)

    error = None
    # Calculate node deltas
    for i in reversed(range(self.num_layers)):
      if (i == self.num_layers - 1): # Output layer
        # Compare to target values to get delta values
        output_delta = self.layer_output[i] - targets.T
        error = np.sum(output_delta**2)
        delta.append(output_delta * tfs.tanh_derivative(self.layer_input[i]))
      else: # The following layer's delta
        delta_propagated = self.weights[i+1].T.dot(delta[-1])
        delta.append(delta_propagated[:-1,:] * tfs.tanh_derivative(self.layer_input[i])) # Ignoring the bias nodes

    # Calculate weight deltas
    for i in range(self.num_layers):
      di = self.num_layers - 1 - i
      if (i == 0): # Input layer
        layer_output = np.vstack([inputs.T, np.ones([1, num_runs])])
      else:
        layer_output = np.vstack([self.layer_output[i-1], np.ones([1, self.layer_output[i-1].shape[1]])])

      current_weight_delta = np.sum(layer_output[None,:,:].transpose(2,0,1) * delta[di][None,:,:].transpose(2,1,0), axis=0)

      weight_delta = learning_rate * current_weight_delta + momentum * self.previous_weight_delta[i]

      self.weights[i] -= weight_delta
      self.previous_weight_delta[i] = weight_delta

    return error

  def train(self, dataset, learning_rate = 0.2, momentum = 0.5, epochs = 1000000, min_err = 1e-3):
    errors = []
    for i in range(epochs):
      error = self.train_epoch(dataset, learning_rate, momentum)
      errors.append(error)
      if (i % 100 == 0):
        diffs = abs(np.diff(errors[-100:]))
        roc = float(np.sum(diffs))
        log.info("{0}: {1:.10f} roc: {2:.2f}".format(i,error, roc))

      if (error <= min_err):
        log.info("Reached minimum after {0} epochs".format(i))
        break

    return np.array(errors)

  # def __getstate__(self):
  #   """ Save the network without the dataset """
  #   d = dict(self.__dict__)
  #   del d['dataset'] # We don't want to save the dataset with the network
  #   return d

  # def __setstate__(self, d):
  #   """ Load the network, set dataset = None """
  #   d['dataset'] = None
  #   self.__dict__.update(d)

  def __repr__(self):
    s = "Back Propagation Neural Network\n"
    s += "Shape: %s\n" % (list(self.shape))
    return s



