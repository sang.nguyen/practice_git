import numpy as np
from masks import IndexMask

class d2oMaskFactory:
  def __init__(self, df, target_mask, cutoff=None, req_cols=[], forecasting_cols=[]):
    """ Temporary input mask generator with hard coded values
    Do not use for anything else than d2o dataframes
    """
    self.req_cols = req_cols
    self.forecasting_cols = forecasting_cols

    if (len(req_cols) > 0):
      if (not all([x in df.columns for x in self.req_cols])):
        raise Exception("Missing required columns %s in dataframe" % self.req_cols)

    if (len(target_mask.masks.keys()) != 1):
      raise Exception("Expected one dimensional target mask, got mask with length = %s" % len(target_mask.masks.keys()))

    self.df = df
    self.target_mask = target_mask # The target_mask = [i+t, i+t1, i+t2, ..., i+tn]

    # This implementation only supports one variable/column
    self.target_column = target_mask.masks.keys()[0]
    self.target_indices = target_mask.masks.values()[0][0][:cutoff]

  def input_mask(self, padding=1):
    """ The logic is as follows:
          For index i (in the dataframe); The input_mask = [i, i-t1, i-t2, ..., i-tn]
          Where t is an element in the the target_mask

    NOTE: In addition to this we manually add a few lag periods (i-t's)
          1. We manually add i-365.
          2. The columns ['Total_RN', 'Total_RV'] are added to the input mask, with the same
             lags as the target variable
          3. Padding is added to include more data around the relevant lag periods
    """

    # Flip the target mask
    # TODO: Remove hardcoded split (due to performance issues) ..[:len(self.target_indices)/2]
    t_minuses = (-self.target_indices).tolist()[:len(self.target_indices)] + [-365]

    # Add padding (in reverse order [::-1] as it makes more sense for negative numbers)
    t_minuses = np.array([np.arange(t-padding-1, t+padding-1+1)[::-1] for t in t_minuses])
    t_minuses = t_minuses.reshape(-1) # We want a serialized vector

    # Add index = 0
    t_minuses = np.array([0] + t_minuses.tolist())

    # A little check to make sure we don't have any target indices in the input mask
    if (any([x in t_minuses for x in self.target_indices])):
      raise Exception("Target indices in input mask")

    # Create the final mask (target column + Total_RN and Total_RV)
    input_mask_dict = {key:[t_minuses] for key in [self.target_column] + self.req_cols}

    # Add forecasting values
    if (len(self.forecasting_cols) > 0):
      for key in self.forecasting_cols:
        input_mask_dict[key] = [np.array([0])]

    return IndexMask(input_mask_dict)

# # Some tests
# import unittest
# class d2oMaskFactoryTest(unittest.TestCase):
#   import pandas as pd

#   df = pd.DataFrame.from_dict({'Total_RV':range(0,10000), 'Total_RN':range(0,10000), 'Restaurant':range(0,10000)})
#   targets = [6,13,20,27,34,41]
#   target_mask = IndexMask({'Restaurant':[np.array(targets)]})

#   padding = 2
#   input_mask = d2oMaskFactory(df, target_mask).input_mask(padding=padding)
#   num_input_variables = len(input_mask.masks.keys())

#   def test_numvars(self):
#     """ Number of input variables """
#     self.assertTrue(self.num_input_variables == 3)

#   def test_len_input(self):
#     n_target_mask = len(self.target_mask) + 1
#     padding = self.padding*2 + 1
#     n_input_variables = self.num_input_variables
#     n_zeros = 3

#     lhs = len(self.input_mask)
#     rhs = n_target_mask*padding*n_input_variables+n_zeros

#     self.assertTrue(lhs == rhs)

#   def test_index_leaks(self):
#     """ Make sure target indices don't exist in the input mask """
#     self.assertFalse(any([x in np.concatenate(self.input_mask.masks.values()).reshape(-1) for x in self.targets]))

# if __name__ == "__main__":
#   unittest.main()