# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 00:34:27 2016

@author: stefan

Part of d2o. Used in neural networks to screen input parameters.
"""
# TODO: File contains certain hardcoded features. Needs other solutions.

import pandas as pd
import numpy as np
from masks import IndexMask
import d2o.analyze.correlation as correlation
import d2o.analyze.fourier as fourier
from d2o.utils.putils import add_mav


class MaskFactory:
  """
  Analyses a data frame and an output mask, then decides on a suitable
  input frame to use in time series predication.
  """
  def __init__(self):#, df, output_mask):
    #Need for init?
    pass

  def _analyze(self, df, **kwargs):
    # TODO: Implement function for threshold. (Significant update)
    # TODO: Add kwarg for what methods to use to analyze. (Fourier, aoutocorr, AIC, ALL, etc.)
    # Possible future feature, Full AIC of all interesting columns. (Significant update)
    # Normalize data before analyzing? what method of normalization?
    """
    Uses correlation, autocorrelation and fourier transformation to
    determine how the data behaves, and what periodes to use when
    creating the input mask.

    Returns
    -------
    analyze_results : dict
      Contains the indexes found to be of interest assigned to the keys
      of the colum they came from.
    """
    self.keys = kwargs.get("key", "ALL")
    if self.keys == "ALL":
      self.keys = df.columns.values.tolist()
    elif type(self.keys) == str:
      self.keys = [self.keys] #Needs to be list for itteration purposes.

    mav_width = kwargs.get("mav_width", 20) # TODO: Arbitrarily set to 20. Find a more robust solution?
    mav_df = add_mav(df, mav_width)

    #This part needs testing. Do mav_df contain the original columns?
    #Remove addition in key name from add_mav(), "_MAV(XX)"
    rename_dict = {}
    length_of_addition = 6 + len(str(mav_width))
    for key in mav_df:
      rename_dict[key] = key[0:-length_of_addition]
    mav_df.rename(index=str, columns=rename_dict, inplace=True)

    threshold = kwargs.get("threshold", 0.1) # TODO: find robust solution for threshold.

    results_fft = self._get_fourier_frequencies(mav_df, self.keys, threshold)
    results_pac = self._get_partial_autocorr_lags(mav_df, self.keys, threshold)

    #Join lists
    analyze_results = {}
    for key in self.keys:
#      print "FFT:", results_fft[key], "PAC:", results_pac[key]
      analyze_results[key] = results_fft[key] + results_pac[key]
#      print "Joined:", analyze_results[key]

#    print "analyze_results:"
#    print analyze_results
    return analyze_results

  def _create_mask(self, df, output_mask, **kwargs):
    # TODO: Optional to invert indexes?
    """
    Uses the output_mask and analyzes df to produce a input mask.
    """
    indexes_dict = self._analyze(df, **kwargs)

    padding = kwargs.get("padding", 5) #Arbitrary default padding.
    indexes_dict = self._add_padding(indexes_dict, padding)

    max_gap = kwargs.get("max_gap", 3) #Arbitrary default gap.
    indexes_dict = self._splitt_list(indexes_dict, max_gap)

    indexes_dict = self._invert_indexes(indexes_dict)

    input_mask = IndexMask(indexes_dict)
    return input_mask

  def _add_padding(self, indexes_dict, padding):
    """
    Adds padding on each index in list supplied. Does not add negative
    values.

    Returns
    -------
    unique_indexes_dict : dict
      Dict with sorted lists with unique elements.
    """
    unique_indexes_dict = {}
    for key in indexes_dict:
      indexes = indexes_dict[key]
      unique_indexes = []
      for element in indexes:
        if padding >= element:
          for new_element in range(1, element+padding+1):
            unique_indexes.append(new_element)
        else:
          for new_element in range(element-padding, element+padding+1):
            unique_indexes.append(new_element)
      unique_indexes = sorted(set(unique_indexes))
      unique_indexes_dict[key] = unique_indexes
    return unique_indexes_dict

  def _splitt_list(self, padded_dict, max_gap):
    """
    Splits the list in to a list of sublist based on the numbers in that
    list, and the gap allowed between numbers.

    Returns
    -------
    splitted_list_dict : dict of lists of np.arrays
    """
    splitt_list_dict = {}
    for key in padded_dict:
      padded_list = padded_dict[key]
      splitted_list = []
      sub_list = [padded_list[0]]

      for i in xrange(len(padded_list)-1):
        if padded_list[i+1] - padded_list[i] <= max_gap:
          sub_list.append(padded_list[i+1])
        else:
          splitted_list.append(np.array(sub_list))
          sub_list = [padded_list[i+1]]

      splitted_list.append(np.array(sub_list))
      splitt_list_dict[key] = splitted_list
    return splitt_list_dict

  def get_mask(self, df, output_mask, **kwargs):
    """
    Analyzes data frame and output_mask, and returns an input mask

    Parameters
    ----------
    df : pd.DataFrame

    outputMask : IndexMask
      Type is from clib/learn/nn/masks.py

    kwargs
    ------
    keys : str, list of strings, defaults "ALL".
      What columns in df to analyze and return mask for.

    threshold : float, default .2
      Number between 0 and 1.

    padding : int
      How much padding to add on either side of the indexes found in analyze.

    max_gap : int
      The largest gap allowed in a sub list in the returned mask.

    Returns
    -------
    input_mask : IndexMask
      From clib/learn/nn/masks.py
    """
    input_mask = self._create_mask(df, output_mask, **kwargs)

    return input_mask

  def _get_fourier_frequencies(self, df, keys, threshold=.2):
    # TODO: Most of this function is repeated in _get_partial_autocorr_lags(), seperate in to own function?
    """
    Analyzes the dataframe and returns the interesting frequencies for
    use in making the mask this class produces.

    Parameters
    ----------
    df : pd.DataFrame

    Returns
    -------
    fourier_dict : dict
    """
    fourier_df = fourier.fftdf(df, keys)

    fourier_dict = {}
    for key in keys:
      indexes = []
      index = 1

      fourier_max = np.max(fourier_df[key].values[1:-1])
      for element in fourier_df[key].values:
        if element > fourier_max*threshold: # TODO: Move multiplication outside loop.
          indexes.append(index)
        index += 1

      fourier_dict[key] = indexes
    return fourier_dict

  def _get_partial_autocorr_lags(self, df, keys, threshold=.2, nlags=370):
    # TODO: Include indexes with significant negative corelation?
    """
    Does a partial autocorrelation and returns the index lags that show greatest correlation.
    """
    pac_df = correlation.partial_autocorr(df, nlags=nlags, keys=keys)

    pac_dict = {}
    for key in keys:
      indexes = []
      index = 1

      pac_max = np.max(pac_df[key+"_PACF"].values[1:-1])
      for element in pac_df[key+"_PACF"].values:
        if element > pac_max*threshold:
          indexes.append(index)
        index += 1

      pac_dict[key] = indexes
    return pac_dict

  def _invert_indexes(self, a_dict):
    """
    Inverts the elements in a_dict.
    """
    for key in a_dict:
      for a_list in a_dict[key]:
        for i in xrange(len(a_list)):
          a_list[i] = -a_list[i]
    return a_dict

if __name__ == "__main__":
  print "a test block"


