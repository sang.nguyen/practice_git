import numpy as np
import math

def float_equal(x, y, limit=0.001):
  return np.abs(float(x) - float(y)) <= limit

def to_array(l):
  if (isinstance(l, list)):
    return np.array(l, dtype=np.float64)
  elif (is_numeric(l)):
    return l
  else:
    raise Exception("Can not convert type: %s to array" % type(l))

def is_numeric(obj):
  attrs = ['__add__', '__sub__', '__mul__', '__div__', '__pow__']
  return all(hasattr(obj, attr) for attr in attrs)

def absolute_error(approximate, exact):
  approximate = to_array(approximate)
  exact = to_array(exact)

  return np.abs(exact - approximate)

# Relative error
# Absolute error / exact values
def relative_error(approximate, exact):
  approximate = to_array(approximate)
  exact = to_array(exact)

  ret = np.abs(1 - approximate/exact).tolist()

  # Check for elements where approximate == exact
  # and replace inf with 0.0
  # TODO: Tidy up
  indices_to_replace = []
  for i in range(len(approximate.tolist())):
    if (float_equal(approximate[i], exact[i])):
      indices_to_replace.append(i)

  nret = []
  for i,el in enumerate(ret):
    if i in indices_to_replace:
      nret.append(0.0)
    else:
      nret.append(el)
  return np.array(nret)

def percent_difference(val1, val2):
  val1 = to_array(val1)
  val2 = to_array(val2)
  return np.abs((val1-val2)/((val1+val2)/2))*100

# Percent error
# (relative error * 100)
def percent_error(approximate, exact):
  return relative_error(approximate, exact)*100

# Mean absolute error
def mae(approximate, exact, median=False):
  abs_error = absolute_error(approximate, exact)
  if (median):
    return np.median(abs_error)
  else:
    return abs_error.mean()

# Mean absolute percentage error
# (mean of percent errors)
def mape(approximate, exact, median=False):
  per_error = percent_error(approximate, exact)

  finites = []
  for el in per_error:
    try:
      if (np.isfinite(el)):
        finites.append(el)
    except:
      continue

  if (median):
    return np.median(finites)
  else:
    return np.array(finites).mean()

# Symmetric absolute percentage error
def sapes(approximate, exact):
  approximate = to_array(approximate)
  exact = to_array(exact)

  indices_to_replace = []
  for i in range(len(approximate.tolist())):
    if (float_equal(approximate[i], exact[i])):
      indices_to_replace.append(i)

  ret = (np.abs(exact-approximate)/(exact + approximate) * 100).tolist()

  nret = []
  for i,el in enumerate(ret):
    if i in indices_to_replace:
      nret.append(0.0)
    else:
      nret.append(el)
  return np.array(nret)

# Symmetric mean absolute percentage error
def smape(approximate, exact, median=False):
  sapes_ = sapes(approximate, exact)

  if (median):
    return np.median(sapes_)
  else:
    return sapes_.mean()

# Mean squared error
def mse(approximate, exact):
  approximate = to_array(approximate)
  exact = to_array(exact)

  return ((approximate-exact)**2).mean()

# Root mean squared error
def rmse(approximate, exact):
  return math.sqrt(mse(approximate, exact))

# Mean absolute scaled error
# Train is the expected outputs (targets) in the training set
def mase(train, approximate, exact):
  train = to_array(train)
  approximate = to_array(approximate)
  exact = to_array(exact)

  n = train.shape[0] # Length
  scaling = np.abs(np.diff(train)).sum() / (n - 1)

  return mae(approximate, exact)/scaling

# Returns a vector of hits (1.0) and misses (0.0)
# based on the percent error
# If the sape <= threshold % -> hit
# If the sape > threshold % -> miss
# Threshold defaults to SMdAPE
def hits(approximate, exact, th=None):
  sapes_ = sapes(approximate, exact)
  threshold = th if th != None else int(smape(approximate, exact, median=True))

  hm = []
  for el in sapes_.tolist():
    if el <= threshold:
      hm.append(1)
    else:
      hm.append(0)

  return hm

def percent_hits(approximate, exact, th=None):
  hm = hits(approximate, exact, th)
  num_hits = hm.count(1)

  return (float(num_hits)/len(exact)) * 100