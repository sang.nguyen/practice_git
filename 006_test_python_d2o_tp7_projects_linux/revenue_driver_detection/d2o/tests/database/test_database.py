# coding: utf-8
from d2o.database import Database

import unittest
class DatabaseTest(unittest.TestCase):
  """ Test queries (only selects, no inserts) """
  # Might want to move these to separate config files...
  TEST_HOST = '192.168.75.96'
  TEST_USER = 'd2o'
  TEST_PWD = 'ChronosIT123#'
  TEST_DB = 'pmi_thon'

  TEST_HOTEL = 1140 # Britannia
  TEST_HID = 1150 # Hjørnet
  TEST_SEGMENT = 0 # Total

  db = Database(TEST_USER, TEST_PWD, TEST_HOST, TEST_DB, 'dbo')
  def test_is_connected(self):
    self.assertTrue(self.db.is_connected())

  def test_percent_data(self):
    return self.assertTrue(len([self.db.percent_data(self.TEST_HID, self.TEST_SEGMENT)]) > 0)

  def test_has_segments(self):
    return self.assertTrue(len([self.db.has_segments(self.TEST_HOTEL)]) > 0)

if __name__ == "__main__":
  unittest.main()