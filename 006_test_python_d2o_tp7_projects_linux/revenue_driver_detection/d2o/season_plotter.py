from d2o.utils import systools
from d2o.utils import logger as log
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np

Writer = animation.writers['ffmpeg']

class SeasonPlotter:
  def __init__(self, resdict, root_dir, hotel_id, hotel_name, database):
    self.df           = resdict['df']
    self.ycol         = resdict['ycol']
    self.y            = self.df[self.ycol]
    self.color_cols   = resdict['color_cols']
    self.diffs        = resdict['diffs']
    self.z_scores     = resdict['z_scores']
    self.cws          = resdict['cws']
    self.cps          = resdict['cps']
    self.operiods     = resdict['operiods']
    self.periods      = resdict['periods']
    self.iw_size      = resdict['iw_size']
    self.ow_size      = resdict['ow_size']
    self.min_length   = resdict['min_length']

    self.hotel_id   = hotel_id
    self.hotel_name = hotel_name
    self.file_desc = '%s %s' % (hotel_id, hotel_name)
    self.file_desc = self.file_desc.strip().replace(' ', '_').lower()

    if (root_dir[-1] == '/'):
      root_dir = root_dir[:-1]

    self.plots_dir = '%s/%s/%s' % (root_dir,database,hotel_id)
    self.writer = Writer(fps=40, metadata=dict(artist='d2o'), bitrate=1200)

    systools.makedirs(self.plots_dir)

    self.extra_colors = ['r','b','y','c','m','k']

  def create_animation(self):
    log.info("Creating animation...")
    # 1. Draw y with z_score calculation
    fig, ax  = plt.subplots()
    y_line,  = ax.plot(range(0,len(self.y)), self.y, c='g')
    d_line,  = ax.plot(range(0,len(self.diffs)),self.diffs, c='m')
    z_line,  = ax.plot([],[], c='m')
    cp_dots  = [ax.plot(x,self.y[x],'ro')[0] for x in self.cps]

    other = [x for x in self.color_cols if x != None]
    other_lines = [ax.plot(range(0,len(self.df[x])),self.df[x],color=self.extra_colors[i], label=x)[0] for i,x in enumerate(other)]

    o_vspans = [p.add_vspan(ax) for p in self.operiods]
    o_period_middles = [int(np.rint((p.x0+p.x1)/2.0)) for p in self.operiods]

    n_vspans = [p.add_vspan(ax) for p in self.periods]
    n_period_middles = [int(np.rint((p.x0+p.x1)/2.0)) for p in self.periods]

    [cp.set_visible(False) for cp in cp_dots]
    [vs.set_visible(False) for vs in o_vspans]
    [vs.set_visible(False) for vs in n_vspans]
    [o.set_visible(False) for o in other_lines]

    # For plotting the windows
    o_left = ax.axvline(0)
    o_right = ax.axvline(0)
    i_left = ax.axvline(0)
    i_right = ax.axvline(0)
    it_line = ax.axvline(0)

    # To show which season we are working on
    arrow = ax.annotate('', xy=(-1, -1),  xycoords='data', xytext=(150, 1.0), size=20,
      arrowprops=dict(arrowstyle="simple"),)

    handles = [mpatches.Patch(color=self.extra_colors[i], label=x) for i,x in enumerate(other)]
    legend = plt.legend(handles=handles, shadow=True, fancybox=True, frameon=True, framealpha=0.8)
    legend.set_visible(False)

    lines = [z_line, d_line, o_left, o_right, i_left, i_right, it_line] + \
      cp_dots + o_vspans + n_vspans + other_lines + [legend]

    ax.set_ylim([-0.1,1.1])
    ax.set_xlim([0,len(self.y)])

    ylen = len(self.y)

    def draw_iterator(i):
      it_line.set_data([i,i],[0,1])

    def draw_arrow(i):
      draw_iterator(i)

    def draw_window(i):
      ostart = (i-self.ow_size/2)
      ostart = ostart if  (ostart > 0) else 0
      oend   = (i+self.ow_size/2)
      oend   = oend if (oend < len(self.y)) else len(self.y)

      iend    = i + int(self.iw_size/2)
      iend    = iend if (iend < len(self.y)) else len(self.y)
      istart  = i - int(self.iw_size/2)
      istart  = istart if (istart > 0) else 0

      o_left.set_data([ostart,ostart],[0,1])
      o_right.set_data([oend,oend],[0,1])
      i_left.set_data([istart,istart],[0,1])
      i_right.set_data([iend,iend],[0,1])

    def clear_lines():
      for line in lines[:6]:
        line.set_data([],[])
      return lines

    def clear_points():
      for line in lines[6:]:
        line.set_data([],[])
      return lines

    def animate(i):
      log.progress(i, ylen*5, mod=5)

      if (i < ylen):
        z_line.set_data(range(0,i), self.z_scores[:i])
        draw_window(i)

      if (i == ylen):
        clear_lines()

      if (i >= ylen and i < ylen*2):
        j = i - ylen
        d_line.set_data(range(0,j), self.diffs[:j])
        draw_iterator(j)
        if (j in self.cps):
          dot = cp_dots[self.cps.index(j)]
          dot.set_visible(True)

      if (i == ylen*2):
        clear_lines()
        # clear_points()

      if (i >= ylen*2 and i < ylen*3):
        j = i - ylen*2
        draw_arrow(j)
        if (j in o_period_middles):
          vspan = o_vspans[o_period_middles.index(j)]
          vspan.set_visible(True)

      if (i == ylen*3):
        pass

      if (i >= ylen*3):
        j = i-ylen*3
        draw_arrow(j)
        if (j in o_period_middles):
          ovspan = o_vspans[o_period_middles.index(j)]
          ovspan.set_visible(False)
        if (j in n_period_middles):
          nvspan = n_vspans[n_period_middles.index(j)]
          nvspan.set_visible(True)

      if (i == ylen*4):
        [o.set_visible(True) for o in other_lines]
        y_line.set_visible(False)
        legend.set_visible(True)

      return lines

    anim_save_path = '%s/%s_animation.mp4' % (self.plots_dir,self.file_desc)
    anim = animation.FuncAnimation(fig, animate, init_func=clear_lines, frames=ylen*5, interval=1, blit=True)
    anim.save(anim_save_path, writer=self.writer)
    log.info("Animation saved to '%s'" % anim_save_path)

  def create_plots(self):
    fontsize = 16
    xlabel = "time (days)"
    ylabel = "value"
    dejfont = {'fontname':'Dejavu Sans Mono'}

    file_name = self.file_desc.replace('_', ' ')
    parameter_string = 'IW: %s, OW: %s, ML: %s' % (self.iw_size,self.ow_size,self.min_length)

    fig, ax = plt.subplots()
    fig.tight_layout()

    # BUG Causing this message "UserWarning: axes.color_cycle is deprecated and replaced with axes.prop_cycle; please use the latter."
    #pd.set_option('display.mpl_style', 'default')

    ax.plot(range(0, len(self.y)), self.y, color='g', label=self.ycol, linewidth=2)
    ax.plot(range(0, len(self.z_scores)), self.z_scores, color='m', label='z_scores')
    other = [x for x in self.color_cols if x != None]

    for i,el in enumerate(other):
      ax.plot(range(0,len(self.df[el])), self.df[el], color=self.extra_colors[i], label=el)
    legend = plt.legend(shadow=True, fancybox=True, frameon=True, framealpha=0.8)
    ax.set_xlim([0,len(self.y)])
    for cw in self.cws:
      plt.axvspan(cw[0],cw[-1], color='grey', alpha=0.4)
      plt.axvline(cw[0], color='k', alpha=0.5)
      plt.axvline(cw[-1], color='k', alpha=0.5)

    plt.suptitle("Z-Scores and change windows - %s" % file_name, fontsize=fontsize, **dejfont)
    t = plt.figtext(0.5, 0.93, parameter_string, fontsize=fontsize-4, ha='center', **dejfont)
    t.set_bbox(dict(color='white', edgecolor='black'))
    plt.xlabel(xlabel, fontsize=fontsize, **dejfont)
    plt.ylabel(ylabel, fontsize=fontsize, **dejfont)

    figure = plt.gcf()
    figure.set_size_inches(19.2, 7)
    zscores_save_path = '%s/%s_zscores.png' % (self.plots_dir, self.file_desc)
    plt.savefig(zscores_save_path, dpi = 100)
    log.info("ZScores plot saved to '%s'" % zscores_save_path)

    fig, ax = plt.subplots()
    fig.tight_layout()

    ax.plot(range(0, len(self.y)), self.y, color='g', label=self.ycol, linewidth=2)
    ax.plot(range(0, len(self.diffs)), self.diffs, color='m', label='diffs')

    for i,el in enumerate(other):
      ax.plot(range(0,len(self.df[el])), self.df[el], color=self.extra_colors[i], label=el)
    legend = plt.legend(shadow=True, fancybox=True, frameon=True, framealpha=0.8)
    ax.set_xlim([0,len(self.y)])
    for cp in self.cps:
      plt.axvline(cp, color='k')
      ax.plot(cp, self.y[cp], 'ro')

    plt.suptitle("Changepoints - %s" % file_name, fontsize=fontsize, **dejfont)
    t = plt.figtext(0.5, 0.93, parameter_string, fontsize=fontsize-4, ha='center', **dejfont)
    t.set_bbox(dict(color='white', edgecolor='black'))
    plt.xlabel(xlabel, fontsize=fontsize, **dejfont)
    plt.ylabel(ylabel, fontsize=fontsize, **dejfont)

    figure = plt.gcf()
    figure.set_size_inches(19.2, 7)
    cps_save_path = '%s/%s_changepoints.png' % (self.plots_dir, self.file_desc)
    plt.savefig(cps_save_path, dpi = 100)
    log.info("Changepoints plot saved to '%s'" % cps_save_path)

    fig, ax = plt.subplots()
    fig.tight_layout()
    ax.plot(range(0, len(self.y)), self.y, color='g', label=self.ycol, linewidth=2)
    for i,el in enumerate(other):
      ax.plot(range(0,len(self.df[el])), self.df[el], color=self.extra_colors[i], label=el)
    legend = plt.legend(shadow=True, fancybox=True, frameon=True, framealpha=0.8)
    ax.set_xlim([0,len(self.df)])
    for p in self.periods:
      p.add_vspan(plt)
      ax.plot(range(p.x0,p.x1), p.df[['Rate','Room Nights']][p.x0:p.x1])

    plt.suptitle("Seasons - %s" % file_name, fontsize=fontsize, **dejfont)
    t = plt.figtext(0.5, 0.93, parameter_string, fontsize=fontsize-4, ha='center', **dejfont)
    t.set_bbox(dict(color='white', edgecolor='black'))
    plt.xlabel(xlabel, fontsize=fontsize, **dejfont)
    plt.ylabel(ylabel, fontsize=fontsize, **dejfont)

    figure = plt.gcf()
    figure.set_size_inches(19.2, 7)
    seasons_save_path = '%s/%s_seasons.png' % (self.plots_dir, self.file_desc)
    plt.savefig(seasons_save_path, dpi = 100)
    log.info("Seasons plot saved to '%s'" % seasons_save_path)