# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 10:53:28 2016

@author: stefan

This class takes in a pd.DataFrame and analyzes the contents using
several methods, to get an idea of the completeness and the value of the
data.
"""
import pandas as pd
import numpy as np
import unittest

from d2o.utils.putils import get_path_to_dataset

class prep:
  def __init__(self, df):
    self.df = df
    self.num_cols = len(df.columns)
    self.num_rows = len(df.index)
    self.index_key = df.index.name

  def get_counts(self):
    """
    Counts several quantities, NaNs, Zeros and negative values.
    Returns the number of each category, and the ratio of these to the total
    length of each column.

    Parameters
    ----------
    find_loc : bool, default False
      If The function should return the index where NaN, zero or negative values
      are found.

    kwargs
    ------
    find_loc_type : str or list, default "all"
      what quantities to find the location of. Can be "Nan", "zero", "neg" or "all".

    Returns
    -------
    counts : pd.DataFrame
      Contains the counts and the ratios of the specified quantities.
    """
    # Find number of NaN, zero and negative values.
    self.num_NaN = (self.num_rows - self.df.count())
    self.num_zero = self.df[self.df == 0].count()
    self.num_neg = self.df[self.df < 0].count()

    self.num_NaN.name = "num_NaN"
    self.num_zero.name = "num_zero"
    self.num_neg.name = "num_neg"

    # Find location of NaN, zero and negative values.
    if find_loc:
      kwargs.get("find_loc_type", "all")
      for col in self.df.columns:
        print col, self.df[self.df[col]==0].index

    return pd.concat([self.num_NaN, self.num_zero, self.num_neg], axis=1).transpose()

  def get_count_indexes(self, column, kind=np.NaN):
    """
    Returns the indexes of the column elements containing kind.

    Parameters
    ----------
    column : str(, list of str?)
      Name of column to get indexes from.

    kind : str, type default np.NaN
      What kind of quantity to get indexes of. Can be np.NaN, int(0),
      "NaN", "neg" or "zero".

    Returns
    -------
    indexes : pd.Series
    """

  def get_stats(self):
    """
    Checks the distribution, + other stats.
    """
    return "YOU FOOL!"

  def get_arima():
    return "HULK SMASH YOUR CODE!"




class GetStatsTest(unittest.TestCase):
  def setUp(self):
    length = 21
    spann_half = 5*np.pi
    np.random.seed(seed=17)

    x = np.linspace(-spann_half, spann_half, length)

    random_normal = np.random.normal(size=length)
    random_normal_abs = np.abs(random_normal)

    y = np.zeros(length)
    y[length/2:-1] += random_normal[length/2:-1]

    sin_trend = np.sin(x) + .1*x
    sin_trend_noise = sin_trend + .3*random_normal

    z = np.zeros(length)

    for i in xrange(len(x)):
      if i%3 == 0:
        y[i] = np.NaN

    for i in xrange(length - int(.4*length)):
      z[length/5 + i] = np.random.random_sample()

    temp_dict = {"x": x, "rand_norm":random_normal, "rand_norm_abs":random_normal_abs,
                 "leading_zero_NaN": y, "sin_trend":sin_trend, "sin_trend_noise":sin_trend_noise,
                 "leading_trailing_zeros":z}
    self.df = pd.DataFrame.from_dict(temp_dict).set_index("x")

    self.df_prep = prep(self.df)
    self.df_correct = pd.read_csv(get_path_to_dataset("count_df_prepper_test.csv"), index_col=0)

  def test_get_stats(self):
    counts = self.df_prep.get_counts()
    test_result = counts.equals(self.df_correct)
    self.assertTrue(test_result, msg="Test not implemented")











