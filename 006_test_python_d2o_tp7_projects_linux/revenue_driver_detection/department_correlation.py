"""This module is used to compute the correlation between each pair of departments in one hotel """

import pandas as pd
import numpy as np
from datetime import timedelta, datetime
from sklearn import linear_model
import project_lib as lib
import argparse
import os
from capture_arr import *
import pickle


def get_series_to_compute_corr(y_col_name, x_col_name, lag, y_dates, df):
    """
    Prepare series to compute correlation
    :param y_col_name: name of column y
    :param x_col_name: name of column x
    :param lag: lag days between y_col and x_col
    :param y_dates: dates series by y_col
    :param df: dataframe contain all data of one hotel
    :return:
    """
    col_dates  = lib.get_dates_lag(y_dates, lag)
    y_series   = lib.get_df_by_dates(y_dates, df)[y_col_name]
    col_series = lib.get_df_by_dates(col_dates, df)[x_col_name]
    # if length of x_col data smaller than of y_col just return data by length x_col
    return y_series[:len(col_series)], col_series

def get_df_corr_data(dep_id, seg_id, season_tf, day_of_week, df):
    """
    Get data to test. It's consistent with the ways getting data in compute_corr
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :return:
    """
    dates = df['date'].sort_values(ascending=False)
    season_dates = pd.Series(lib.get_season_dates_from_tf(season_tf, dates))
    y_dates = list(season_dates[season_dates.dt.weekday == day_of_week])
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name
    cols = set(df.columns) - {'date', 'day_of_week'}
    y_series = lib.get_df_by_dates(y_dates,df)[y_col]
    df_result = pd.DataFrame({y_col:list(y_series)})
    for lag in [0, 1, 7, 14, 21, 30, 365]:
        for i, col in enumerate(cols):
            df_result["date_{}".format(lag)] = lib.get_dates_lag(y_dates, lag)
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            y_series, col_series = get_series_to_compute_corr(y_col, col, lag, y_dates, df)
            if len(col_series) < len(y_dates):
                df_result["{}_{}".format(col, lag)] = list(col_series) + ['F']*(len(y_dates)-len(col_series))
            else:
                df_result["{}_{}".format(col, lag)] = list(col_series)
    return df_result


# ver 2 is ERROR
def compute_corr_ver_2(dep_id, seg_id, season_tf, num_day, day_of_week, significance_level, df):
    """
    Compute explanation (1 -error) for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """

    dates = df['date'].sort_values(ascending=False)
    season_dates = pd.Series(lib.get_season_dates_from_tf(season_tf, dates))
    y_dates = list(season_dates[season_dates.dt.weekday == day_of_week])
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name

    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])
    # x is some col with some lag
    index = 0
    for i, col in enumerate(cols):
        for lag in [0, 1, 7, 14, 21, 30, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            y_series, col_series = get_series_to_compute_corr(y_col, col, lag, y_dates, df)

            cap_arr = compute_capture_arr(y_col, col, lag, season_tf, num_day, day_of_week, df)
            y_series_rm, col_series = lib.remove_outlier(y_series, col_series)
            cov_val = 1 - lib.compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])

            # cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            if "rn" in col:
                conf_interval_0, conf_interval_1 = cov_val, cov_val
            else:
                conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1
    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)

# ver 2 is ERROR
def compute_corr_ver_2_1(dep_id, seg_id, season_tf, num_day, day_of_week, significance_level, df, date_ss):
    """
    Compute explanation (1 -error) for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """

    # dates = df['date'].sort_values(ascending=False)
    # season_dates = pd.Series(lib.get_season_dates_from_tf(season_tf, dates))
    y_dates_full = [i for i in date_ss if i.weekday() == day_of_week]
    y_dates = y_dates_full[-(min(num_day, len(y_dates_full))):]
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name
    df1_full = lib.get_df_by_dates(y_dates_full, df)
    y_series = df1_full[y_col]

    df1 = lib.get_df_by_dates(y_dates, df1_full)
    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])
    # x is some col with some lag
    index = 0

    dict_df = {}
    dict_df[0] = df1[df1.columns]
    dict_df["0_full"] = df1_full[df1_full.columns]
    for lag in [1, 7, 365]:
        dates  = lib.get_dates_lag(y_dates, lag)
        dates_full  = lib.get_dates_lag(y_dates_full, lag)
        df2_full = lib.get_df_by_dates(dates_full, df)
        df2 = lib.get_df_by_dates(dates, df2_full)
        dict_df[lag] = df2
        dict_df[str(lag) + "_full"] = df2_full

    for i, col in enumerate(cols):
        # start_time = time.time()
        if lib.check_total(y_col, col):
            # print("a")
            continue
        for lag in [0, 1, 7, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            
            col_series = dict_df[str(lag) + "_full"][col]
            y_series_rm = y_series[-len(col_series):]
            df2 = dict_df[lag]

            # col_dates  = lib.get_dates_lag(y_dates, lag)
            # col_dates_full  = lib.get_dates_lag(y_dates_full, lag)
            # df2_full = lib.get_df_by_dates(col_dates_full, df)
            # col_series = df2_full[col]
            # df2 = lib.get_df_by_dates(col_dates, df2_full)

            # y_series_rm = y_series[-len(col_series):]

            y_series_rm, col_series_rm = lib.remove_outlier(y_series_rm, col_series)
            cap_arr = compute_capture_arr_ver_2(y_col, col, 1, 1, df1, df2, cols)
            cov_val = 1 - lib.compute_error(y_series_rm, col_series_rm, cap_arr['capture'], cap_arr['ARR'])

            # cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            if "rn" in col:
                conf_interval_0, conf_interval_1 = cov_val, cov_val
            else:
                conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1

    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)


# ver 3 is CORRELATION / REGRESSION
def compute_corr_ver_3(dep_id, seg_id, season_tf, num_day, day_of_week, significance_level, df):
    """
    Compute correlation for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """
    dates = df['date'].sort_values(ascending=False)
    season_dates = pd.Series(lib.get_season_dates_from_tf(season_tf, dates))
    y_dates = list(season_dates[season_dates.dt.weekday == day_of_week])
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name

    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])

    # x is some col with some lag
    index = 0
    for i, col in enumerate(cols):
        for lag in [0, 1, 7, 14, 21, 30, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            y_series, col_series = get_series_to_compute_corr(y_col, col, lag, y_dates, df)

            # cap_arr = compute_capture_arr(y_col, col, lag, season_tf, num_day, day_of_week, df)
            y_series_rm, col_series = lib.remove_outlier(y_series, col_series)
            # cov_val = 1 - compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])

            cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1
            
    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)


# ver 3 is CORRELATION / REGRESSION
def compute_corr_ver_3_1(dep_id, seg_id, season_tf, num_day, day_of_week, significance_level, df, date_ss):
    """
    Compute correlation for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """
    y_dates = [i for i in date_ss if i.weekday() == day_of_week]
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name
    y_series = lib.get_df_by_dates(y_dates, df)[y_col]
    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])
    # x is some col with some lag
    index = 0

    for i, col in enumerate(cols):
        # start_time = time.time()
        if lib.check_total(y_col, col):
            # print("a")
            continue
        for lag in [0, 1, 7, 14, 21, 30, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                # print("a1")
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                # print("a2")
                continue
            # print("0--- %s seconds ---" % (time.time() - start_time))
            col_dates  = lib.get_dates_lag(y_dates, lag)
            col_series = lib.get_df_by_dates(col_dates, df)[col]
            # print("1--- %s seconds ---" % (time.time() - start_time))

            # print("2--- %s seconds ---" % (time.time() - start_time))
            y_series_rm, col_series = lib.remove_outlier(y_series, col_series)
            # cap_arr = compute_capture_arr_ver_2(y_col, col, lag, season_tf, num_day, day_of_week, df, date_ss)
            # cov_val = 1 - lib.compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])
            # print("3--- %s seconds ---" % (time.time() - start_time))

            cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            if "rn" in col:
                conf_interval_0, conf_interval_1 = cov_val, cov_val
            else:
                conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            # print("4*** %s seconds ---" % (time.time() - start_time))
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1

    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)


# ver 2 is ERROR
def compute_corr_ver_2_week(dep_id, seg_id, season_tf, significance_level, df, year_input):
    """
    Compute explanation (1 -error) for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """
    dates = df['date'].sort_values(ascending=False)
    season_dates = pd.Series(lib.get_season_dates_from_tf(season_tf, dates))
    # y_dates = list(season_dates[season_dates.dt.weekday == day_of_week])
    y_dates = list(season_dates)
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name

    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])

    # x is some col with some lag
    index = 0
    for i, col in enumerate(cols):
        for lag in [0, 1, 7, 14, 21, 30, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            y_series, col_series = get_series_to_compute_corr(y_col, col, lag, y_dates, df)

            cap_arr = compute_capture_arr_week(y_col, col, lag, season_tf, df, year_input)
            y_series_rm, col_series = lib.remove_outlier(y_series, col_series)
            cov_val = 1 - lib.compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])

            # cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            if "rn" in col:
                conf_interval_0, conf_interval_1 = cov_val, cov_val
            else:
                conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1
            
    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)


def compute_corr_ver_2_week_1(dep_id, seg_id, season_tf, significance_level, df, year_input, date_ss):
    y_dates = date_ss
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name
    df1_full = lib.get_df_by_dates(y_dates, df)
    y_series = df1_full[y_col]

    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])
    # x is some col with some lag
    index = 0

    dict_df = {}
    dict_df["0_2"] = df1_full[df1_full.columns]
    dict_df["0_1"] = df1_full[df1_full.columns]
    for lag in [1, 7, 365]:
        col_dates  = lib.get_dates_lag(y_dates, lag)
        df2 = lib.get_df_by_dates(col_dates, df)
        dict_df[str(lag) + "_2"] = df2[df2.columns]
        dict_df[str(lag) + "_1"] = df1_full[(len(df1_full) - len(df2)) : ]

    for i, col in enumerate(cols):
        if lib.check_total(y_col, col):
            continue
        for lag in [0, 1, 7, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            # col_dates  = lib.get_dates_lag(y_dates, lag)
            # df2 = lib.get_df_by_dates(col_dates, df)
            # col_series = df2[col]
            # y_series_rm = y_series[-len(col_series):]
            # df1 = df1_full[(len(df1_full) - len(df2)) : ]

            df1 = dict_df[str(lag) + "_1"]
            df2 = dict_df[str(lag) + "_2"]
            col_series = df2[col]
            y_series_rm = y_series[-len(col_series):]

            y_series_rm, col_series = lib.remove_outlier(y_series_rm, col_series)
            cap_arr = compute_capture_arr_ver_2(y_col, col, 1, 1, df1, df2, cols)
            cov_val = 1 - lib.compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])

            if "rn" in col:
                conf_interval_0, conf_interval_1 = cov_val, cov_val
            else:
                conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1

    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)


# ver 3 is CORRELATION / REGRESSION
def compute_corr_ver_3_week(dep_id, seg_id, season_tf, significance_level, df):
    """
    Compute correlation for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """
    dates = df['date'].sort_values(ascending=False)
    season_dates = pd.Series(lib.get_season_dates_from_tf(season_tf, dates))
    y_dates = list(season_dates)
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name

    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])

    # x is some col with some lag
    index = 0
    for i, col in enumerate(cols):
        for lag in [0, 1, 7, 14, 21, 30, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                continue
            y_series, col_series = get_series_to_compute_corr(y_col, col, lag, y_dates, df)

            # cap_arr = compute_capture_arr(y_col, col, lag, season_tf, num_day, day_of_week, df)
            y_series_rm, col_series = lib.remove_outlier(y_series, col_series)
            # cov_val = 1 - compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])

            cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1
            
    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)



# ver 3 is CORRELATION / REGRESSION
def compute_corr_ver_3_week_1(dep_id, seg_id, season_tf, significance_level, df, date_ss):
    """
    Compute correlation for all pair of deparment_segment in one hotel
    :param dep_id: department id
    :param seg_id: segment id
    :param season_tf: season timeframe
    :param day_of_week:  day of week range 0-6
    :param df: Dataframe contain all data of one hotel
    :param significance_level: significance to compute confidence interval
    :return:
    """
    y_dates = date_ss
    y_col = '_'.join([str(dep_id), str(seg_id)]) + '_rv'  # Get revenue data name
    y_series = lib.get_df_by_dates(y_dates, df)[y_col]
    cols = set(df.columns) - {'date', 'day_of_week'}
    # result_df = pd.DataFrame(columns=['col', 'lag', 'cov_value', "conf_interval_0", "conf_interval_1"])
    result_df = pd.DataFrame(columns=['col', "cov_col", 'lag', 'cov_value', "conf_interval_0", "conf_interval_1", "adj_corr_col"])
    # x is some col with some lag
    index = 0

    for i, col in enumerate(cols):
        # start_time = time.time()
        if lib.check_total(y_col, col):
            # print("a")
            continue
        for lag in [0, 1, 7, 14, 21, 30, 365]:
            if (col.split('_')[0] != str(dep_id)) and (lag not in [0, 1]):
                # print("a1")
                continue
            if (col.split('_')[0] == str(dep_id)) and (lag in [0, 1]):
                # print("a2")
                continue
            # print("0--- %s seconds ---" % (time.time() - start_time))
            col_dates  = lib.get_dates_lag(y_dates, lag)
            col_series = lib.get_df_by_dates(col_dates, df)[col]
            # print("1--- %s seconds ---" % (time.time() - start_time))

            # print("2--- %s seconds ---" % (time.time() - start_time))
            y_series_rm, col_series = lib.remove_outlier(y_series, col_series)
            # cap_arr = compute_capture_arr_ver_2(y_col, col, lag, season_tf, num_day, day_of_week, df, date_ss)
            # cov_val = 1 - lib.compute_error(y_series_rm, col_series, cap_arr['capture'], cap_arr['ARR'])
            # print("3--- %s seconds ---" % (time.time() - start_time))

            cov_val = np.corrcoef(y_series_rm, col_series)[1, 0]
            if "rn" in col:
                conf_interval_0, conf_interval_1 = cov_val, cov_val
            else:
                conf_interval_0, conf_interval_1 = lib.compute_interval(cov_val, significance_level)
            # print("4*** %s seconds ---" % (time.time() - start_time))
            result_df.loc[index] = [y_col, col, lag, cov_val, conf_interval_0, conf_interval_1, col + "_" + str(lag)]
            index += 1

    result_df = result_df.fillna(-99)
    return result_df.sort_values('cov_value', ascending=False)


if __name__ == "__main__":
    # parser = argparse.ArgumentParser("Compute correlation",
    #                                  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # parser.add_argument("-H", "--hotel", help="Hotel ID", required=True)
    # parser.add_argument("-d", "--dep_id", help="Department ID", required=True)
    # parser.add_argument("-seg", "--seg_id", help="Segment", required=True)
    # # parser.add_argument("-s", "--season", help="Season", required=True)
    # parser.add_argument("-t", "--type", help="Type of running", required=True)
    #
    #
    # args = parser.parse_args()
    # hotel_id = args.hotel_id
    # seg_id = args.seg_id
    # dep_id = args.dep_id
    # type_run = args.type
    #
    # if type_run == 'corr':
    #     # pre-process
    rn_df = pd.read_csv(os.path.join('data', '223','hotel_223_rn_4y_1Apr_filled.csv'))
    rv_df = pd.read_csv(os.path.join('data', '223','hotel_223_rv_4y_1Apr_filled.csv'))
    gn_df = pd.read_csv(os.path.join('data', '223','hotel_223_guest_4y_1Apr_filled.csv'))

    date_s = rn_df['date']
    del rn_df['date']
    del rv_df['date']
    del gn_df['date']

    rn_df.columns = ["{0}_rn".format(col) for col in rn_df.columns]
    rv_df.columns = ["{0}_rv".format(col) for col in rv_df.columns]
    gn_df.columns = ["{0}_gn".format(col) for col in gn_df.columns]

    df = pd.concat([date_s, rv_df, rn_df, gn_df], axis=1)
    df['date'] = pd.to_datetime(df['date'])
    df = df[(df['date'] > datetime(year=2013, day=1, month=1)) & (df['date'] < datetime(year=2014, day=31, month=12))]
    df['day_of_week'] = df['date'].dt.dayofweek
    #
    # season_tf = [('02-25', '03-03'), ('05-28', '06-01')]
    # test_df = get_df_corr_data(223, 0, season_tf, day_of_week, df)
    significance_level = 0.05
    num_day = 10


    dict_df_corr = {}
    for col in rv_df.columns:
        dep_id = col.split("_")[0]
        seg_id = col.split("_")[1]
        l_season_tf = lib.read_dep_season_timeframe(dep_id)
        for season_tf in l_season_tf:
            for day_of_week in xrange(7):
                # print col, season_tf, day_of_week
                key = "{0}_{1}_{2}".format(col, season_tf, day_of_week)
                dict_df_corr[key] = compute_corr_ver_2(dep_id, seg_id, season_tf, num_day,\
                                                       day_of_week, significance_level, df)

    pickle.dump(dict_df_corr, open(os.path.join('data', '223', "pickle_dict_df_corr"), "wb"))





